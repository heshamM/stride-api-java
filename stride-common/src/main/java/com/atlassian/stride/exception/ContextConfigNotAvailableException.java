package com.atlassian.stride.exception;

import com.atlassian.stride.config.ContextConfig;

/**
 * Thrown when some part of the code tried to get {@link ContextConfig} based on the current
 * context (thread/request) but it was NOT available.
 * <p>
 * It may happen if you're trying to call the API without from a thread in which the context was not initialized, e.g.:
 * <ul>
 *     <li>You're in a background thread and there's no context attached to it.</li>
 *     <li>Your controller class or method is NOT annotated with {@code AuthorizeJwtHeader} or {@code AuthorizeJwtParameter} from the
 *      {@code stride-auth} library, thus the JWT token was not verified and the context was not set.</li>
 *     <li>There's no {@code clientId} path variable in your request mapping, thus the context was not initialized.</li>
 * </ul>
 * <p>
 * In order to fix that, make sure you're calling the API within a context-bounded thread, or pass a configuration
 * manually when calling the API, or use a fixed instance configuration.
 */
public class ContextConfigNotAvailableException extends RuntimeException {

    private static final String message = "Configuration not available in current context (request).";

    public ContextConfigNotAvailableException() {
        super(message);
    }

}
