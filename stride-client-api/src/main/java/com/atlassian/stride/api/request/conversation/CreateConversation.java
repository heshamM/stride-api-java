package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.ConversationCreateRequest;
import com.atlassian.stride.api.model.EntityCreatedResponse;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.webhooks.Conversation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Create a conversation
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
@Slf4j
public class CreateConversation {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String name;
    private String privacy = Conversation.PRIVACY_PUBLIC;
    private String topic;

    public CreateConversation makePublic() {
        this.privacy = Conversation.PRIVACY_PUBLIC;
        return this;
    }

    public CreateConversation makePrivate() {
        this.privacy = Conversation.PRIVACY_PRIVATE;
        return this;
    }

    /**
     * Create conversation
     */
    public CompletableFuture<EntityCreatedResponse> on(String cloudId) {
        return apiRequest.build(EntityCreatedResponse.class, b -> b
                .acceptJson()
                .scope(Scope.STRIDE_MANAGE_CONVERSATION)
                .expect(HttpStatus.SC_CREATED)
                .body(ConversationCreateRequest.builder().name(name).privacy(privacy).topic(topic).build()))
                .post(apiRequest.uri(StrideEndpoint.conversation.manage.create(cloudId)))
                .whenComplete((a, b) -> log.debug("Created conversation with id {}", a.getId()));
    }

}