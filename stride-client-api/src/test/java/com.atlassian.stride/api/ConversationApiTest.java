package com.atlassian.stride.api;

import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.model.ConversationGetAllResponse;
import com.atlassian.stride.api.model.EntityCreatedResponse;
import com.atlassian.stride.api.model.Roster;
import com.atlassian.stride.model.context.Context;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@Slf4j
public class ConversationApiTest {

    private StrideClient stride = Helper.client();

    @Test
    public void createConversation() throws Exception {
        String conversationName = "Tést Conversation " + System.currentTimeMillis();
        String topic = "Hello tópic " + System.currentTimeMillis();

        //Create convo
        EntityCreatedResponse e = stride.conversation().manage().create()
                .name(conversationName)
                .topic(topic)
                .makePublic()
                .on(Helper.cloudId())
                .get();
        log.info("Created conversation {}", e.getId());

        //fetch and assert name
        Conversation conversation = stride.conversation().fetch().one()
                .from(Context.conversation(Helper.cloudId(), e.getId()))
                .get();
        assertThat(conversation.getName(), is(conversationName));
        assertThat(conversation.getTopic(), is(topic));
        assertThat(conversation.isPublic(), is(true));
        assertThat(conversation.isPrivate(), is(false));
    }

    @Test
    public void archiveAndUnarchive() throws Exception {
        Conversation conversation = stride.conversation().fetch().one()
                .from(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .get();
        assertThat(conversation.getArchived(), is(false));

        //archive
        stride.conversation().manage().archive().on(conversation).get();
        Conversation archivedConvo = stride.conversation().fetch().one()
                .from(conversation)
                .get();
        assertThat(archivedConvo.getArchived(), is(true));

        //unarchive
        stride.conversation().manage().unarchive().on(conversation).get();
        Conversation unarchivedConvo = stride.conversation().fetch().one()
                .from(conversation)
                .get();
        assertThat(unarchivedConvo.getArchived(), is(false));
    }

    @Test
    public void update() throws Exception {
        String conversationName = "Tést Conversation " + System.currentTimeMillis();
        String topic = "Hello tópic " + System.currentTimeMillis();

        //Resets
        stride.conversation().manage().update()
                .name(conversationName)
                .topic(topic)
                .makePublic()
                .on(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .get();
        Conversation conversation = stride.conversation().fetch().one()
                .from(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .join();
        assertThat(conversation.getName(), is(conversationName));
        assertThat(conversation.getArchived(), is(false));

        //update convo
        stride.conversation().manage().update()
                .using(conversation)
                .topic(topic + " [changed]")
                .on(conversation.asContext())
                .join();
        Conversation updatedConvo = stride.conversation().fetch().one()
                .from(conversation)
                .get();
        assertThat(updatedConvo.getName(), is(conversationName));
        assertThat(updatedConvo.getTopic(), is(topic + " [changed]"));
        assertThat(updatedConvo.isPublic(), is(true));
        assertThat(updatedConvo.isPrivate(), is(false));

        //update convo again
        stride.conversation().manage().update(conversation)
                .name(conversationName + " 2")
                .on(conversation.asContext())
                .join();
        Conversation updatedConvo2 = stride.conversation().fetch().one()
                .from(conversation)
                .get();
        assertThat(updatedConvo2.getName(), is(conversationName + " 2"));
        assertThat(updatedConvo2.getTopic(), is(topic));
        assertThat(updatedConvo2.isPublic(), is(true));
        assertThat(updatedConvo2.isPrivate(), is(false));
    }

    @Test
    public void roster() throws Exception {
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        Roster roster = stride.conversation().fetch().roster()
                .from(context)
                .get();
        assertThat(roster.getUsers().size(), greaterThan(0));
    }

    @Test
    public void list() throws Exception {
        ConversationGetAllResponse convos = stride.conversation().fetch().list()
                .asc()
                .limit(2)
                .from(Helper.cloudId())
                .get();
        assertThat(convos.getValues().size(), is(2));
        ConversationGetAllResponse convosPageTwo = stride.conversation().fetch().list()
                .cursor(convos.getCursor())
                .asc()
                .limit(2)
                .from(Helper.cloudId())
                .get();
        assertThat(convosPageTwo.getValues().size(), is(2));
        assertThat(convosPageTwo.getValues(), not(contains(convos.getValues().get(0))));
        assertThat(convosPageTwo.getValues(), not(contains(convos.getValues().get(1))));
    }
}