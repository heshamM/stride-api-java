package com.atlassian.stride.request;

import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.concurrent.CompletableFuture;

/**
 * Async version of {@link Request}
 */
@ParametersAreNonnullByDefault
public interface AsyncRequest<R> extends Request<CompletableFuture<R>> {

    /**
     * Post the request and returns {@link CompletableFuture#completedFuture(Object)} containing a response with the
     * given {@code responseType}.
     */
    CompletableFuture<R> post(URI uri);


    /**
     * Post the request and returns {@link CompletableFuture#completedFuture(Object)} containing a response with the
     * given {@code responseType}.
     */
    CompletableFuture<R> put(URI uri);

    /**
     * Post the request and returns {@link CompletableFuture#completedFuture(Object)} containing a response with the
     * given {@code responseType}.
     */
    CompletableFuture<R> patch(URI uri);

    /**
     * Send the request and returns {@link CompletableFuture#completedFuture(Object)} containing a response with the
     * given {@code responseType}.
     */
    CompletableFuture<R> get(URI uri);

    /**
     * Send the request and returns {@link CompletableFuture#completedFuture(Object)}.
     */
    CompletableFuture<R> delete(URI uri);

}
