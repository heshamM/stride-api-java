package com.atlassian.stride.auth.request;

import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.request.httpclient.ContextConfigInHttpContext;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.protocol.HttpContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;

/**
 * HTTP request retry strategy that allows the decoration of a delegated strategy - so the user can still
 * use its desired strategy - while does additional retry in case the token was rejected with a HTTP response 401.
 */
@ParametersAreNonnullByDefault
@AllArgsConstructor(staticName = "of")
@Slf4j
public class TokenRefreshingRetryHandler implements HttpRequestRetryHandler {

    @Nonnull
    private final CachedTokenGenerator cachedTokenGenerator;
    @Nullable
    private final HttpRequestRetryHandler delegate;

    @Override
    public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
        val retry = delegate == null || delegate.retryRequest(exception, executionCount, context);
        if (retry) {
            final HttpClientContext clientContext = HttpClientContext.adapt(context);
            if (clientContext.getResponse().getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED 
                    || clientContext.getResponse().getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN) {
                val config = ContextConfigInHttpContext.adapt(context);
                if (config.isPresent()) {
                    log.debug("Retrying request from App {} to {} that got error {}", 
                            config.get().clientId(),
                            clientContext.getTargetHost().toURI(), 
                            clientContext.getResponse().getStatusLine().getStatusCode()); 
                    cachedTokenGenerator.invalidate(config.get().clientId());
                    return true;
                } else {
                    log.warn("Request to {} was unauthorized but there's no config to retry", clientContext.getTargetHost().toURI());
                }
            }
        }
        return false;
    }

}
