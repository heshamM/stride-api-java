package com.atlassian.stride.auth.model;

import com.atlassian.stride.model.Scope;
import lombok.Data;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents the success response when asking for a fresh token.
 */
@Data
public class RefreshTokenResponse {

    public static final String HEADER_NAME = "Authorization";

    private String access_token;

    /**
     * Expiration timeout in seconds
     */
    private Long expires_in;
    private String scope;
    private String token_type;

    public String getAuthorizationHeaderValue() {
        return getToken_type() + " " + getAccess_token();
    }

    /**
     * Return a list of parsed scopes from the String in {@link #scope}. 
     */
    public Set<Scope> parsedScopes() {
        return Arrays.stream(scope.split("\\s"))
                .map(Scope::from)
                .collect(Collectors.toSet());
    }
    
}
