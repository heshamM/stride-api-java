package com.atlassian.adf;

/**
 * Represents a node of the Atlassian Document Format.
 */
public interface Node {

    /**
     * Serializes node to text in an ad-hoc non-official fashion.
     */
    String text();

}
