package com.atlassian.stride.api.request;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.MediaUploadResponse;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * Upload a media file
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class UploadMedia {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String name;
    private Object content;

    /**
     * Upload the media
     */
    public CompletableFuture<MediaUploadResponse> to(ConversationContext conversationContext) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(content, "content");

        return apiRequest.build(MediaUploadResponse.class, b -> b
                .contentType(ContentType.APPLICATION_OCTET_STREAM)
                .acceptJson()
                .expect(HttpStatus.SC_CREATED)
                .body(content))
                .post(apiRequest.uriBuilder()
                        .endpoint(StrideEndpoint.media.upload(conversationContext))
                        .param("name", name)
                        .build());
    }

}