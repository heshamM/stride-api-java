package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.BlockNodeContainer;
import com.atlassian.adf.Node;
import com.atlassian.adf.base.AbstractBlockNodeContainer;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.stream.Collectors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/blockquote
 */
@Data(staticConstructor = "blockQuote")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("blockquote")
public class BlockQuote extends AbstractBlockNodeContainer<BlockQuote> implements BlockNode, BlockNodeContainer<BlockQuote> {

    public static BlockQuote blockQuote(BlockNode... elements) {
        return blockQuote().add(elements);
    }

    @Override
    public String text() {
        final List<BlockNode> children = children();
        if (children != null) {
            return children.stream().map(Node::text).map(s -> "  " + s).collect(Collectors.joining("\n"));
        }
        return "";
    }

}
