package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * ConversationGetAllResponse
 */
@Data
public class ConversationGetAllResponse extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;
    private String cursor;
    private Long limit;
    private Long size;
    private List<SortField> sort;
    private List<Conversation> values;

    /**
     * SortField represent a data structure that includes the field name and sorting order
     */
    @Data
    public class SortField {
        private String field;
        private String order;
    }

}

