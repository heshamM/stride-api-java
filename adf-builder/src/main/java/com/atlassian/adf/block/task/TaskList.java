package com.atlassian.adf.block.task;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.val;

import java.util.List;
import java.util.function.Consumer;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/taskList
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("taskList")
public class TaskList extends AbstractNodeContainer<TaskList, TaskItem> implements BlockNode {

    private TaskList() {
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = TaskItem.class)
    public List<TaskItem> children() {
        return super.children();
    }

    @JsonIgnore
    public String localId() {
        return strAttribute("localId").orElse("");
    }

    @JsonIgnore
    public TaskList localId(String localId) {
        return attribute("localId", localId);
    }

    public TaskList done(String localId, String text) {
        return add(TaskItem.task(localId, TaskItem.TaskState.DONE).text(text));
    }

    public TaskList done(String localId, Consumer<TaskItem> c) {
        val t = TaskItem.task(localId, TaskItem.TaskState.DONE);
        c.accept(t);
        return add(t);
    }

    public TaskList todo(String localId, String text) {
        return add(TaskItem.task(localId, TaskItem.TaskState.TODO).text(text));
    }

    public TaskList todo(String localId, Consumer<TaskItem> c) {
        val t = TaskItem.task(localId, TaskItem.TaskState.TODO);
        c.accept(t);
        return add(t);
    }

    public static TaskList taskList(String localId, TaskItem... elements) {
        return new TaskList().localId(localId).add(elements);
    }

}
