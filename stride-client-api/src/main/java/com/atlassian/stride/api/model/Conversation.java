package com.atlassian.stride.api.model;

import com.atlassian.stride.model.context.Context;
import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

import static com.atlassian.stride.model.webhooks.Conversation.PRIVACY_PRIVATE;
import static com.atlassian.stride.model.webhooks.Conversation.PRIVACY_PUBLIC;

/**
 * Represents a conversation object
 */
@Data
public class Conversation extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;
    private String avatarUrl;
    private String cloudId;
    private String id;
    @JsonProperty("isArchived")
    private Boolean archived;
    private String name;
    private String privacy;
    private String topic;
    private String type;
    private Date created;

    public boolean isPublic() {
        return PRIVACY_PUBLIC.equals(privacy);
    }

    public boolean isPrivate() {
        return PRIVACY_PRIVATE.equals(privacy);
    }

    public ConversationContext asContext() {
        return Context.conversation(cloudId, id);
    }

}