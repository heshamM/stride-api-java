package com.atlassian.stride.api.model;

import com.atlassian.adf.Document;
import lombok.Value;

/**
 * Maps the message object when sending a message.
 */
@Value(staticConstructor = "of")
public class SendMessageRequest {

    private Document body;

}
