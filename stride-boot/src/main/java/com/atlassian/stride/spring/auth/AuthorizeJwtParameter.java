package com.atlassian.stride.spring.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Secure controller by verifying the JWT in the request query parameter and injects the corresponding
 * {@link com.atlassian.stride.config.ContextConfig} in the current context.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorizeJwtParameter {

}
