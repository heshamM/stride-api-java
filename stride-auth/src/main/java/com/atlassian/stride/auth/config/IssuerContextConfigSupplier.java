package com.atlassian.stride.auth.config;

import com.atlassian.stride.config.ContextConfig;

import java.util.Optional;

/**
 * Provides credentials according to the issuer.
 */
public interface IssuerContextConfigSupplier {

    Optional<ContextConfig> forIssuer(String iss);

}
