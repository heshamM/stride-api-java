package com.atlassian.stride.api.request.message;

import com.atlassian.adf.Document;
import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.EntityCreatedResponse;
import com.atlassian.stride.api.model.SendMessageRequest;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.context.UserContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * Send a message in text or Atlassian Document Format ({@link Document}).
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
@Slf4j
public class SendMessage {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String text;
    private Document document;

    /**
     * Send a message to a conversation
     */
    public CompletableFuture<EntityCreatedResponse> toConversation(ConversationContext conversation) {
        return send(StrideEndpoint.message.send.toConversation(conversation));
    }

    /**
     * Send a message to an user
     */
    public CompletableFuture<EntityCreatedResponse> toUser(UserContext user) {
        return send(StrideEndpoint.message.send.toUser(user));
    }

    private CompletableFuture<EntityCreatedResponse> send(String endpointPath) {
        Objects.requireNonNull(text == null ? document : text, "text | document");

        return apiRequest.build(EntityCreatedResponse.class, b -> b
                .body(text != null ? Document.create().paragraph(text) : SendMessageRequest.of(document))
                .acceptJson()
                .scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION)
                .expect(HttpStatus.SC_CREATED))
                .post(apiRequest.uri(endpointPath))
                .whenComplete((a, b) -> log.debug("Created message with id {}", a.getId()));
    }

}