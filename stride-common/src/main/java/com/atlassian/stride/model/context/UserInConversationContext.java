package com.atlassian.stride.model.context;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Defines the context for a user in a conversation.
 *
 * @see Context
 */
@JsonDeserialize(as = ContextImpl.class)
public interface UserInConversationContext extends UserContext, ConversationContext {

}
