package com.atlassian.adf;

import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.Paragraph;
import com.atlassian.adf.block.codeblock.Language;
import com.atlassian.adf.block.list.BulletList;
import com.atlassian.adf.block.list.ListItem;
import com.atlassian.adf.inline.Text;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeFilterTest {

    private Document create() {
        return Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!").hardBreak())
                .h1(p -> p.text("Title"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .note(p -> p.paragraph("note"));
    }

    @Test
    void filterParagraphs() throws Exception {
        Document doc = create();
        Heading h = (Heading) doc.children().get(1);
        BulletList b = (BulletList) doc.children().get(2);

        doc.filter((node, parent) -> !(node instanceof Paragraph), false);

        assertEquals(6, doc.children().size());
        assertEquals(h, doc.children().get(0));

        assertEquals(0, b.children().get(0).children().size());
        assertEquals(0, b.children().get(1).children().size());
    }

    @Test
    void filterText() throws Exception {
        Document doc = create();
        Heading h = (Heading) doc.children().get(1);
        Paragraph p = (Paragraph) doc.children().get(0);

        doc.filter((node, parent) -> !(node instanceof Text), false);

        assertEquals(7, doc.children().size());
        assertEquals(0, h.children().size());
        assertEquals(1, p.children().size());
    }

    @Test
    void filterListItemRecursively() throws Exception {
        Document doc = create();

        doc.filter((node, parent) -> !(node instanceof ListItem), true);

        assertEquals(5, doc.children().size());
    }

    @Test
    void filterTextRecursively() throws Exception {
        Document doc = create();
        Paragraph p = (Paragraph) doc.children().get(0);

        doc.filter((node, parent) -> !(node instanceof Text), true);

        assertEquals(1, doc.children().size());
        assertEquals(p, doc.children().get(0));
        assertEquals(1, p.children().size());
    }

    @Test
    void filterAll() throws Exception {
        Document doc = create();
        doc.filter((node, parent) -> false, true);
        assertEquals(0, doc.children().size());
    }

}
