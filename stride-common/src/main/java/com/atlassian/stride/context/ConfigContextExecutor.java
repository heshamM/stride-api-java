package com.atlassian.stride.context;

import com.atlassian.stride.config.ContextConfig;

import java.util.concurrent.Executor;

/**
 * Wraps a delegated executor, propagating the configuration context and also the very executor instance being
 * used to all tasks so they can use the same context automatically even those which are not wrapped.
 */
public class ConfigContextExecutor implements Executor {

    private final Executor delegate;
    private final ContextConfig config;

    public ConfigContextExecutor(Executor delegate, ContextConfig config) {
        this.delegate = delegate;
        this.config = config;
    }

    @Override
    public void execute(Runnable command) {
        delegate.execute(ContextPassingRunner.withContext(command, config, this));
    }

}
