package com.atlassian.adf.remoterenderer;

import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.DefaultCachedTokenGenerator;
import com.atlassian.stride.auth.DefaultTokenGenerator;
import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.config.DefaultContextConfig;
import com.atlassian.stride.config.EnvironmentConfig;
import com.atlassian.stride.request.common.ClientUriBuilder;
import com.atlassian.stride.request.httpclient.HttpClientRequestProvider;
import lombok.val;

public class RendererHelper {

    private static String clientId = System.getenv("STRIDE_TEST_APP_CLIENT_ID");
    private static String secret = System.getenv("STRIDE_TEST_APP_SECRET");
    private static EnvironmentConfig env = EnvironmentConfig.staging;
    private static CachedTokenGenerator tokenGenerator = new DefaultCachedTokenGenerator(
            DefaultTokenGenerator.builder().requestProvider(HttpClientRequestProvider.def()).build());

    public static String renderHtml(Object document) throws FailedToObtainTokenException {
        val p = HttpClientRequestProvider.def();
        val header = tokenGenerator
                .generate(DefaultContextConfig.of(clientId, secret, env))
                .getAuthorizationHeaderValue();
        return p.builder()
                .header("Authorization", header)
                .acceptHtml()
                .body(document)
                .build(String.class)
                .post(ClientUriBuilder.of(env.apiUrl()).endpoint("pf-editor-service/render").build());
    }

}
