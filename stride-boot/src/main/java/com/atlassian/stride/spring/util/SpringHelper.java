package com.atlassian.stride.spring.util;

import lombok.val;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Map;

/**
 * Helper methods related to Spring Framework
 */
@ParametersAreNonnullByDefault
public class SpringHelper {

    /**
     * Given a method, finds a specific annotation in the method itself, in the class, or - if it's an override - in its
     * super methods.
     */
    public static <A extends Annotation> A getMethodOrClassAnnotation(HandlerMethod handlerMethod, Class<A> annotationType) {
        A ann = handlerMethod.getMethodAnnotation(annotationType);
        if (ann == null) {
            return handlerMethod.getBeanType().getAnnotation(annotationType);
        }
        return ann;
    }

    /**
     * Extract path variables
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    public static Map<String, String> getPathVariables(HttpServletRequest httpServletRequest) {
        val map = (Map<String, String>) httpServletRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        return map != null ? map : Collections.emptyMap();
    }

}
