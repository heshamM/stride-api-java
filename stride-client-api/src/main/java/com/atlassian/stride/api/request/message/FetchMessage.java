package com.atlassian.stride.api.request.message;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.MessageWithLinks;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch a message
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class FetchMessage {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String id;

    /**
     * Fetch a message
     */
    public CompletableFuture<MessageWithLinks> from(ConversationContext conversationContext) {
        return apiRequest.build(MessageWithLinks.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uri(StrideEndpoint.message.fetch.one(conversationContext, id)));
    }

}