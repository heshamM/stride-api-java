package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.exception.ContextConfigNotAvailableException;
import com.atlassian.stride.spring.config.model.AppConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Loads your app configurations from your {@code application.yaml}.
 * <p>
 * Notice you can load multiple configurations if you have multiple "instances" of you App created in DAC
 * (developer.atlassian.com). In this case, your App service will work in a multi-tenanted fashion, so take care with
 * context-bound operations.
 * <p>
 * For instance, each request will verify the incoming JWT token and tie the request with the specific App instance
 * according to its client Id. Then, calls to the API within that request will use outgoing JWT token generated
 * using the credentials for the same client.
 * <p>
 * Otherwise you can specify a single {@code instance} entry in your {@code application.yaml} with your App attributes,
 * it'll tie all operations with the provided credentials.
 */
@Component
@AllArgsConstructor
@Slf4j
public class RequestBoundedAppConfigSupplier {

    private final AppConfigSupplier appConfigSupplier;
    private final RequestBoundedContextConfigSupplier requestBoundedContextConfigSupplier;

    public AppConfig get() {
        return appConfigSupplier.byClientId(requestBoundedContextConfigSupplier.get().clientId())
                .orElseThrow(ContextConfigNotAvailableException::new);
    }

}
