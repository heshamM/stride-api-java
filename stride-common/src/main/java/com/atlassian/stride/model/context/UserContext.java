package com.atlassian.stride.model.context;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Nonnull;

/**
 * Defines the context for a user.
 *
 * @see Context
 */
@JsonDeserialize(as = ContextImpl.class)
public interface UserContext extends SiteContext {

    @Nonnull
    String getUserId();

}
