package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * User details as in https://developer.atlassian.com/cloud/stride/learning/getting-user-details/
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserDetail extends UnknownPropertiesSupport {

    /**
     * Unique Atlassian Account Identifier for user. Unique across all sites.
     */
    private String id;

    /**
     * The account name; this is what the user provides as their user name when they log in
     */
    private String userName;
    private String nickName;
    private Boolean active;

    /**
     * An object containing constituents of the user’s name
     */
    private Name name;

    /**
     * The name to use where displayed in the UI
     */
    private String displayName;

    /**
     * A list of email addresses associated with the account.
     */
    private List<Email> emails;
    private Map<String, Object> meta;
    private String title;
    private String timezone;

    /**
     * List of avatars associated with the account
     */
    private List<Photo> photos;

    @JsonIgnore
    public Optional<String> primaryEmail() {
        return Optional.ofNullable(emails)
                .flatMap(l -> l.stream()
                        .filter(e -> Boolean.TRUE.equals(e.primary))
                        .findFirst()
                        .map(e -> e.value));
    }

    @JsonIgnore
    public Optional<String> primaryAvatar() {
        return Optional.ofNullable(photos)
                .flatMap(l -> l.stream()
                        .filter(e -> Boolean.TRUE.equals(e.primary))
                        .findFirst()
                        .map(e -> e.value));
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class Photo extends UnknownPropertiesSupport {
        private Boolean primary;
        private String value;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class Email extends UnknownPropertiesSupport {
        private Boolean primary;
        private String value;
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class Name extends UnknownPropertiesSupport {
        private String familyName;
        private String givenName;
        private String formatted;
    }

}

