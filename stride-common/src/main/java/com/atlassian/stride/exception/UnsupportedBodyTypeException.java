package com.atlassian.stride.exception;

/**
 * Thrown when trying to build a request but the object provided as a body cannot be serialized in the request.
 */
public class UnsupportedBodyTypeException extends RuntimeException {

    public UnsupportedBodyTypeException(String message) {
        super(message);
    }

    public UnsupportedBodyTypeException(String message, Throwable cause) {
        super(message, cause);
    }

}
