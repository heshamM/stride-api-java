package com.atlassian.stride.request;

/**
 * Generic request helper without a response type.
 */
public interface UntypedRequest extends Request<Void> {

}
