package com.atlassian.adf;

import com.atlassian.adf.inline.Text;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeRemoveTest {

    private Document createNode() {
        return Document.create()
                .paragraph(p -> p.text("Here"))
                .h1("Title")
                .paragraph(p -> p.link("A link", "http://google.com").text(":"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("Here"))
                        .item(i -> i.paragraph("List")))
                .note(p -> p.paragraph("note"));
    }

    @Test
    void removeTextNodes() throws Exception {
        Document doc = createNode();
        List<Node> textNodesWithHereWord = doc.findAll((node, parent) -> (node instanceof Text) && "Here".equals(node.text()));
        assertEquals(2, textNodesWithHereWord.size());
        doc.removeAll(textNodesWithHereWord, false);
        assertEquals(5, doc.children().size());
        assertEquals(0, ((NodeContainer<Node>) doc.children().get(0)).children().size());
    }

    @Test
    void removeTextNodesAndCleanUp() throws Exception {
        Document doc = createNode();
        List<Node> textNodesWithHereWord = doc.findAll((node, parent) -> (node instanceof Text) && "Here".equals(node.text()));
        assertEquals(2, textNodesWithHereWord.size());
        doc.removeAll(textNodesWithHereWord, true);
        assertEquals(4, doc.children().size());
        assertEquals(1, ((NodeContainer<Node>) doc.children().get(2)).children().size());
    }

}
