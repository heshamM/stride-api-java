package com.atlassian.stride.api.request.message;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.MessageListWithLinks;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch message history
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
public class MessageHistory {

    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Number of messages to fetch: 1 to 75
     */
    @Setter
    private Integer limit;
    private String beforeMessage;
    private String afterMessage;
    private Instant beforeTimestamp;
    private Instant afterTimestamp;

    /**
     * Filter messages before this message Id
     */
    public MessageHistory before(String messageId) {
        this.beforeMessage = messageId;
        return this;
    }

    /**
     * Filter messages after this message Id
     */
    public MessageHistory after(String messageId) {
        this.afterMessage = messageId;
        return this;
    }

    /**
     * Filter messages before this timestamp
     */
    public MessageHistory before(Instant timestamp) {
        this.beforeTimestamp = timestamp;
        return this;
    }

    /**
     * Filter messages after this timestamp
     */
    public MessageHistory after(Instant timestamp) {
        this.afterTimestamp = timestamp;
        return this;
    }

    /**
     * Fetch message history
     */
    public CompletableFuture<MessageListWithLinks> from(ConversationContext conversationContext) {
        return apiRequest.build(MessageListWithLinks.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uriBuilder()
                        .endpoint(StrideEndpoint.message.fetch.history(conversationContext))
                        .param("limit", limit)
                        .param("beforeMessage", beforeMessage)
                        .param("beforeTimestamp", beforeTimestamp)
                        .param("afterMessage", afterMessage)
                        .param("afterTimestamp", afterTimestamp)
                        .build());
    }

}