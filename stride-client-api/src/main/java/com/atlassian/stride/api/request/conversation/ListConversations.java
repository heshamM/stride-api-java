package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.ConversationGetAllResponse;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.request.RequestBuilder;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * List conversations using pagination
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class ListConversations {

    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    
    /**
     * Include private conversations into response. Defaults to {@code true}
     */
    private Boolean includePrivate;
    
    /**
     * Include archived conversations into response. Defaults to {@code false}
     */
    private Boolean includeArchived;
    
    /**
     * A string containing full or part of conversation's name. May be empty
     */
    private String query;
    
    /**
     * The cursor is a string, that you can use to scroll through the data set to get more results
     */
    private String cursor;
    
    /**
     * A sort order of the conversations lists. Supported values: {@code asc} or {@code desc}
     */
    private String sort;
    
    /**
     * Number of conversations to fetch: 1 to 75
     */
    private Integer limit;

    /**
     * Sort conversation name ascending
     */
    public ListConversations asc() {
        this.sort = SORT_ASC;
        return this;
    }

    /**
     * Sort conversation name descending
     */
    public ListConversations desc() {
        this.sort = SORT_DESC;
        return this;
    }

    /**
     * List conversation from one site
     */
    public CompletableFuture<ConversationGetAllResponse> from(String cloudId) {
        Objects.requireNonNull(cloudId, "cloudId");

        return apiRequest.build(ConversationGetAllResponse.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uriBuilder()
                        .endpoint(StrideEndpoint.conversation.fetch.list(cloudId))
                        .param("include-private", includePrivate)
                        .param("include-archived", includeArchived)
                        .param("limit", limit)
                        .param("query", query)
                        .param("cursor", cursor)
                        .param("sort", sort)
                        .build());
    }

}