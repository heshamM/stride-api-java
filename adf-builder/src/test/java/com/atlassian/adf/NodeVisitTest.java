package com.atlassian.adf;

import com.atlassian.adf.block.BlockQuote;
import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.Panel;
import com.atlassian.adf.block.Paragraph;
import com.atlassian.adf.block.codeblock.CodeBlock;
import com.atlassian.adf.block.codeblock.Language;
import com.atlassian.adf.block.list.BulletList;
import com.atlassian.adf.block.list.ListItem;
import com.atlassian.adf.block.list.OrderedList;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class NodeVisitTest {

    @Test
    void visitWithConsumer() throws Exception {
        Document doc = Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!").hardBreak())
                .h1(p -> p.text("Title"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .note(p -> p.paragraph("note"));
        List<Node> visitedNodes = new ArrayList<>();
        List<Node> parentNodes = new ArrayList<>();
        doc.visitAll((node, parent) -> {
            visitedNodes.add(node);
            parentNodes.add(parent);
            return true;
        });

        assertEquals(30, visitedNodes.size());
        assertEquals(30, parentNodes.size());

        assertEquals(doc, visitedNodes.get(0));
        assertNull(parentNodes.get(0));

        List<BlockNode> children = doc.children();
        Paragraph p1 = (Paragraph) children.get(0);

        assertEquals(p1, visitedNodes.get(1));
        assertEquals(doc, parentNodes.get(1));
        List<InlineNode> p1Children = p1.children();
        assertEquals(p1Children.get(0), visitedNodes.get(2));
        assertEquals(p1, parentNodes.get(2));
        assertEquals(p1Children.get(1), visitedNodes.get(3));
        assertEquals(p1, parentNodes.get(3));
        assertEquals(p1Children.get(2), visitedNodes.get(4));
        assertEquals(p1, parentNodes.get(4));
        assertEquals(p1Children.get(3), visitedNodes.get(5));
        assertEquals(p1, parentNodes.get(5));

        Heading h1 = (Heading) children.get(1);
        assertEquals(h1, visitedNodes.get(6));
        assertEquals(doc, parentNodes.get(6));
        assertEquals(h1.children().get(0), visitedNodes.get(7));
        assertEquals(h1, parentNodes.get(7));

        BulletList b1 = (BulletList) children.get(2);
        List<ListItem> b1Children = b1.children();
        assertEquals(b1, visitedNodes.get(8));
        assertEquals(doc, parentNodes.get(8));
        ListItem i1 = b1Children.get(0);
        assertEquals(i1, visitedNodes.get(9));
        assertEquals(i1.children().get(0), visitedNodes.get(10));
        assertEquals(((Paragraph) i1.children().get(0)).children().get(0), visitedNodes.get(11));
        ListItem i2 = b1Children.get(1);
        assertEquals(i2, visitedNodes.get(12));
        assertEquals(i2.children().get(0), visitedNodes.get(13));
        assertEquals(((Paragraph) i2.children().get(0)).children().get(0), visitedNodes.get(14));

        OrderedList o1 = (OrderedList) children.get(3);
        List<ListItem> o1Children = o1.children();
        assertEquals(o1, visitedNodes.get(15));
        assertEquals(doc, parentNodes.get(15));
        ListItem i3 = o1Children.get(0);
        assertEquals(i3, visitedNodes.get(16));
        assertEquals(i3.children().get(0), visitedNodes.get(17));
        assertEquals(((Paragraph) i3.children().get(0)).children().get(0), visitedNodes.get(18));
        ListItem i4 = o1Children.get(1);
        assertEquals(i4, visitedNodes.get(19));
        assertEquals(i4.children().get(0), visitedNodes.get(20));
        assertEquals(((Paragraph) i4.children().get(0)).children().get(0), visitedNodes.get(21));

        BlockQuote q1 = (BlockQuote) children.get(4);
        assertEquals(q1, visitedNodes.get(22));
        assertEquals(doc, parentNodes.get(22));
        assertEquals(q1.children().get(0), visitedNodes.get(23));
        assertEquals(((Paragraph) q1.children().get(0)).children().get(0), visitedNodes.get(24));

        CodeBlock c1 = (CodeBlock) children.get(5);
        assertEquals(c1, visitedNodes.get(25));
        assertEquals(doc, parentNodes.get(25));
        assertEquals(c1.children().get(0), visitedNodes.get(26));
        assertEquals(c1, parentNodes.get(26));

        Panel panel = (Panel) children.get(6);
        assertEquals(panel, visitedNodes.get(27));
        assertEquals(doc, parentNodes.get(27));
        assertEquals(panel.children().get(0), visitedNodes.get(28));
        assertEquals(panel, parentNodes.get(28));
        assertEquals(((Paragraph) panel.children().get(0)).children().get(0), visitedNodes.get(29));
        assertEquals(panel.children().get(0), parentNodes.get(29));
    }

    @Test
    void visitWithInterruptingFunction() throws Exception {
        Document doc = Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!").hardBreak())
                .h1(p -> p.text("Title"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .note(p -> p.paragraph("note"));
        List<Node> visitedNodes = new ArrayList<>();
        List<Node> parentNodes = new ArrayList<>();
        doc.visitAll((node, parent) -> {
            visitedNodes.add(node);
            parentNodes.add(parent);
            return !(node instanceof Heading);
        });

        assertEquals(7, visitedNodes.size());
        assertEquals(7, parentNodes.size());

        assertEquals(doc, visitedNodes.get(0));
        assertNull(parentNodes.get(0));

        List<BlockNode> children = doc.children();
        Paragraph p1 = (Paragraph) children.get(0);

        assertEquals(p1, visitedNodes.get(1));
        assertEquals(doc, parentNodes.get(1));
        List<InlineNode> p1Children = p1.children();
        assertEquals(p1Children.get(0), visitedNodes.get(2));
        assertEquals(p1, parentNodes.get(2));
        assertEquals(p1Children.get(1), visitedNodes.get(3));
        assertEquals(p1, parentNodes.get(3));
        assertEquals(p1Children.get(2), visitedNodes.get(4));
        assertEquals(p1, parentNodes.get(4));
        assertEquals(p1Children.get(3), visitedNodes.get(5));
        assertEquals(p1, parentNodes.get(5));

        Heading h1 = (Heading) children.get(1);
        assertEquals(h1, visitedNodes.get(6));
        assertEquals(doc, parentNodes.get(6));
    }

}
