package com.atlassian.stride.model.webhooks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Represents an user object containing just its Id as generally used in WebHooks.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserId extends UnknownPropertiesSupport {

    private String id;

}
