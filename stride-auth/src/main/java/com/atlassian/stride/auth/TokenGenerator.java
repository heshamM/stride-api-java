package com.atlassian.stride.auth;

import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.auth.model.RefreshTokenResponse;
import com.atlassian.stride.config.ContextConfig;

/**
 * Generates a token for the Stride API.
 */
public interface TokenGenerator {
    
    RefreshTokenResponse generate(ContextConfig contextConfig) throws FailedToObtainTokenException;

}
