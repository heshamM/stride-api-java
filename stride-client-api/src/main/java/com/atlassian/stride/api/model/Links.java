package com.atlassian.stride.api.model;

import java.util.HashMap;

/**
 * Link collection whose keys are titles and values URLs.
 */
public class Links extends HashMap<String, String> {

}

