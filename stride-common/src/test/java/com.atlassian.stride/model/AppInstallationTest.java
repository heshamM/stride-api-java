package com.atlassian.stride.model;

import com.atlassian.stride.model.webhooks.AppInstallation;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AppInstallationTest {

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testInstallation() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/app-installation-payload.json"), AppInstallation.class);
        assertThat(a.getKey(), is("pinned-messages-staging-24898a3a-fbbe-4bdc-aede-564a16940c2f"));
        assertThat(a.isInstallation(), is(true));
        assertThat(a.other().get("blah"), is(1));
    }

    @Test
    public void testUninstallation() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/app-uninstallation-payload.json"), AppInstallation.class);
        assertThat(a.getKey(), is("pinned-messages-staging-24898a3a-fbbe-4bdc-aede-564a16940c2f"));
        assertThat(a.isUninstallation(), is(true));
    }

    @Test
    public void testUnknownPropertiesAreCorrectlyMapped() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/app-installation-payload.json"), AppInstallation.class);
        assertThat(a.other().get("blah"), is(1));
    }

}
