package com.atlassian.stride.request.common;

import com.atlassian.stride.request.AsyncRequest;
import com.atlassian.stride.request.Request;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Wraps a regular {@link Request} so that it's executed asynchronously using {@link CompletableFuture}s to manage
 * the execution.
 */
@AllArgsConstructor
@ParametersAreNonnullByDefault
public class DefaultAsyncRequest<R> implements AsyncRequest<R> {

    @NonNull
    private final Executor executor;
    @NonNull
    private final Request<R> delegate;

    @Override
    public CompletableFuture<R> post(URI uri) {
        return CompletableFuture.supplyAsync(() -> delegate.post(uri), executor);
    }

    @Override
    public CompletableFuture<R> put(URI uri) {
        return CompletableFuture.supplyAsync(() -> delegate.put(uri), executor);
    }

    @Override
    public CompletableFuture<R> patch(URI uri) {
        return CompletableFuture.supplyAsync(() -> delegate.patch(uri), executor);
    }

    @Override
    public CompletableFuture<R> get(URI uri) {
        return CompletableFuture.supplyAsync(() -> delegate.get(uri), executor);
    }

    @Override
    public CompletableFuture<R> delete(URI uri) {
        return CompletableFuture.supplyAsync(() -> delegate.delete(uri), executor);
    }

}
