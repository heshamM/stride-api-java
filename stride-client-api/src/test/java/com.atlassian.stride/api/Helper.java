package com.atlassian.stride.api;

import com.atlassian.stride.config.DefaultContextConfig;
import com.atlassian.stride.config.EnvironmentConfig;

/**
 * Reads environment variables for integration tests.
 */
public class Helper {

    public static String userId() {
        return System.getenv("STRIDE_TEST_USER_ID");
    }

    public static String userEmail() {
        return System.getenv("STRIDE_TEST_USER_EMAIL");
    }

    public static String cloudId() {
        return System.getenv("STRIDE_TEST_CLOUD_ID");
    }

    public static String conversationId() {
        return System.getenv("STRIDE_TEST_CONVERSATION_ID");
    }

    public static String defaultConversationName() {
        return System.getenv("STRIDE_TEST_CONVERSATION_NAME");
    }

    public static String clientId() {
        return System.getenv("STRIDE_TEST_APP_CLIENT_ID");
    }

    public static String secret() {
        return System.getenv("STRIDE_TEST_APP_SECRET");
    }

    static StrideClient client() {
        return StrideClientSupplier.builder().withDefaults().build()
                .withConfig(() -> DefaultContextConfig.of(Helper.clientId(), Helper.secret(), EnvironmentConfig.production));
    }

}
