package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Title extends UnknownPropertiesSupport {

    @Nonnull
    private String text;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private User user;

    private Title(@JsonProperty("text") String text) {
        this.text = text;
    }

    public Title user(String url, String label) {
        return user(User.user(Icon.icon(url, label)));
    }

    public Title user(String iconUrl, String iconLabel, String userId) {
        return user(User.user(Icon.icon(iconUrl, iconLabel)).id(userId));
    }

    public static Title title(String text) {
        return new Title(text);
    }

}