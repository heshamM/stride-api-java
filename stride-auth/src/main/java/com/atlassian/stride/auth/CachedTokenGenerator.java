package com.atlassian.stride.auth;

/**
 * Add caching on top of {@link DefaultTokenGenerator}.
 */
public interface CachedTokenGenerator extends TokenGenerator {

    /**
     * Makes the request to generate the token, cleaning the cache first.
     */
    void invalidate(String clientId);

}
