package com.atlassian.adf;

import com.atlassian.adf.inline.Emoji;
import com.atlassian.adf.inline.HardBreak;
import com.atlassian.adf.inline.Mark;
import com.atlassian.adf.inline.Mention;
import com.atlassian.adf.inline.Text;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Consumer;

/**
 * Represents a node that contains child inline nodes.
 */
@ParametersAreNonnullByDefault
public interface InlineNodeContainer<T extends InlineNodeContainer> extends NodeContainer<InlineNode> {

    T text(Text node);

    T text(String text);

    T text(String text, Consumer<Text> consumer);

    T text(String text, Mark... marks);

    T em(String text);

    T strong(String text);

    T code(String text);

    T strike(String text);

    T sub(String text);

    T sup(String text);

    T underline(String text);

    T link(String text, String href);

    T color(String text, String hexColor);

    T hardBreak(HardBreak node);

    T hardBreak();

    T mention(Mention node);

    T mention(String id, String text);

    T emoji(Emoji node);

    T emoji(String shortName);

    T emoji(String shortName, String text);

    T emoji(String shortName, String text, String id);

    T emoji(String shortName, Consumer<Emoji> consumer);

}
