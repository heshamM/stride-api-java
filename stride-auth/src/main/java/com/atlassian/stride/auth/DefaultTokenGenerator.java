package com.atlassian.stride.auth;

import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.auth.model.RefreshTokenRequest;
import com.atlassian.stride.auth.model.RefreshTokenResponse;
import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.request.RequestProvider;
import com.atlassian.stride.request.common.ClientUriBuilder;
import com.nimbusds.jwt.SignedJWT;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.text.ParseException;

/**
 * General purpose class that abstracts the request to the authorization API that generates tokens to be used
 * with the Stride API.
 */
@Slf4j
@RequiredArgsConstructor
@Builder
public class DefaultTokenGenerator implements TokenGenerator {

    @lombok.NonNull
    private final RequestProvider requestProvider;

    /**
     * Makes the request to generate the token
     */
    @Override
    public RefreshTokenResponse generate(ContextConfig contextConfig) throws FailedToObtainTokenException {
        val req = RefreshTokenRequest.builder()
                .client_id(contextConfig.clientId())
                .client_secret(contextConfig.secret())
                .grant_type("client_credentials")
                .build();

        try {
            val resp = requestProvider.builder()
                    .body(req)
                    .build(RefreshTokenResponse.class)
                    .post(ClientUriBuilder.of(contextConfig.env().apiUrl()).endpoint("/oauth/token").build());
            val signedJwt = SignedJWT.parse(resp.getAccess_token());
            log.debug("JWT headers: {} / payload: {}", signedJwt.getHeader(), signedJwt.getPayload());
            return resp;
        } catch (ParseException e) {
            // Invalid plain JOSE object encoding
            throw new FailedToObtainTokenException("Failed to decode token!", e);
        }
    }
    
}
