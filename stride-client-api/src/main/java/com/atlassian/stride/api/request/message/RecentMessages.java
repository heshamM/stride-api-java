package com.atlassian.stride.api.request.message;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.MessageListWithLinks;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch recent messages
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
@Setter
public class RecentMessages {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Number of messages to fetch: 1 to 75
     */
    private Integer limit;

    /**
     * Fetch recent messages
     */
    public CompletableFuture<MessageListWithLinks> from(ConversationContext conversationContext) {
        return apiRequest.build(MessageListWithLinks.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uriBuilder()
                        .endpoint(StrideEndpoint.message.fetch.recent(conversationContext))
                        .param("limit", limit)
                        .build());
    }

}