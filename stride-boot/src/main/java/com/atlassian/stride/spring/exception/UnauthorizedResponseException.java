package com.atlassian.stride.spring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown when the current incoming request is returning the Unauthorized HTTP status: 401
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedResponseException extends RuntimeException {
}
