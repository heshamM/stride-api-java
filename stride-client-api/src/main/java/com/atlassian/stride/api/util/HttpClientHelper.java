package com.atlassian.stride.api.util;

import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.request.JwtSigningRequestInterceptor;
import com.atlassian.stride.auth.request.TokenRefreshingRetryHandler;
import com.atlassian.stride.request.httpclient.HttpClientRequestProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultServiceUnavailableRetryStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public class HttpClientHelper {

    public static HttpClientBuilder createRegularHttpClientBuilder() {
        val cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(200);
        cm.setDefaultMaxPerRoute(20);
        return HttpClients.custom()
                .setServiceUnavailableRetryStrategy(new DefaultServiceUnavailableRetryStrategy(2, 1000))
                .setRetryHandler(DefaultHttpRequestRetryHandler.INSTANCE)
                .useSystemProperties()
                .setConnectionManager(cm);
    }

    public static HttpClientBuilder createJwtSigningHttpClientBuilder(
            ObjectMapper objectMapper,
            CachedTokenGenerator cachedTokenGenerator) {
        return decorateWithTokenRefreshenerRetryHandler(
                decorateWithJwtSigningInterceptor(
                        createRegularHttpClientBuilder(),
                        createRegularHttpClientBuilder().build(),
                        objectMapper,
                        cachedTokenGenerator
                ),
                cachedTokenGenerator);
    }

    public static HttpClientBuilder decorateWithJwtSigningInterceptor(
            HttpClientBuilder httpClientBuilder,
            CloseableHttpClient undecoratedClient,
            ObjectMapper objectMapper,
            CachedTokenGenerator cachedTokenGenerator) {
        return httpClientBuilder.addInterceptorLast(JwtSigningRequestInterceptor.of(
                cachedTokenGenerator,
                HttpClientRequestProvider.of(undecoratedClient, objectMapper)));
    }

    public static HttpClientBuilder decorateWithTokenRefreshenerRetryHandler(
            HttpClientBuilder httpClientBuilder,
            CachedTokenGenerator cachedTokenGenerator) {
        return httpClientBuilder.setRetryHandler(TokenRefreshingRetryHandler.of(
                cachedTokenGenerator,
                DefaultHttpRequestRetryHandler.INSTANCE));
    }


}
