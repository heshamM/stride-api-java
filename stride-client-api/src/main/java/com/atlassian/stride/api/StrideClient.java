package com.atlassian.stride.api;

import com.atlassian.adf.Document;
import com.atlassian.stride.api.async.Delayed;
import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.model.Message;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.api.request.Configuration;
import com.atlassian.stride.api.request.FetchMedia;
import com.atlassian.stride.api.request.FetchUser;
import com.atlassian.stride.api.request.Glance;
import com.atlassian.stride.api.request.UploadMedia;
import com.atlassian.stride.api.request.conversation.ArchiveConversation;
import com.atlassian.stride.api.request.conversation.CreateConversation;
import com.atlassian.stride.api.request.conversation.FetchConversation;
import com.atlassian.stride.api.request.conversation.FetchRoster;
import com.atlassian.stride.api.request.conversation.ListConversations;
import com.atlassian.stride.api.request.conversation.UnarchiveConversation;
import com.atlassian.stride.api.request.conversation.UpdateConversation;
import com.atlassian.stride.api.request.message.FetchMessage;
import com.atlassian.stride.api.request.message.MessageContext;
import com.atlassian.stride.api.request.message.MessageHistory;
import com.atlassian.stride.api.request.message.RecentMessages;
import com.atlassian.stride.api.request.message.RenderMessage;
import com.atlassian.stride.api.request.message.SendMessage;
import com.atlassian.stride.context.ContextConfigSupplier;
import com.atlassian.stride.context.ContextPassingRunner;
import com.atlassian.stride.exception.ContextConfigNotAvailableException;
import com.atlassian.stride.model.webhooks.MessageSent;
import com.atlassian.stride.request.RequestProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Entry point for Stride Client API.
 */
@Slf4j
@ParametersAreNonnullByDefault
@Builder
public class StrideClient {

    @lombok.NonNull
    protected final ObjectMapper objectMapper;
    @lombok.NonNull
    protected final Executor executor;
    @lombok.NonNull
    protected final ScheduledExecutorService scheduler;
    @lombok.NonNull
    protected final RequestProvider requestProvider;
    @lombok.NonNull
    protected final ContextConfigSupplier contextConfigSupplier;

    protected final MessageApi message = new MessageApi();
    protected final ConversationApi conversation = new ConversationApi();
    protected final MediaApi media = new MediaApi();
    protected final UserApi user = new UserApi();
    protected final DocumentApi document = new DocumentApi();

    /**
     * Allow delayed asynchronous executions to the API while keeping the current context.
     */
    public Delayed delayed(Duration duration) {
        Objects.requireNonNull(duration,"duration");
        return Delayed.of(scheduler, duration, contextConfigSupplier);
    }

    /**
     * Requests related to glances
     */
    public Glance glance() {
        return Glance.of(requestInfo());
    }

    /**
     * Requests related to configuration
     */
    public Configuration configuration(String key) {
        return Configuration.of(requestInfo()).key(key);
    }

    /**
     * Requests related to messages
     */
    public MessageApi message() {
        return message;
    }

    /**
     * Requests related to conversations
     */
    public ConversationApi conversation() {
        return conversation;
    }

    public class MessageApi {
        protected final FetchMessageApi fetch = new FetchMessageApi();

        /**
         * Fetch a message
         */
        public FetchMessageApi fetch() {
            return fetch;
        }

        /**
         * Render an Atlassian Document to text or HTML
         */
        public RenderMessage render(Document document) {
            return RenderMessage.of(requestInfo()).document(document);
        }

        /**
         * Send a text message
         */
        public SendMessage send(String message) {
            return SendMessage.of(requestInfo()).text(message);
        }

        /**
         * Send a formatted message
         */
        public SendMessage send(Document document) {
            return SendMessage.of(requestInfo()).document(document);
        }

        public class FetchMessageApi {
            /**
             * Fetch one message
             */
            public FetchMessage one(String messageId) {
                return FetchMessage.of(requestInfo()).id(messageId);
            }

            /**
             * Fetch message history
             */
            public MessageHistory history() {
                return MessageHistory.of(requestInfo());
            }

            /**
             * Fetch contextual message history
             */
            public MessageContext context(String messageId) {
                return MessageContext.of(requestInfo()).message(messageId);
            }

            /**
             * Fetch recent messages
             */
            public RecentMessages recent() {
                return RecentMessages.of(requestInfo());
            }
        }
    }

    public class ConversationApi {
        protected final FetchConversationApi fetch = new FetchConversationApi();
        protected final ManageConversationApi manage = new ManageConversationApi();

        /**
         * Requests related to fetching conversations
         */
        public FetchConversationApi fetch() {
            return fetch;
        }

        public class FetchConversationApi {
            /**
             * Fetch a conversation
             */
            public FetchConversation one() {
                return FetchConversation.of(requestInfo());
            }

            /**
             * List conversations using pagination
             */
            public ListConversations list() {
                return ListConversations.of(requestInfo());
            }

            /**
             * Fetch conversation roster using pagination
             */
            public FetchRoster roster() {
                return FetchRoster.of(requestInfo());
            }
        }

        /**
         * Requests related to managing messages
         */
        public ManageConversationApi manage() {
            return manage;
        }

        public class ManageConversationApi {
            /**
             * Create a conversation
             */
            public CreateConversation create() {
                return CreateConversation.of(requestInfo());
            }

            /**
             * Update a conversation
             */
            public UpdateConversation update() {
                return UpdateConversation.of(requestInfo());
            }

            /**
             * Update a conversation
             */
            public UpdateConversation update(Conversation conversation) {
                return UpdateConversation.of(requestInfo()).using(conversation);
            }

            /**
             * Archive a conversation
             */
            public ArchiveConversation archive() {
                return ArchiveConversation.of(requestInfo());
            }

            /**
             * Unarchive a conversation
             */
            public UnarchiveConversation unarchive() {
                return UnarchiveConversation.of(requestInfo());
            }
        }
    }

    /**
     * Requests related to media/resources
     */
    public MediaApi media() {
        return media;
    }

    public class MediaApi {
        /**
         * Get a media file
         */
        public FetchMedia get(String id) {
            return FetchMedia.of(requestInfo()).id(id);
        }

        /**
         * Upload a media file
         */
        public UploadMedia upload(byte[] content) {
            return UploadMedia.of(requestInfo()).content(content);
        }
    }

    /**
     * Requests related to users
     */
    public UserApi user() {
        return user;
    }

    public class UserApi {
        /**
         * Get user details
         */
        public FetchUser get() {
            return FetchUser.of(requestInfo());
        }
    }

    /**
     * Atlassian Document utilities for converting raw messages to {@link Document}.
     */
    public DocumentApi document() {
        return document;
    }

    public class DocumentApi {
        /**
         * Obtain a document from a generic {@link JsonNode}. The node has to be in the Atlassian Document Format.
         */
        public Document of(JsonNode jsonNode) throws JsonProcessingException {
            return objectMapper.treeToValue(jsonNode, Document.class);
        }

        /**
         * Obtain a document from a {@link Message}, which is used when you fetch messages using this API.
         * The message body has to be in the Atlassian Document Format.
         */
        public Document of(Message message) throws JsonProcessingException {
            return of(message.getBody());
        }

        /**
         * Obtain a document from a {@link MessageSent.Message}, which you get in WebHooks when Stride calls your
         * service. The message body has to be in the Atlassian Document Format.
         */
        public Document of(MessageSent.Message message) throws JsonProcessingException {
            return of(message.getBody());
        }

        /**
         * Obtain a document from a {@link String}. The string has to be a serialized version of the
         * Atlassian Document Format.
         */
        public Document of(String message) throws IOException {
            return objectMapper.readValue(message, Document.class);
        }
    }

    protected ApiRequestHelper requestInfo() {
        try {
            return ApiRequestHelper.of(requestProvider, ContextPassingRunner.wrap(executor, contextConfigSupplier.get()), contextConfigSupplier.get());
        } catch (ContextConfigNotAvailableException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

}
