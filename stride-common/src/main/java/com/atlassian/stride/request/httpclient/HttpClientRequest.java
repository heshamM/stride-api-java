package com.atlassian.stride.request.httpclient;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.exception.RequestErrorException;
import com.atlassian.stride.exception.UnexpectedBodyForHttpMethodException;
import com.atlassian.stride.exception.UnexpectedEmptyResponseException;
import com.atlassian.stride.exception.UnexpectedResponseStatusException;
import com.atlassian.stride.exception.UnexpectedResponseTypeException;
import com.atlassian.stride.exception.UnsupportedBodyTypeException;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.request.Request;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * {@link Request} based on Apache HTTP Client
 */
@AllArgsConstructor
@ParametersAreNonnullByDefault
@Slf4j
public class HttpClientRequest<R> implements Request<R> {

    @NonNull
    private final CloseableHttpClient client;
    @Nonnull
    private final ObjectMapper mapper;
    @Nonnull
    private final Class<R> responseType;
    @Nonnull
    private final Map<String, String> headers;
    @Nonnull
    private final List<Integer> expectedResponseStatuses;
    @Nonnull
    private final List<ContentType> accept;
    @Nullable
    private final ContentType contentType;
    @Nullable
    private final Object body;
    @Nullable
    private final ContextConfig contextConfig;
    @Nullable
    private final Scope scope;

    /**
     * Make the request and get a response with the given {@code responseType}.
     */
    @SuppressWarnings("unchecked")

    private R execute(HttpRequestBase request) {
        log.debug("{} {}", request.getMethod(), request.getURI());

        // custom headers
        headers.forEach(request::addHeader);
        // content-type
        val contentTypeList = accept.stream().map(ContentType::getMimeType).collect(Collectors.joining(", "));
        request.setHeader(HttpHeaders.ACCEPT, contentTypeList);

        // json by default if contains body
        val ct = contentType == null ? ContentType.APPLICATION_JSON.withCharset(StandardCharsets.UTF_8) : contentType;
        if (body != null || contentType != null) {
            request.setHeader(HttpHeaders.CONTENT_TYPE, ct.toString());
        }

        try {
            // set body
            if (body != null) {
                if (request instanceof HttpPost) {
                    ((HttpPost) request).setEntity(asEntity(body, ct));
                } else if (request instanceof HttpPut) {
                    ((HttpPut) request).setEntity(asEntity(body, ct));
                } else if (request instanceof HttpPatch) {
                    ((HttpPatch) request).setEntity(asEntity(body, ct));
                } else {
                    throw new UnexpectedBodyForHttpMethodException("Body not supported for method " + request.getMethod());
                }
            }

            // perform request
            HttpContext ctx = new BasicHttpContext();
            ctx = ContextConfigInHttpContext.withContext(ctx, contextConfig);
            ctx = ScopeCheckingInHttpContext.withContext(ctx, scope);
            val response = client.execute(request, ctx);

            // check status code
            val status = response.getStatusLine().getStatusCode();
            if (!expectedResponseStatuses.contains(status) && HttpStatus.SC_OK != status) {
                throw new UnexpectedResponseStatusException(expectedResponseStatuses, status);
            }

            // ignore body?
            if (Void.class.equals(responseType)) {
                if (response.getEntity() != null) {
                    try {
                        response.getEntity().getContent().close();
                    } catch (Exception e) {
                        //nothing
                    }
                }
                return null;
            }

            val e = response.getEntity();
            if (e == null ||  e.getContentLength() == 0) {
                throw new UnexpectedEmptyResponseException("Expected response of ");
            }
            ContentType contentType = ContentType.get(e);

            if (byte[].class.isAssignableFrom(responseType)) {
                return (R) asByte(e.getContent());
            }
            if (InputStream.class.isAssignableFrom(responseType)) {
                return (R) e.getContent();
            }
            if (contentType.getMimeType().equals(ContentType.APPLICATION_JSON.getMimeType())) {
                return mapper.readValue(e.getContent(), responseType);
            }
            if (String.class.isAssignableFrom(responseType)) {
                if (contentType.getCharset() == null) {
                    return (R) new String(asByte(e.getContent()), StandardCharsets.UTF_8);
                } else {
                    return (R) new String(asByte(e.getContent()), contentType.getCharset());
                }
            }

            throw new UnexpectedResponseTypeException("Expected " + responseType.getName() + " got " + contentType.toString());
        } catch (IOException e) {
            throw new RequestErrorException(e);
        }
    }

    private HttpEntity asEntity(Object o, ContentType contentType) {
        if (o instanceof String) {
            return new StringEntity((String) o, contentType);
        }
        if (byte[].class.isInstance(o)) {
            return new ByteArrayEntity((byte[]) o, contentType);
        }
        try {
            return new ByteArrayEntity(mapper.writeValueAsBytes(o), contentType);
        } catch (JsonProcessingException e) {
            throw new UnsupportedBodyTypeException("Cannot serialize object ot type " + o.getClass().getName(), e);
        }
    }

    private byte[] asByte(InputStream is) {
        val buffer = new ByteArrayOutputStream();
        val data = new byte[1024];
        int nRead;
        try {
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            return buffer.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public R post(URI uri) {
        return execute(new HttpPost(uri));
    }

    @Override
    public R put(URI uri) {
        return execute(new HttpPut(uri));
    }

    @Override
    public R patch(URI uri) {
        return execute(new HttpPatch(uri));
    }

    @Override
    public R get(URI uri) {
        return execute(new HttpGet(uri));
    }

    @Override
    public R delete(URI uri) {
        return execute(new HttpDelete(uri));
    }
}
