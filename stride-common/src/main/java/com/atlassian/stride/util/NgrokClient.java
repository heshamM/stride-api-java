package com.atlassian.stride.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.net.URL;
import java.util.Optional;

/**
 * Fetch ngrok info from a running process
 */
@Slf4j
public class NgrokClient {

    private static final String NGROK_TUNNELS_API_URL = System.getProperty("NGROK_API_URL", "http://127.0.0.1:4040/api/tunnels");

    /**
     * Fetch ngrok info from a running process with the expected payload as below:
     * <pre>
     * {@code
     * {
     *   "tunnels":[{
     *     "name":"command_line (http)",
     *     "uri":"/api/tunnels/command_line+%28http%29",
     *     "public_url":"http://7eb7114a.ngrok.io",
     *     "proto":"http",
     *     "config":{"addr":"localhost:8080","inspect":true},
     *     "metrics":{
     *       "conns":{"count":0,"gauge":0,"rate1":0,"rate5":0,"rate15":0,"p50":0,"p90":0,"p95":0,"p99":0},
     *       "http":{"count":0,"rate1":0,"rate5":0,"rate15":0,"p50":0,"p90":0,"p95":0,"p99":0}
     *     }
     *   },{
     *     "name":"command_line",
     *     "uri":"/api/tunnels/command_line",
     *     "public_url":"https://7eb7114a.ngrok.io",
     *     "proto":"https",
     *     "config":{"addr":"localhost:8080","inspect":true},
     *     "metrics":{
     *       "conns":{"count":0,"gauge":0,"rate1":0,"rate5":0,"rate15":0,"p50":0,"p90":0,"p95":0,"p99":0},
     *       "http":{"count":0,"rate1":0,"rate5":0,"rate15":0,"p50":0,"p90":0,"p95":0,"p99":0}
     *     }
     *   }],
     *   "uri":"/api/tunnels"
     * }
     * }
     * </pre>
     */
    public static Optional<String> fetchPublicUrlForPort(int port) {
        val mapper = new ObjectMapper();
        try {
            //fetch tunnel info from local ngrok process
            val root = mapper.readTree(new URL(NGROK_TUNNELS_API_URL));
            //get tunnels array
            val tunnels = root.path("tunnels");
            if (!tunnels.isArray()) {
                log.error("No tunnels running (Is ngrok running?): {}", root.asText());
                return Optional.empty();
            }
            //search for https on desired port
            for (val t : tunnels) {
                //filter https only
                if (!"https".equals(t.get("proto").asText())) {
                    continue;
                }
                //check port in config.addr node
                val addr = t.path("config").path("addr").asText();
                if (addr.isEmpty()) {
                    log.error("No local address found in configuration (Is ngrok running?): {}", root.asText());
                    return Optional.empty();
                }
                if (addr.endsWith(Integer.toString(port))) {
                    val publicUrl = t.path("public_url").asText();
                    if (publicUrl.isEmpty()) {
                        log.error("No public url found for the tunnel (Is ngrok running?): {}", root.asText());
                        return Optional.empty();
                    }
                    log.info("Public URL for service: {}", publicUrl);
                    return Optional.of(publicUrl);
                }
            }
        } catch (Throwable e) {
            log.error("ngrok not available on port {}", port, e);
        }
        log.error("No public url found in a running ngrok process. Is ngrok running?");
        return Optional.empty();
    }

}
