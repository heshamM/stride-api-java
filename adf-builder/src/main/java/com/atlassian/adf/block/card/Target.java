package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@AllArgsConstructor(staticName = "target")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Target extends UnknownPropertiesSupport {

    @Nonnull
    private String app;
    @Nonnull
    private String key;

}
