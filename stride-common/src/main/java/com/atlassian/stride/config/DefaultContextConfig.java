package com.atlassian.stride.config;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Accessors;

/**
 * Default implementation for {@link ContextConfig}
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Accessors(fluent = true)
public class DefaultContextConfig implements ContextConfig {

    @NonNull
    private final String clientId;
    @NonNull
    private final String secret;
    @NonNull
    private final EnvironmentConfig env;

    public static DefaultContextConfig of(String clientId, String secret) {
        return new DefaultContextConfig(clientId, secret, EnvironmentConfig.production);
    }

    public static DefaultContextConfig of(String clientId, String secret, EnvironmentConfig environmentConfig) {
        return new DefaultContextConfig(clientId, secret, environmentConfig);
    }

}
