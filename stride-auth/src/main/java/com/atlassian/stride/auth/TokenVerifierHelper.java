package com.atlassian.stride.auth;

import com.atlassian.stride.auth.config.IssuerContextConfigSupplier;
import com.atlassian.stride.auth.exception.InvalidTokenException;
import com.atlassian.stride.auth.model.StrideJwtToken;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.minidev.json.JSONObject;

import java.text.ParseException;
import java.util.Objects;

/**
 * General purpose routines for verifying tokens
 */
@Slf4j
@AllArgsConstructor
public class TokenVerifierHelper {

    /**
     * Given a token and a secret, parses the token and check whether it's valid, returning then the information
     * contained in the token.
     */
    public static StrideJwtToken verifyAuthorizationToken(
            String authorizationToken,
            IssuerContextConfigSupplier issuerContextConfigSupplier) throws InvalidTokenException {
        final SignedJWT signedJWT;
        try {
            signedJWT = SignedJWT.parse(authorizationToken);
            log.debug("Parsed token: {}", signedJWT.getParsedString());
        } catch (ParseException e) {
            throw new InvalidTokenException("Failed to parse token", e);
        }

        val token = toStrideJwt(signedJWT);
        val config = issuerContextConfigSupplier.forIssuer(token.getIss()).orElseThrow(() -> {
            log.error("Cannot find credentials for issuer {}", token.getIss());
            return new InvalidTokenException("Token 'iss' does not match any known issuer: " + token.getIss());
        });
        verifySignedJwt(signedJWT, config.secret());

        return token;
    }

    /**
     * Takes a valid signed token (not necessarily verified) and returns it as a {@link StrideJwtToken}.
     */
    public static StrideJwtToken toStrideJwt(SignedJWT signedJWT) throws InvalidTokenException {
        JWTClaimsSet claims;
        try {
            claims = signedJWT.getJWTClaimsSet();
        } catch (ParseException e) {
            throw new InvalidTokenException("Failed to parse claim set", e);
        }
        log.debug("Token: {}, claims: {}", signedJWT, claims);
        val builder = StrideJwtToken.builder()
                .iss(claims.getIssuer())
                .exp(claims.getExpirationTime().getTime())
                .iat(claims.getIssueTime().getTime())
                .jtw(claims.getJWTID())
                .sub(claims.getSubject());

        //fill in context if available
        JSONObject context;
        try {
            context = claims.getJSONObjectClaim("context");
        } catch (ParseException e) {
            throw new InvalidTokenException("Failed to parse claim context", e);
        }
        if (context != null) {
            val cloudId = Objects.toString(context.get("cloudId"), null);
            val resourceType = Objects.toString(context.get("resourceType"), null);
            val resourceId = Objects.toString(context.get("resourceId"), null);
            builder.context(new StrideJwtToken.Context(cloudId, resourceType, resourceId));
        }
        return builder.build();
    }

    /**
     * Verify a signed token against a secret.
     */
    public static void verifySignedJwt(SignedJWT signedJWT, String secret) throws InvalidTokenException {
        try {
            val verifier = new MACVerifier(secret);
            if (!signedJWT.verify(verifier)) {
                log.debug("Invalid token: {}", signedJWT);
                throw new InvalidTokenException("Token is invalid");
            }
            log.debug("Authorized to {} - Token: {}", signedJWT.getJWTClaimsSet().getIssuer());
        } catch (ParseException | JOSEException e) {
            throw new InvalidTokenException("Could not verify token", e);
        }
    }

}