package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

/**
 * Message is a struct that represents the current RTM message spec
 */
@Data
public class Message extends UnknownPropertiesSupport {

    private JsonNode body;
    private String conversationId;
    private String id;
    private Boolean isDeleted;
    private String revisionId;
    private String senderId;
    private String ts;
    private String updatedTs;

}

