package com.atlassian.stride.api;

import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.context.UserContext;

import javax.annotation.ParametersAreNonnullByDefault;
import java.text.MessageFormat;
import java.util.Objects;

/**
 * Defines the URLs for all available Stride endpoints statically.
 * See {@link StrideClient} for the main entry point to call the API.
 */
@ParametersAreNonnullByDefault
public interface StrideEndpoint {

    String APP_CONFIGURATION_UPDATE_URL = "/app/module/chat/conversation/chat:configuration/{0}/state";
    String GLANCE_UPDATE_URL = "/app/module/chat/conversation/chat:glance/{0}/state";
    String SEND_MESSAGE_TO_CONVERSATION_URL = "/site/{0}/conversation/{1}/message";
    String SEND_MESSAGE_TO_USER_URL = "/site/{0}/conversation/user/{1}/message";
    String GET_MESSAGE_BY_ID_URL = "/site/{0}/conversation/{1}/message/{2}";
    String GET_CONVERSATION_LIST_URL = "/site/{0}/conversation";
    String CREATE_CONVERSATION_URL = "/site/{0}/conversation";
    String UPDATE_OR_GET_CONVERSATION_DETAILS_URL = "/site/{0}/conversation/{1}";
    String ARCHIVE_CONVERSATION_URL = "/site/{0}/conversation/{1}/archive";
    String UNARCHIVE_CONVERSATION_URL = "/site/{0}/conversation/{1}/unarchive";
    String FILE_UPLOAD_URL = "/site/{0}/conversation/{1}/media";
    String FILE_GET_URL = "/site/{0}/conversation/{1}/media/{2}";
    String MESSAGE_HISTORY_URL = "/site/{0}/conversation/{1}/message";
    String MESSAGE_CONTEXTUAL_HISTORY_URL = "/site/{0}/conversation/{1}/message/{2}/context";
    String MESSAGE_LATEST_URL = "/site/{0}/conversation/{1}/message/recent";
    String GET_ROSTER_URL = "/site/{0}/conversation/{1}/roster";

    String RENDER_DOCUMENT_URL = "/pf-editor-service/render";
    String GET_USER_URL = "/scim/site/{0}/Users/{1}";

    String TOKEN_URL = "/oauth/token";

    interface glance {
        static String update(String key) {
            Objects.requireNonNull(key, "key");
            return MessageFormat.format(GLANCE_UPDATE_URL, key);
        }
    }

    interface configuration {
        static String update(String key) {
            Objects.requireNonNull(key, "key");
            return MessageFormat.format(APP_CONFIGURATION_UPDATE_URL, key);
        }
    }

    interface message {
        static String render() {
            return RENDER_DOCUMENT_URL;
        }
        interface send {
            static String toConversation(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(SEND_MESSAGE_TO_CONVERSATION_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
            static String toUser(UserContext userContext) {
                Objects.requireNonNull(userContext, "userContext");
                return MessageFormat.format(SEND_MESSAGE_TO_USER_URL, userContext.getCloudId(), userContext.getUserId());
            }
        }
        interface fetch {
            static String one(ConversationContext conversationContext, String messageId) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                Objects.requireNonNull(messageId, "messageId");
                return MessageFormat.format(GET_MESSAGE_BY_ID_URL, conversationContext.getCloudId(), conversationContext.getConversationId(), messageId);
            }
            static String history(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(MESSAGE_HISTORY_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
            static String context(ConversationContext conversationContext, String messageId) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                Objects.requireNonNull(messageId, "messageId");
                return MessageFormat.format(MESSAGE_CONTEXTUAL_HISTORY_URL, conversationContext.getCloudId(), conversationContext.getConversationId(), messageId);
            }
            static String recent(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(MESSAGE_LATEST_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
        }
    }

    interface conversation {
        interface fetch {
            static String one(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(UPDATE_OR_GET_CONVERSATION_DETAILS_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
            static String list(String cloudId) {
                Objects.requireNonNull(cloudId, "cloudId");
                return MessageFormat.format(GET_CONVERSATION_LIST_URL, cloudId);
            }
            static String roster(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(GET_ROSTER_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
        }
        interface manage {
            static String create(String cloudId) {
                Objects.requireNonNull(cloudId, "cloudId");
                return MessageFormat.format(CREATE_CONVERSATION_URL, cloudId);
            }
            static String update(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(UPDATE_OR_GET_CONVERSATION_DETAILS_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
            static String archive(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(ARCHIVE_CONVERSATION_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
            static String unarchive(ConversationContext conversationContext) {
                Objects.requireNonNull(conversationContext, "conversationContext");
                return MessageFormat.format(UNARCHIVE_CONVERSATION_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
            }
        }
    }

    interface media {
        static String get(ConversationContext conversationContext, String mediaId) {
            Objects.requireNonNull(conversationContext, "conversationContext");
            Objects.requireNonNull(mediaId, "mediaId");
            return MessageFormat.format(FILE_GET_URL, conversationContext.getCloudId(), conversationContext.getConversationId(), mediaId);
        }
        static String upload(ConversationContext conversationContext) {
            Objects.requireNonNull(conversationContext, "conversationContext");
            return MessageFormat.format(FILE_UPLOAD_URL, conversationContext.getCloudId(), conversationContext.getConversationId());
        }
    }

    interface user {
        static String get(UserContext userContext) {
            Objects.requireNonNull(userContext, "userContext");
            return MessageFormat.format(GET_USER_URL, userContext.getCloudId(), userContext.getUserId());
        }
    }

    interface auth {
        interface token {
            static String get() {
                return TOKEN_URL;
            }
        }
    }

}
