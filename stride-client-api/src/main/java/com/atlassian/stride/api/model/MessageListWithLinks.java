package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Represents a message list with links
 */
@Data
public class MessageListWithLinks extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;
    private Long limit;
    private List<Message> messages;
    private Long size;
    private Long start;

}

