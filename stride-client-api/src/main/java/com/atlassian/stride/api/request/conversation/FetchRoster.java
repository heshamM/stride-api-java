package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.model.Roster;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch a conversation roster
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
public class FetchRoster {

    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Fetch roster
     */
    public CompletableFuture<Roster> from(ConversationContext context) {
        return apiRequest.build(Roster.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uri(StrideEndpoint.conversation.fetch.roster(context)));
    }

    /**
     * Fetch roster
     */
    public CompletableFuture<Roster> from(Conversation conversation) {
        return from(conversation.asContext());
    }

}