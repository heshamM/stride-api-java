# Java Atlassian Document Format Builder

Simple library to whose purpose is to be fluent and make it safer to work with Atlassian Document Format in Java.

Feel free to contribute for improvements and bug fixes. 

## Principles

### Easy to use

A fluent interface similar to Builder Pattern (but with the real mutable object) allied to
lambdas and lots of helper methods helps you to build document with less with, 
auto-complete assistance, type-safety, and so on.

### Mutability

Nodes are mutable to facilitate transformation.

One approach with immutables was prototyped but it was becoming to hard to evolve and maintain, 
too much complex to perform transformations while maintaining a fluent and DRY-oriented API.

### Resilience

All node classes accept unknown properties so as the document format evolves you can 
access new fields through the `other` map without having to wait for a new library release. 

### Correctness

Sometimes there are divergences between the document schema, the online document builder,
the documentation, and what is actually accepted by the renderer service.

In order to avoid issues with this problem, there's an integration test available that
hits the renderer service with the various node types and fails whenever there's an HTTP
response with code 400, meaning a bad request. 

## How to use

### Build a document

A simple paragraph:

```java
Document document = Document.create()
        .paragraph(p -> p.text("Hello, ").strong("all").text("!"));
```

A complex document:

```java
Document document = Document.create()
        .paragraph(p -> p.text("Hello, ").strong("all").text("!"))
        .h1("Title")
        .paragraph(p -> p
                .text("Text ")
                .em("with ")
                .code("format")
                .text(" ")
                .strike("and")
                .text(" ")
                .underline("other")
                .text(" ")
                .sup("things")
                .sub(". "))
        .paragraph(p -> p.link("A link", "http://google.com").text(":"))
        .bulletList(l -> l
                .item(i -> i.paragraph("An unordered"))
                .item(i -> i.paragraph("List")))
        .paragraph("A:")
        .orderedList(l -> l
                .item(i -> i.paragraph("numbered"))
                .item(i -> i.paragraph("list")))
        .quote(b -> b.paragraph("Quote me"))
        .paragraph(p -> p.color("asda", "#97a0af"))
        .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
        .paragraph(p -> p
                        .text("Multi")
                        .hardBreak()
                        .text("Line")
                        .hardBreak()
                        .text("No")
                        .hardBreak()
                        .text("Paragraph"))
        .warning(p -> p.paragraph("alert"))
        .info(p -> p.paragraph("Info"))
        .tip(p -> p.paragraph("idea"))
        .note(p -> p.paragraph("note"));
```

An advanced Application Card:

```java
Document document = Document.create().card("Donovan Kolbly updated a file: applicationCard.md", c -> c.attrs()
        .title(t -> t.user("https://www.gravatar.com/avatar/c3c9338e575a73892b0f1257eb9ee997", "Donovan Kolbly"))
        .link("https://atlassian.com")
        .collapsible(true)
        .description("\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis varius mattis massa, quis ornare orci. Integer congue\nrutrum velit, quis euismod eros condimentum quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris\nlobortis nibh id odio egestas luctus. Nunc nulla lacus, congue eu nibh non, imperdiet varius lacus. Nulla sagittis\nmagna et tincidunt volutpat. Nunc augue lorem, eleifend et tempor ut, malesuada ac lorem. Praesent quis feugiat eros,\net vehicula nibh. Maecenas vehicula commodo nisi, at rutrum ipsum posuere sit amet. Integer sit amet nisl sed ligula\nconsectetur feugiat non at ligula. Cras dignissim suscipit magna at mattis. Maecenas ante leo, feugiat vestibulum velit\na, commodo finibus velit. Maecenas interdum ullamcorper velit non suscipit. Proin tempor, magna vitae dapibus laoreet,\nquam dui convallis lectus, in vestibulum arcu eros eu velit. Quisque vel dolor enim.\n")
        .context("Stride Documentation / ... / Nodes", "https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png", "stride")
        .preview("https://image.ibb.co/kkFYW5/screenshot_13.png")
        .detail(d -> d.icon("https://image.ibb.co/fUViW5/board.png", "Issue type").text("Story"))
        .detail(d -> d.badge(101, b -> b.max(99L).important()))
        .detail(d -> d.lozenge("Nearly Complete", l -> l.inprogress()))
        .detail(d -> d.title("Watchers")
                .user("https://www.gravatar.com/avatar/5db869c9686a1d191f99fc153c4d118c.jpg", "Kitty")
                .user("https://www.gravatar.com/avatar/440f0328de63d671bf337779b4eece44.jpg", "Puppy")));
``` 

### Markdown Support

This library provides limited experimental unofficial support for Markdown.

You can create an Atlassian Document from Markdown text like this:

```java
Document document = Document.fromMarkdown("hello *world*");
```

Transformation is base on [commonmark-java](https://github.com/atlassian/commonmark-java) and consists in visiting 
Markdown nodes and creating the corresponding Atlassian Document Format nodes.

Basic [Markdown formatting supported](http://commonmark.org/help/):

- `*Italic*`
- `**Bold**`
- Heading:
```markdown
Heading 1
===

Heading 2
---

### Heading 3
```
- `[link](http://atlassian.com)`
- `> Block quote`
- Unordered list:  
```markdown
* Item
* Another item
```
    
- Ordered list:  
```markdown
1. One
2. Two
```

- Horizontal rule:   
```markdown
Paragraph

---

Another paragraph
```

Currently, it does not support:

- Images: because first they would have to be uploaded using the Media API
- Inline HTML
- HTML Blocks
- Custom nodes

Unsupported formatting will be ignored by default. If you want to capture errors, 
use the second optional argument:
 
```java
Document document = Document.fromMarkdown("some unsupported markdown", true);
```

### Parsing a JSON document 

Just use regular Jackson:

```java
Document document = objectMapper.readValue(s, Document.class);
```

### Serializing document to JSON

Use regular Jackson:

```java
String s = objectMapper.writeValueAsString(document);
```

### Rendering the document

The proper way to perform document serialization is calling the renderer service at the API (`pf-editor-service/render`).
With this service you can render a document into HTML or text depending on the `Accept` HTTP header you provide.

However, for simple use cases, this API provides an unofficial way to serialize a `Document` to text:

```java
String text = doc.text();
```  

### Searching nodes

Looking for a specific node type:

```java
List<Text> nodes = document.findAll(Text.class);
```

Searching all text nodes starting with "A" (case insensitive):

```java
List<Text> nodes = doc.findAll((node, parent) -> node instanceof Text && node.text().toLowerCase().startsWith("a"));
``` 

### Visiting nodes

Processing all nodes:

```java
document.visitAll((node, parent) -> {  
        /* do something */
        return true;
    });
```

Stopping once you find what you're looking for:

```java
document.visitAll((node, parent) -> {
        if (isMyNode(node)) {
            /* do something */
            return false;
        }
        return true;
    });
```

### Filtering/removing nodes

Removing all paragraphs:

```java
doc.filter((node, parent) -> !(node instanceof Paragraph), false);
```

If you want to cleanup nodes that become empty, just pass `true` in the second parameter:

```java
doc.filter((node, parent) -> !(node instanceof Paragraph), true);
```

Removing specific nodes:

```java
Collection<Node> nodesToKill = ...
doc.removeAll(nodesToKill, false);
```

Removing specific nodes and cleanup the empty ones:

```java
Collection<Node> nodesToKill = ...
doc.removeAll(nodesToKill, true);
```

## Limitations

- [Table-related nodes](https://developer.atlassian.com/cloud/stride/apis/document/nodes/table/) are not implemented.

## References

- [Message Format documentation](https://developer.atlassian.com/cloud/stride/apis/document/structure/)
- [Json Schema](https://bitbucket.org/atlassian/atlaskit/src/3d13c3f4c2d10e38b7653f3bcc150eb4e3bbf742/packages/editor-core/dist/json-schema/v1/full.json?at=master&fileviewer=file-view-default)

