package com.atlassian.stride.model;

import lombok.Value;
import lombok.val;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents an API scope as defined at developer.atlassian.com. An App needs to be enrolled into one or more scopes
 * in order to be authorized to use their respective API endpoints. 
 */
@Value
public class Scope {
    
    public static final Scope STRIDE_PARTICIPATE_IN_CONVERSATION = new Scope("participate:conversation", "Enables your app to perform most operations via the API");
    public static final Scope STRIDE_MANAGE_CONVERSATION = new Scope("manage:conversation", "Required to create, update, archive or unarchive conversations");
    public static final Scope USER_VIEW_USER_PROFILE = new Scope("view:userprofile", "Required to view users profiles");
    private static Map<String, Scope> scopeMap;
    
    @Nonnull
    private String key;
    @Nonnull
    private String description;
    
    static {
        scopeMap = new HashMap<>(3);
        scopeMap.put(STRIDE_PARTICIPATE_IN_CONVERSATION.key, STRIDE_PARTICIPATE_IN_CONVERSATION);
        scopeMap.put(STRIDE_MANAGE_CONVERSATION.key, STRIDE_MANAGE_CONVERSATION);
        scopeMap.put(USER_VIEW_USER_PROFILE.key, USER_VIEW_USER_PROFILE);
    }

    /**
     * Registers a new scope globally.
     * 
     * @return Returns {@code true} if an existent scope was replaced
     */
    public static boolean register(Scope scope) {
        return scopeMap.put(scope.key, scope) != null;
    }

    /**
     * Parses a string into a scope, creating one if it doesn't exist
     */
    public static Scope from(String key) {
        val s = scopeMap.get(key);
        if (s != null) {
            return s;
        }
        return new Scope(key, key);
    }

    /**
     * Return all scopes currently registered
     */
    public static Set<Scope> globalScopes() {
        return new HashSet<>(scopeMap.values());
    }
    
}
