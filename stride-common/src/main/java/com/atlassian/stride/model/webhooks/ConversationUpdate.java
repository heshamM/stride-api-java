package com.atlassian.stride.model.webhooks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Object mapping the WebHook request to the App when a conversation is created, modified, archived or deleted.
 * See: https://developer.atlassian.com/cloud/stride/apis/modules/chat/webhook/
 * <p>
 * Example:
 * <pre>
 * {@code
 * {
 *   "action": "update",
 *   "cloudId": "f7ebe2c0-0309-4687-b913-41d422f2110b",
 *   "conversation": {
 *     "avatarUrl": "https://static.stride.com/default-room-avatars/web/96pt/default_09-96.png",
 *     "created": "2017-09-06T18:51:02.235Z",
 *     "id": "83a31d5f-ef12-4ff0-9e04-2c30c506f264",
 *     "isArchived": false,
 *     "modified": "2017-09-27T01:32:18.555Z",
 *     "name": "My Favorite Room",
 *     "privacy": "public",
 *     "topic": "This is the new topic!",
 *     "type": "group"
 *   },
 *   "initiator": {
 *     "id": "655363:ff396d0c-29db-40f0-ba3d-492b765b15ff"
 *   },
 *   "type": "chat_conversation_updates"
 * }
 * }
 * </pre>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ConversationUpdate extends UnknownPropertiesSupport {

    public enum Action {
        /**
         * A conversation was created. Only applicable for apps installed at the site level.
         */
        create,
        /**
         * A conversation was modified. One or more of the privacy, name, or topic was changed.
         */
        update,
        /**
         * A conversation was archived.
         */
        archive,
        /**
         * A conversation was unarchived.
         */
        unarchive,
        /**
         * A conversation was deleted.
         */
        delete
    }

    private Action action;
    private String cloudId;
    private Conversation conversation;
    private UserId initiator;
    private String type;

    public boolean wasCreated() {
        return Action.create.equals(action);
    }

    public boolean wasUpdated() {
        return Action.update.equals(action);
    }

    public boolean wasDeleted() {
        return Action.delete.equals(action);
    }

    public boolean wasArchived() {
        return Action.archive.equals(action);
    }

    public boolean wasUnarchived() {
        return Action.unarchive.equals(action);
    }

}
