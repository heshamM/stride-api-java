package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.spring.config.model.AppConfig;
import com.atlassian.stride.spring.config.model.AppConfigHolder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Provides current configuration, either if there's one {@code instance} or multiple {@code instances} defined in your 
 * configuration, making them accessible by their OAuth Client ID.
 */
@AllArgsConstructor
@Slf4j
public class DefaultAppConfigSupplier implements AppConfigSupplier {

    private final AppConfigHolder appConfigHolder;

    /**
     * Access to {@code instance} configuration or the multiple {@code instances} defined in your configuration, 
     * whatever matches the client id.
     */
    @Override
    public Optional<AppConfig> byClientId(@Nonnull final String clientId) {
        if (clientId == null || clientId.isEmpty()) {
            log.warn("Client id not found in path or in current context");
            return Optional.empty();
        }
        return appConfigHolder.instances().stream()
                .filter(e -> clientId.equals(e.getClientId()))
                .findFirst();
    }
}
