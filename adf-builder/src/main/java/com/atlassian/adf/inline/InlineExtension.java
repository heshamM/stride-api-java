package com.atlassian.adf.inline;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.annotation.Nonnull;

@JsonTypeName("inlineExtension")
public class InlineExtension extends AbstractNode<InlineExtension> implements InlineNode {
    @JsonIgnore
    @Override
    public String text() {
        return strAttribute("text").orElse("");
    }


    /**
     * Creates an {@link InlineExtension} of extensionKey value: "actionGroup." and the passed parameters.
     *
     * <p>More on this can be found
     * <a href="https://developer.atlassian.com/cloud/stride/learning/adding-actions/">here</a>.
     *
     * @param parameters the parameters to pass to the inline extension.
     * @return an {@link InlineExtension} of extensionKey value: "actionGroup." and the passed parameters.
     */
    @Nonnull
    public static InlineExtension ofActionGroup(@Nonnull final InlineExtensionParameters parameters) {
        return of("com.atlassian.stride", "actionGroup", parameters);
    }

    @Nonnull
    public static InlineExtension ofActionGroup(@Nonnull final String actionGroupKey,
                                                @Nonnull final ActionGroupAction... actions) {

        return ofActionGroup(new ActionGroupParameters(actionGroupKey, actions));
    }


    /**
     * Creates an {@link InlineExtension} of extensionKey: {@code extensionKey}, extensionType: {@code extensionType}
     * and the passed parameters.
     *
     * <p>More on this can be found
     * <a href="https://developer.atlassian.com/cloud/stride/learning/adding-actions/">here</a>.
     *
     * @param parameters the parameters to pass to the inline extension.
     * @return an {@link InlineExtension} of extensionKey: {@code extensionKey}, extensionType: {@code extensionType}
     *         and the passed parameters.
     */
    @Nonnull
    public static InlineExtension of(@Nonnull final String extensionType,
                                     @Nonnull final String extensionKey,
                                     @Nonnull final InlineExtensionParameters parameters) {

        final InlineExtension inlineExtension = new InlineExtension();
        inlineExtension.anyAttribute("extensionType", extensionType);
        inlineExtension.anyAttribute("extensionKey", extensionKey);
        inlineExtension.anyAttribute("parameters", parameters);
        return inlineExtension;
    }
}
