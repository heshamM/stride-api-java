package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@RequiredArgsConstructor(staticName = "context")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Context extends UnknownPropertiesSupport {

    @Nonnull
    private String text;
    private Icon icon;

    @JsonIgnore
    public Context icon(String url, String label) {
        this.icon = Icon.icon(url, label);
        return this;
    }

    public static Context context(String text, String iconUrl, String iconLabel) {
        return context(text).icon(iconUrl, iconLabel);
    }

}
