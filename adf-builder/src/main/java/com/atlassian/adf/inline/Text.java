package com.atlassian.adf.inline;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/text/
 */
@Data(staticConstructor = "of")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("text")
public class Text extends AbstractNode<Text> implements InlineNode {

    @Nonnull
    @JsonProperty
    private String text;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonProperty
    private List<Mark> marks;

    @JsonIgnore
    public Text em() {
        mark(Mark.em());
        return this;
    }

    @JsonIgnore
    public Text strong() {
        mark(Mark.strong());
        return this;
    }

    @JsonIgnore
    public Text code() {
        mark(Mark.code());
        return this;
    }

    @JsonIgnore
    public Text strike() {
        mark(Mark.strike());
        return this;
    }

    @JsonIgnore
    public Text sub() {
        mark(Mark.sub());
        return this;
    }

    @JsonIgnore
    public Text sup() {
        mark(Mark.sup());
        return this;
    }

    @JsonIgnore
    public Text underline() {
        mark(Mark.underline());
        return this;
    }

    @JsonIgnore
    public Text link(String href) {
        mark(Mark.link(href));
        return this;
    }

    @JsonIgnore
    public Text color(String hexColor) {
        mark(Mark.color(hexColor));
        return this;
    }

    public Text mark(Mark... newMarks) {
        if (marks == null) {
            marks = new ArrayList<>();
        }
        Collections.addAll(marks, newMarks);
        return this;
    }

}
