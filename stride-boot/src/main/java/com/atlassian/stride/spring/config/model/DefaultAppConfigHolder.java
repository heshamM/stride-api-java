package com.atlassian.stride.spring.config.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Loads your app configurations from your {@code application.yaml}.
 * <p>
 * Notice you can load multiple configurations if you have multiple "instances" of you App created in DAC
 * (developer.atlassian.com). In this case, your App service will work in a multi-tenanted fashion, so take care with
 * context-bound operations.
 * <p>
 * For instance, each request will verify the incoming JWT token and tie the request with the specific App instance
 * according to its client Id. Then, calls to the API within that request will use outgoing JWT token generated
 * using the credentials for the same client.
 * <p>
 * Otherwise you can specify a single {@code instance} entry in your {@code application.yaml} with your App attributes,
 * it'll tie all operations with the provided credentials.
 */
@Getter
@Setter
@Validated
public class DefaultAppConfigHolder implements AppConfigHolder {

    @Nonnull
    private String name;
    private Map<String, AppConfig> instances = new HashMap<>();
    @NestedConfigurationProperty
    private AppConfig instance;

    @Override
    public String name() {
        return name;
    }

    @Override
    public List<AppConfig> instances() {
        List<AppConfig> list = new ArrayList<>(instances.values());
        if (instance != null) {
            list.add(instance);
        }
        return list;
    }
    
}
