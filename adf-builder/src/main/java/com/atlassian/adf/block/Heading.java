package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.atlassian.adf.base.TextNodeContainerMixin;
import com.atlassian.adf.inline.Text;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/heading
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("heading")
@ParametersAreNonnullByDefault
public class Heading extends AbstractNodeContainer<Heading, Text> implements BlockNode, TextNodeContainerMixin<Heading> {

    private Heading() {
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = Text.class)
    public List<Text> children() {
        return super.children();
    }

    @Override
    public Heading add(Text child) {
        return super.add(child);
    }

    @Override
    public Heading add(Text... items) {
        return super.add(items);
    }

    @JsonIgnore
    public Heading level(Level level) {
        return attribute("level", level.ordinal() + 1);
    }

    @JsonIgnore
    public Level level() {
        return intAttribute("level").map(Level::of).orElse(Level.H1);
    }

    @JsonIgnore
    public Heading add(String... lines) {
        for (String line : lines) {
            add(Text.of(line));
        }
        return this;
    }

    public static Heading heading(Level level, Text... elements) {
        return heading(level).add(elements);
    }

    public static Heading heading(Level level, String... lines) {
        return heading(level).add(lines);
    }

    public static Heading heading(Level level) {
        return new Heading().level(level);
    }

    public enum Level {
        H1, H2, H3, H4, H5, H6;

        public static Level of(int level) {
            switch (level) {
                case 1: return H1;
                case 2: return H2;
                case 3: return H3;
                case 4: return H4;
                case 5: return H5;
                case 6: return H6;
                default: throw new RuntimeException("Invalid heading level: " + level);
            }
        }
    }

}
