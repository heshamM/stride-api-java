package com.atlassian.stride.api;

import com.atlassian.stride.api.util.HttpClientHelper;
import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.DefaultCachedTokenGenerator;
import com.atlassian.stride.auth.DefaultTokenGenerator;
import com.atlassian.stride.context.ContextConfigSupplier;
import com.atlassian.stride.request.httpclient.HttpClientRequestProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Easier way to build {@link StrideClient} instances.
 */
@Slf4j
@ParametersAreNonnullByDefault
@Builder
public class StrideClientSupplier {

    @lombok.NonNull
    protected final ObjectMapper objectMapper;
    @lombok.NonNull
    protected final Executor executor;
    @lombok.NonNull
    protected final ScheduledExecutorService scheduler;
    @lombok.NonNull
    protected final HttpClientBuilder httpClientBuilder;
    @lombok.NonNull
    protected final CachedTokenGenerator cachedTokenGenerator;

    public static class StrideClientSupplierBuilder {
        public StrideClientSupplierBuilder withDefaults() {
            val objectMapper = new ObjectMapper();
            val cachedTokenGenerator = new DefaultCachedTokenGenerator(new DefaultTokenGenerator(HttpClientRequestProvider.of(
                    HttpClientHelper.createRegularHttpClientBuilder().build(), objectMapper)));
            this.objectMapper(objectMapper);
            this.executor(Executors.newFixedThreadPool(5));
            this.scheduler(Executors.newScheduledThreadPool(5));
            this.httpClientBuilder(HttpClientHelper.createJwtSigningHttpClientBuilder(objectMapper, cachedTokenGenerator));
            this.cachedTokenGenerator(cachedTokenGenerator);
            return this;
        }
        
    }

    public StrideClient withConfig(ContextConfigSupplier contextConfigSupplier) {
        return StrideClient.builder()
                .objectMapper(objectMapper)
                .executor(executor)
                .scheduler(scheduler)
                .requestProvider(HttpClientRequestProvider.of(httpClientBuilder.build(), objectMapper))
                .contextConfigSupplier(contextConfigSupplier)
                .build();
    }

}
