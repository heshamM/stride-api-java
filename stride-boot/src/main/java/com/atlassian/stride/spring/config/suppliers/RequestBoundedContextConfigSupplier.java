package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.context.ContextConfigSupplier;
import com.atlassian.stride.context.ThreadBoundedContextConfigSupplier;
import com.atlassian.stride.exception.ContextConfigNotAvailableException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Provides a {@link ContextConfig} according to the current context, which is bounded to the thread/request.
 * <p>
 * It may throw {@link ContextConfigNotAvailableException} if the context was not properly setup.
 */
@Component
@AllArgsConstructor
@Slf4j
@ParametersAreNonnullByDefault
public class RequestBoundedContextConfigSupplier implements ContextConfigSupplier {

    /**
     * It may throw {@link ContextConfigNotAvailableException} if the context was not properly setup.
     */
    @Override
    public ContextConfig get() {
        return ThreadBoundedContextConfigSupplier.obtainConfigFromThread()
                .orElseThrow(ContextConfigNotAvailableException::new);
    }

}