package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Maps a possible unknown block node.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UnknownBlockNode extends AbstractNode<UnknownBlockNode> implements BlockNode {

    public String text() {
        return strAttribute("text").orElse("");
    }

}
