package com.atlassian.adf;

import com.atlassian.adf.block.BlockQuote;
import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.Panel;
import com.atlassian.adf.block.Paragraph;
import com.atlassian.adf.block.card.ApplicationCard;
import com.atlassian.adf.block.codeblock.CodeBlock;
import com.atlassian.adf.block.codeblock.Language;
import com.atlassian.adf.block.decision.DecisionItem;
import com.atlassian.adf.block.decision.DecisionList;
import com.atlassian.adf.block.list.BulletList;
import com.atlassian.adf.block.list.ListItem;
import com.atlassian.adf.block.list.OrderedList;
import com.atlassian.adf.block.media.Media;
import com.atlassian.adf.block.media.MediaGroup;
import com.atlassian.adf.block.task.TaskItem;
import com.atlassian.adf.block.task.TaskList;
import com.atlassian.adf.inline.Text;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.Consumer;

/**
 * Represents a node that contains child block nodes.
 */
@ParametersAreNonnullByDefault
public interface BlockNodeContainer<T extends BlockNodeContainer> extends NodeContainer<BlockNode> {

    T decisionList(DecisionList node);

    T decisionList(String localId, DecisionItem... nodes);

    T decisionList(String localId, Consumer<DecisionList> consumer);

    T taskList(TaskList node);

    T taskList(String localId, TaskItem... nodes);

    T taskList(String localId, Consumer<TaskList> consumer);

    T quote(BlockQuote node);

    T quote(BlockNode... nodes);

    T quote(Consumer<BlockQuote> consumer);

    T paragraph(Paragraph node);

    T paragraph(String text);

    T paragraph(InlineNode... nodes);

    T paragraph(Consumer<Paragraph> consumer);

    T rule();

    T bulletList(BulletList node);

    T bulletList(ListItem... nodes);

    T bulletList(Consumer<BulletList> consumer);

    T orderedList(OrderedList node);

    T orderedList(ListItem... nodes);

    T orderedList(Consumer<OrderedList> consumer);

    T code(CodeBlock node);

    T code(Text... nodes);

    T code(String... lines);

    T code(Language language, Text... nodes);

    T code(Language language, String... lines);

    T code(Consumer<CodeBlock> consumer);

    T head(Heading node);

    T head(Heading.Level level, Text... nodes);

    T head(Heading.Level level, String... lines);

    T head(Heading.Level level, Consumer<Heading> consumer);

    T h1(Text... nodes);

    T h1(String... lines);

    T h1(Consumer<Heading> consumer);

    T h2(Text... nodes);

    T h2(String... lines);

    T h2(Consumer<Heading> consumer);

    T h3(Text... nodes);

    T h3(String... lines);

    T h3(Consumer<Heading> consumer);

    T h4(Text... nodes);

    T h4(String... lines);

    T h4(Consumer<Heading> consumer);

    T h5(Text... nodes);

    T h5(String... lines);

    T h5(Consumer<Heading> consumer);

    T h6(Text... nodes);

    T h6(String... lines);

    T h6(Consumer<Heading> consumer);

    T mediaGroup(MediaGroup node);

    T mediaGroup(Media... nodes);

    T mediaGroup(Consumer<MediaGroup> consumer);

    T panel(Panel node);

    T panel(Panel.PanelType type, BlockNode... nodes);

    T info(BlockNode... nodes);

    T info(Consumer<Panel> consumer);

    T note(BlockNode... nodes);

    T note(Consumer<Panel> consumer);

    T tip(BlockNode... nodes);

    T tip(Consumer<Panel> consumer);

    T warning(BlockNode... nodes);

    T warning(Consumer<Panel> consumer);

    T card(ApplicationCard node);

    T card(String title);

    T card(String title, Consumer<ApplicationCard> consumer);

}
