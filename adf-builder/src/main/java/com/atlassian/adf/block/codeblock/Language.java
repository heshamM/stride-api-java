package com.atlassian.adf.block.codeblock;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@ParametersAreNonnullByDefault
public enum Language {
    abap,
    actionscript("as"),
    ada,
    arduino,
    autoit,
    c,
    @JsonProperty("c++")
    c_plus_plus("c++"),
    clojure("cj"),
    coffeescript,
    csharp("c#"),
    css,
    cuda,
    d,
    dart,
    delphi,
    elixir,
    erlang,
    fortran,
    foxpro,
    go,
    groovy,
    haskell,
    haxe,
    html,
    java,
    javascript("js"),
    julia,
    kotlin,
    latex,
    livescript,
    lua,
    mathematica,
    matlab,
    @JsonProperty("objective-c")
    objective_c("objective-c"),
    @JsonProperty("objective-j")
    objective_j("objective-j"),
    objectpascal("pascal"),
    ocaml,
    octave,
    perl,
    php,
    powershell,
    prolog,
    puppet,
    python,
    qml,
    r,
    racket,
    restructuredtext,
    ruby("rb"),
    rust,
    sass,
    scala,
    scheme,
    shell("sh"),
    smalltalk,
    sql,
    standardml,
    swift,
    tcl,
    tex,
    typescript("ts"),
    vala,
    vbnet,
    verilog,
    vhdl,
    xml,
    xquery;

    private final static Map<String, Language> cache = new HashMap<>();
    private final Set<String> synonyms = new HashSet<>();

    Language() {
        this.synonyms.add(name());
    }

    Language(String... synonyms) {
        this();
        Collections.addAll(this.synonyms, synonyms);
    }

    /**
     * Use this method instead of {@link #valueOf(String)} to take into consideration synonyms for the languages.
     *
     * @param nameOrSynonym Name of the language or synonym. Case insensitive.
     */
    @Nullable
    public static Language from(String nameOrSynonym) {
        if (cache != null) {
            for (Language l : values()) {
                for (String name : l.synonyms) {
                    cache.put(name, l);
                }
            }
        }
        return cache.get(nameOrSynonym.toLowerCase());
    }

}
