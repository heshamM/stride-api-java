package com.atlassian.stride.model.webhooks;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * Object mapping the WebHook request to the App when a message - with the characteristics the App is configured to
 * listen to - is sent in a Stride conversation.
 *
 * {@code {
 *  "cloudId":"f7ebe2c0-0309-4687-b913-41d422f2110b",
 *  "message":{
 *    "id":"a8f6c9e1-9459-11e7-b699-02420aff0003",
 *    "body":{
 *      "version":1,
 *      "type":"doc",
 *      "content":[
 *          {"type":"paragraph",
 *              "content":[
 *                  {"type":"mention","attrs":{"id":"655363:d1060509-d24e-4074-9b87-3cddc4e89824","text":"@Pinned Messages Staging","accessLevel":"CONTAINER"}},
 *                  {"type":"text","text":" "}
 *              ]
 *           }
 *         ]
 *     },
 *    "text":"@Pinned Messages Staging ",
 *    "sender":{"id":"655363:72f1fd44-3922-43cc-b14f-80b390c459b3"},
 *    "ts":"2017-09-08T05:50:46.583548936Z"
 *  },
 *  "recipients":[],
 *  "sender":{"id":"655363:72f1fd44-3922-43cc-b14f-80b390c459b3"},
 *  "conversation":{
 *      "avatarUrl":"https://static.stride.com/default-room-avatars/web/96pt/default_08-96.png",
 *      "id":"5301520d-f573-4568-92e7-4866fae5cb2d",
 *      "isArchived":false,
 *      "name":"test 6",
 *      "privacy":"public",
 *      "topic":"",
 *      "type":"group",
 *      "modified":"2017-06-26T04:17:38.237Z",
 *      "created":"2017-06-26T04:17:38.237Z"},
 *  "type":"chat_message_sent"
 * }}
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MessageSent extends UnknownPropertiesSupport {

    private String cloudId;
    private Conversation conversation;
    private String type;
    private UserId sender;
    private List<String> recipients;
    private Message message;

    @Data
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class Message extends UnknownPropertiesSupport {
        private String id;
        private String text;
        private UserId sender;
        private Date ts;
        private JsonNode body;
    }

}
