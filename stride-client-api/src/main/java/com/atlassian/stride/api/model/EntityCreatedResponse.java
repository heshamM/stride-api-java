package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import lombok.Data;

/**
 * Maps the response when creating a message or a conversation, containing the Id.
 */
@Data
public class EntityCreatedResponse extends UnknownPropertiesSupport {

    private String id;

}
