package com.atlassian.stride.api.model;

import lombok.Builder;
import lombok.Data;

/**
 * Body for the create conversation request
 */
@Data
@Builder
public class ConversationCreateRequest {

    @lombok.NonNull
    private String name;
    @lombok.NonNull
    private String privacy;
    @lombok.NonNull
    private String topic;

}

