package com.atlassian.adf.inline;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/mention
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("mention")
@ParametersAreNonnullByDefault
public class Mention extends AbstractNode<Mention> implements InlineNode {

    @JsonIgnore
    public String id() {
        return strAttribute("id").orElse("");
    }

    @JsonIgnore
    public Mention id(String id) {
        return attribute("id", id);
    }

    @JsonIgnore
    @Override
    public String text() {
        return strAttribute("text").orElse("");
    }

    @JsonIgnore
    public Mention text(String text) {
        return attribute("text", text);
    }

    private Mention() {
    }

    public static Mention mention(String id, String text) {
        return new Mention()
                .id(id)
                .text( text);
    }

}
