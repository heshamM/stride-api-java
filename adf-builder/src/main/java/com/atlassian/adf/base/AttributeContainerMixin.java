package com.atlassian.adf.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Base for nodes containing attributes.
 * <p>
 * It contains a map holding the element {@code attrs} which is common for various nodes in the
 * Atlassian Document Format and some helper methods to access those attributes.
 *
 * @param <T> Concrete implementing class, used to allow fluent setters.
 */
@ParametersAreNonnullByDefault
public interface AttributeContainerMixin<T> {

    /**
     * Map with attributes.
     */
    @Nullable
    SortedMap<String, Object> attributes();

    /**
     * Map with attributes.
     */
    @Nullable
    T attributes(SortedMap<String, Object> a);

    /**
     * Set an arbitrary object as an attribute.
     * Take care if this object can be properly serialized and deserialized with Jackson.
     * Intended to be used internally.
     */
    @SuppressWarnings("unchecked")
    @JsonIgnore
    default T anyAttribute(String key, Object value) {
        SortedMap<String, Object> attributes = attributes();
        if (attributes == null) {
            attributes = new TreeMap<>();
            attributes(attributes);
        }
        attributes.put(key, value);
        return (T) this;
    }

    /**
     * Set a String attribute.
     */
    @JsonIgnore
    default T attribute(String key, String str) {
        return anyAttribute(key, str);
    }

    /**
     * Set an integer attribute.
     */
    @JsonIgnore
    default T attribute(String key, int number) {
        return anyAttribute(key, number);
    }

    /**
     * Return an untyped attribute.
     * Intended to be used internally.
     */
    @JsonIgnore
    default Optional<Object> anyAttribute(String key) {
        return Optional.ofNullable(attributes())
                .map(a -> a.get(key));
    }

    /**
     * Return an attribute casting to the specific type.
     * Beware: if the attribute has another type it'll return an empty response.
     */
    @JsonIgnore
    default <G> Optional<G> attribute(String key, Class<G> type) {
        return Optional.ofNullable(attributes())
                .map(a -> a.get(key))
                .filter(type::isInstance)
                .map(type::cast);
    }

    /**
     * Return a String attribute.
     */
    @JsonIgnore
    default Optional<String> strAttribute(String key) {
        return attribute(key, String.class);
    }

    /**
     * Return an integer attribute.
     */
    @JsonIgnore
    default Optional<Integer> intAttribute(String key) {
        return attribute(key, Integer.class);
    }

}
