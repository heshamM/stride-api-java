package com.atlassian.stride.config;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * Default implementation for {@link EnvironmentConfig}
 */
@Builder
@Getter
@Accessors(fluent = true)
public class DefaultEnvironmentConfig implements EnvironmentConfig {


    @lombok.NonNull
    private String apiUrl;
    private String dacUrl;
    private String updateDescriptorUrl;
    private String installationUrl;

}
