package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@RequiredArgsConstructor(staticName = "badge")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Badge extends UnknownPropertiesSupport {

    @Nonnull
    private Long value;
    private Long max;
    private Theme theme;
    private Appearance appearance;

    public Badge important() {
        return appearance(Appearance.important);
    }

    public Badge primary() {
        return appearance(Appearance.primary);
    }

    public Badge added() {
        return appearance(Appearance.added);
    }

    public Badge removed() {
        return appearance(Appearance.removed);
    }

    public Badge dark() {
        return theme(Theme.dark);
    }

    public enum Appearance {
        @JsonProperty("default") _default, primary, important, added, removed
    }

    public enum Theme {
        @JsonProperty("default") _default, dark
    }

}
