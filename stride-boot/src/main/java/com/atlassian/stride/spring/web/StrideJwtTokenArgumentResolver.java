package com.atlassian.stride.spring.web;

import com.atlassian.stride.auth.model.StrideJwtToken;
import com.atlassian.stride.exception.ContextConfigNotAvailableException;
import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.context.SiteContext;
import com.atlassian.stride.model.context.UserContext;
import com.atlassian.stride.model.context.UserInConversationContext;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Resolves controller parameters of type {@link StrideJwtToken} and some other beans that can be derived from it,
 * namely: 
 *  {@link com.atlassian.stride.model.context.SiteContext},
 *  {@link com.atlassian.stride.model.context.ConversationContext},
 *  {@link com.atlassian.stride.model.context.UserContext}, 
 *  {@link com.atlassian.stride.model.context.UserInConversationContext}.
 */
@Slf4j
public class StrideJwtTokenArgumentResolver implements HandlerMethodArgumentResolver {

    static final String REQUEST_ATTRIBUTE = "stride.jwt.token";

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        //all context classes implement SiteContext, so that's enough
        return StrideJwtToken.class.isAssignableFrom(parameter.getParameterType())
                || SiteContext.class.isAssignableFrom(parameter.getParameterType()); 
    }
 
    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        val parameterType = parameter.getParameterType();
        log.debug("Resolving token argument of type {} for method {}", parameterType, parameter.getMethod().toGenericString());
        val token = fromRequest(webRequest.getNativeRequest(HttpServletRequest.class))
                .orElseThrow(ContextConfigNotAvailableException::new);
        //order is important here because of polymorphism
        if (UserInConversationContext.class.isAssignableFrom(parameter.getParameterType())) {
            return token.userInConversationContext();
        }
        if (ConversationContext.class.isAssignableFrom(parameter.getParameterType())) {
            return token.conversationContext();
        }
        if (UserContext.class.isAssignableFrom(parameter.getParameterType())) {
            return token.userContext();
        }
        if (SiteContext.class.isAssignableFrom(parameter.getParameterType())) {
            return token.siteContext();
        }
        return token;
    }

    public static void setInRequest(HttpServletRequest httpServletRequest, StrideJwtToken config) {
        httpServletRequest.setAttribute(REQUEST_ATTRIBUTE, config);
    }

    public static Optional<StrideJwtToken> fromRequest(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable((StrideJwtToken) httpServletRequest.getAttribute(REQUEST_ATTRIBUTE));
    }

}