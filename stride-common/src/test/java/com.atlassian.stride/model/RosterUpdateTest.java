package com.atlassian.stride.model;

import com.atlassian.stride.model.webhooks.RosterUpdate;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class RosterUpdateTest {

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void unserializationTest() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/roster-update-payload.json"), RosterUpdate.class);
        assertThat(a.getConversation().getName(), is("My Favorite Room"));
        assertThat(a.wasAdded(), is(true));
        assertThat(a.getConversation().isPublic(), is(true));
    }

}
