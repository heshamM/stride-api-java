package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.spring.config.model.AppConfig;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Supply a given App configuration according to its OAuth Client ID.
 */
public interface AppConfigSupplier {

    /**
     * Access to App configuration matching the client id.
     */
    Optional<AppConfig> byClientId(@Nonnull String clientId);
    
}
