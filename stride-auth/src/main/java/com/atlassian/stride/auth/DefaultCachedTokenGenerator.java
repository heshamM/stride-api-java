package com.atlassian.stride.auth;

import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.auth.model.RefreshTokenResponse;
import com.atlassian.stride.auth.model.StrideJwtToken;
import com.atlassian.stride.config.ContextConfig;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import static java.time.Instant.now;

/**
 * Add in-memory caching on top of {@link DefaultTokenGenerator}.
 * <p>
 * Caches are invalidated when time is close to {@link StrideJwtToken#exp} (how much depends on the
 * {@code duration} defined when creating the token generator - it defaults to {@link #DEFAULT_ANTICIPATED_TIMEOUT}.
 * <p>
 * It also can be manually invalidated, for instance, if the a request using a token returns an unauthorized response.
 */
@Slf4j
@ParametersAreNonnullByDefault
public class DefaultCachedTokenGenerator implements CachedTokenGenerator {

    public static final Duration DEFAULT_ANTICIPATED_TIMEOUT = Duration.ofSeconds(60);

    private final TokenGenerator delegate;
    private final Duration anticipatedTimeout;
    private final Map<String, TokenTimePair> tokenCache;

    public DefaultCachedTokenGenerator(TokenGenerator delegate, Duration anticipatedTimeout) {
        this.delegate = delegate;
        this.anticipatedTimeout = anticipatedTimeout;
        this.tokenCache = new ConcurrentHashMap<>();
    }

    public DefaultCachedTokenGenerator(TokenGenerator delegate) {
        this(delegate, DEFAULT_ANTICIPATED_TIMEOUT);
    }
    
    @Override
    public void invalidate(String clientId) {
        tokenCache.remove(clientId);
    }

    @Override
    public RefreshTokenResponse generate(ContextConfig contextConfig) throws FailedToObtainTokenException {
        val err = new AtomicReference<Throwable>();
        try {
            return tokenCache.compute(contextConfig.clientId(), (clientId, pair) -> {
                boolean expired = false;
                if (pair != null) {
                    expired = hasExpired(pair.getToken(), pair.getTime());
                    if (!expired) {
                        log.debug("Reused token from cache for {}", clientId);
                        return pair;
                    }
                }
                try {
                    if (expired) {
                        log.debug("Token from cache was expired for {}", clientId);
                    }
                    return TokenTimePair.of(delegate.generate(contextConfig), now());
                } catch (Throwable e) {
                    err.set(e);
                    return null;
                }
            }).getToken();
        } finally {
            Throwable e = err.get();
            if (e != null) {
                if (e instanceof FailedToObtainTokenException) {
                    throw (FailedToObtainTokenException) e;
                }
                if (e instanceof RuntimeException) {
                    throw (RuntimeException) e;
                }
                throw new RuntimeException(e);
            }
        }
    }

    private boolean hasExpired(RefreshTokenResponse token, Instant timeOfGeneration) {
        val timeOfExperiationWithAntecipation = timeOfGeneration
                .plus(Duration.ofSeconds(token.getExpires_in()))
                .minus(anticipatedTimeout);
        if (log.isDebugEnabled()) {
            log.debug("Computing expiration - now: {}, timeout: {}, time of generation: {}, expires in: {}, time to expire: {}",
                    DateTimeFormatter.ISO_INSTANT.format(now()),
                    anticipatedTimeout,
                    DateTimeFormatter.ISO_INSTANT.format(timeOfGeneration),
                    token.getExpires_in(),
                    DateTimeFormatter.ISO_INSTANT.format(timeOfExperiationWithAntecipation));
        }
        return now().isAfter(timeOfExperiationWithAntecipation);
    }

    /**
     * Stores generated token and time of generation (actually, the time the response arrived)
     */
    @Value(staticConstructor = "of")
    private static class TokenTimePair {
        private RefreshTokenResponse token;
        private Instant time;
    }

}
