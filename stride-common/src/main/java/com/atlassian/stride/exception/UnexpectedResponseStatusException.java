package com.atlassian.stride.exception;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Thrown when calling a remote endpoint and the response HTTP status does NOT match what was expected
 */
public class UnexpectedResponseStatusException extends RuntimeException {

    private final List<Integer> expectedStatuses;
    private final Integer obtained;

    public UnexpectedResponseStatusException(List<Integer> expectedStatuses, Integer obtained) {
        super("Unexpected response status of " + obtained + ". Expected one of these: " +
                expectedStatuses.stream().map(Object::toString).collect(Collectors.joining(", ")));
        this.expectedStatuses = expectedStatuses;
        this.obtained = obtained;
    }

    public List<Integer> getExpectedStatuses() {
        return expectedStatuses;
    }

    public Integer getObtainedStatus() {
        return obtained;
    }
}
