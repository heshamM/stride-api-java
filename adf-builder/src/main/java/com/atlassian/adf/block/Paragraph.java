package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.InlineNode;
import com.atlassian.adf.InlineNodeContainer;
import com.atlassian.adf.base.AbstractInlineNodeContainer;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data(staticConstructor = "paragraph")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("paragraph")
public class Paragraph extends AbstractInlineNodeContainer<Paragraph> implements BlockNode, InlineNodeContainer<Paragraph> {

    public static Paragraph paragraph(InlineNode... elements) {
        return paragraph().add(elements);
    }

}
