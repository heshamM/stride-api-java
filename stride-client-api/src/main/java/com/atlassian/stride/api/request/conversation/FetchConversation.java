package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch a conversation
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class FetchConversation {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Fetch conversation
     */
    public CompletableFuture<Conversation> from(ConversationContext context) {
        return apiRequest.build(Conversation.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uri(StrideEndpoint.conversation.fetch.one(context)));
    }

    /**
     * Fetch conversation
     */
    public CompletableFuture<Conversation> from(Conversation conversation) {
        return from(conversation.asContext());
    }

}