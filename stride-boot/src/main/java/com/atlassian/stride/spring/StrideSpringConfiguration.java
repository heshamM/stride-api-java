package com.atlassian.stride.spring;

import com.atlassian.stride.api.StrideClient;
import com.atlassian.stride.api.StrideClientSupplier;
import com.atlassian.stride.api.util.HttpClientHelper;
import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.DefaultCachedTokenGenerator;
import com.atlassian.stride.auth.DefaultTokenGenerator;
import com.atlassian.stride.auth.TokenGenerator;
import com.atlassian.stride.request.RequestProvider;
import com.atlassian.stride.request.httpclient.HttpClientRequestProvider;
import com.atlassian.stride.spring.config.model.AppConfigHolder;
import com.atlassian.stride.spring.config.model.DefaultAppConfigHolder;
import com.atlassian.stride.spring.config.suppliers.AppConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.DefaultAppConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.DefaultServiceConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.RequestBoundedAppConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.RequestBoundedContextConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.ServiceConfigSupplier;
import com.atlassian.stride.spring.web.AppConfigArgumentResolver;
import com.atlassian.stride.spring.web.ContextConfigArgumentResolver;
import com.atlassian.stride.spring.web.ContextConfigPathVariableInterceptor;
import com.atlassian.stride.spring.web.StrideJwtTokenArgumentResolver;
import com.atlassian.stride.spring.web.TokenVerifierInterceptor;
import lombok.val;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * Spring configuration with common stuff for building Stride Apps
 */
@Configuration
@ComponentScan(basePackageClasses = StrideSpringConfiguration.class)
public class StrideSpringConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private Jackson2ObjectMapperBuilder objectMapperBuilder;
    @Autowired
    private Executor executor;
    @Autowired
    private RequestBoundedContextConfigSupplier requestBoundedContextConfigSupplier;
    @Autowired
    private RequestBoundedAppConfigSupplier requestBoundedAppConfigSupplier;
    @Autowired
    private Environment environment;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new AppConfigArgumentResolver(requestBoundedAppConfigSupplier));
        argumentResolvers.add(new ContextConfigArgumentResolver(requestBoundedContextConfigSupplier));
        argumentResolvers.add(new StrideJwtTokenArgumentResolver());
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ContextConfigPathVariableInterceptor(appConfigSupplier()));
        registry.addInterceptor(new TokenVerifierInterceptor(appConfigSupplier()));
    }

    @ConfigurationProperties("app")
    @Bean
    AppConfigHolder appConfigHolder() {
        return new DefaultAppConfigHolder();
    }

    @Bean
    AppConfigSupplier appConfigSupplier() {
        return new DefaultAppConfigSupplier(appConfigHolder());
    }

    @Bean
    ServiceConfigSupplier serviceConfigSupplier() {
        return new DefaultServiceConfigSupplier(environment);
    }

    @Bean
    TokenGenerator tokenGenerator() {
        return new DefaultTokenGenerator(requestProvider());
    }
    
    @Bean
    CachedTokenGenerator cachedTokenGenerator() {
        return new DefaultCachedTokenGenerator(tokenGenerator());
    }
    
    @Bean
    @Primary
    HttpClientBuilder httpClientBuilder() {
        return HttpClientHelper.createRegularHttpClientBuilder();
    }
    
    @Bean
    @Qualifier("signing")
    HttpClientBuilder jwtSigningHttpClientBuilder() {
        return HttpClientHelper.createJwtSigningHttpClientBuilder(objectMapperBuilder.build(), cachedTokenGenerator());
    }

    @Bean
    StrideClientSupplier strideClientSupplier() {
        return StrideClientSupplier.builder()
                .objectMapper(objectMapperBuilder.build())
                .executor(executor)
                .cachedTokenGenerator(cachedTokenGenerator())
                .httpClientBuilder(jwtSigningHttpClientBuilder())
                .scheduler(threadPoolTaskScheduler().getScheduledThreadPoolExecutor())
                .build();
    }

    @Bean
    StrideClient strideClient() {
        return strideClientSupplier().withConfig(requestBoundedContextConfigSupplier);
    }

    @Bean
    ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        val scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadGroupName("stride-async-api");
        scheduler.setThreadNamePrefix("stride-async-api-request");
        val poolSize = System.getenv("THREAD_POOL_SIZE");
        scheduler.setPoolSize(poolSize == null || poolSize.isEmpty() ? 20 : Integer.parseInt(poolSize));
        scheduler.initialize();
        return scheduler;
    }

    @Bean
    RequestProvider requestProvider() {
        return HttpClientRequestProvider.of(httpClientBuilder().build(), objectMapperBuilder.build());
    }

}