package com.atlassian.stride.api.model;

import com.atlassian.stride.model.context.Context;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * Maps the request object when posting a glance update.
 */
@Data(staticConstructor = "of")
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public class UpdateGlanceRequest {

    @Nonnull
    private final Context context;
    @Nonnull
    private final String label;
    private Map<String, Object> metadata;

    public UpdateGlanceRequest metadata(String key, Object value) {
        if (metadata == null) {
            metadata = new HashMap<>();
        }
        metadata.put(key, value);
        return this;
    }

}
