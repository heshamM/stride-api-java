package com.atlassian.stride.spring.config.model;

import com.atlassian.stride.config.DefaultEnvironmentConfig;
import com.atlassian.stride.config.EnvironmentConfig;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nonnull;

/**
 * Allows you to define custom environment information for a given App configuration instance.
 */
@Data
@NoArgsConstructor
@Validated
public class CustomEnvironmentConfig {

    @Nonnull
    private String apiUrl;
    private String dacUrl;
    private String updateDescriptorUrl;
    private String installationUrl;

    @Setter(AccessLevel.NONE)
    private EnvironmentConfig environmentConfig;

    public EnvironmentConfig getEnvironmentConfig() {
        if (environmentConfig == null) {
            environmentConfig = DefaultEnvironmentConfig.builder()
                    .apiUrl(apiUrl)
                    .dacUrl(dacUrl)
                    .installationUrl(installationUrl)
                    .updateDescriptorUrl(updateDescriptorUrl)
                    .build();
        }
        return environmentConfig;
    }

}
