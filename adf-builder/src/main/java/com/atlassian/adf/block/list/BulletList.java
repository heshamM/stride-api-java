package com.atlassian.adf.block.list;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.function.Consumer;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/bulletList
 */
@Data(staticConstructor = "bulletList")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("bulletList")
@ParametersAreNonnullByDefault
public class BulletList extends AbstractNodeContainer<BulletList, ListItem> implements BlockNode {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = ListItem.class)
    public List<ListItem> children() {
        return super.children();
    }

    @JsonIgnore
    public BulletList item(Consumer<ListItem> c) {
        final ListItem node = ListItem.listItem();
        c.accept(node);
        return add(node);
    }

    @JsonIgnore
    public BulletList item(String text) {
        return add(ListItem.listItem().paragraph(text));
    }

    public static BulletList bulletList(ListItem... elements) {
        return bulletList().add(elements);
    }

    @Override
    public String text() {
        final StringBuilder sb = new StringBuilder();
        final List<ListItem> children = children();
        if (children != null) {
            int size = children.size(), n = 0;
            for (ListItem i : children) {
                sb.append(" - ");
                sb.append(i.text());
                if (n++ < size - 1) {
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }


}
