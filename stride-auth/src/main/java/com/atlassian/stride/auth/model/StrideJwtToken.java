package com.atlassian.stride.auth.model;

import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.context.SiteContext;
import com.atlassian.stride.model.context.UserContext;
import com.atlassian.stride.model.context.UserInConversationContext;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.Objects;

/**
 * Holds token information after successful authorization.
 * <p>
 * The object {@code context} provides information about the site ({@code cloudId}) and conversation 
 * ({@code conversationId}) and is the only secure way to obtain these values, since any other payload could be forged.
 * <p>
 * The {@code sub} field represents the user ({@code userId}) currently logged into Stride and is the only secure way 
 * to obtain this value since any other payload could be forged.
 * <p>
 * Example of token payload:
 * <pre>
 * {@code
 * {
 *  "iss":"4O8l7sLxLGrb5qVD5tOhGdzJo5Ppg0oV",
 *  "context":{
 *    "resourceId":"5301520d-f573-4568-92e7-4866fae5cb2d",
 *    "cloudId":"f7ebe2c0-0309-4687-b913-41d422f2110b",
 *    "resourceType":"conversation"
 *  },
 *  "exp":1504856483,
 *  "iat":1504855583,
 *  "jti":"460dfe3f-7626-4601-9e4a-05e15568b636",
 *  "sub":"5301520d-f573-4568-92e7-4866fae5cb2d"
 * }
 * }
 * </pre>
 */
@Value
@Builder
public class StrideJwtToken {

    private String iss;
    private Long exp;
    private Long iat;
    private String jtw;
    private String sub;
    private Context context;

    /**
     * Holds the context information, usually a conversation
     */
    @Value
    @AllArgsConstructor
    public static class Context {
        private String cloudId;
        private String resourceType;
        private String resourceId;

        /**
         * @deprecated Use {@link StrideJwtToken#conversationContext()}
         */
        @Deprecated
        public ConversationContext asConversationContext() {
            if (!"conversation".equals(resourceType)) {
                throw new RuntimeException("Invalid resourceType for context: " + resourceType + ": not a conversation");
            }
            return com.atlassian.stride.model.context.Context.conversation(cloudId, resourceId);
        }
        
        public boolean isConversationContext() {
            return !"conversation".equals(resourceType);
        }

    }

    /**
     * Obtain the verified conversation information from the token.
     */
    public ConversationContext conversationContext() {
        Objects.requireNonNull(context, "context");
        if (context.isConversationContext()) {
            throw new RuntimeException("Invalid resourceType for context: " + context.getResourceType() + ": not a conversation");
        }
        return com.atlassian.stride.model.context.Context.conversation(context.getCloudId(), context.getResourceId());
    }

    /**
     * Obtain the verified user information from the token. 
     * <p>
     * The Stride user is obtained from the {@code sub} field in the JWT claim.
     */
    public UserContext userContext() {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(sub, "sub");
        return com.atlassian.stride.model.context.Context.user(context.getCloudId(), sub);
    }

    /**
     * Obtain the verified conversation and user information from the token.
     * <p>
     * The Stride user is obtained from the {@code sub} field in the JWT claim.
     */
    public UserInConversationContext userInConversationContext() {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(sub, "sub");
        if (context.isConversationContext()) {
            throw new RuntimeException("Invalid resourceType for context: " + context.getResourceType() + ": not a conversation");
        }
        return com.atlassian.stride.model.context.Context.userInConversation(context.getCloudId(), context.getResourceId(), sub);
    }

    /**
     * Obtain the verified site information from the token.
     */
    public SiteContext siteContext() {
        Objects.requireNonNull(context, "context");
        return com.atlassian.stride.model.context.Context.site(context.getCloudId());
    }

}
