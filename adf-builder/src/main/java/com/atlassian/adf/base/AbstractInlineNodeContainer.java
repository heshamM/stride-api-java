package com.atlassian.adf.base;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.Node;
import com.atlassian.adf.inline.Emoji;
import com.atlassian.adf.inline.HardBreak;
import com.atlassian.adf.inline.Mark;
import com.atlassian.adf.inline.Mention;
import com.atlassian.adf.inline.Text;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Base for a node containing one or more {@link InlineNode}s.
 * <p>
 * Contain helper methods for creation of the various flavors of {@link InlineNode}s.
 *
 * @param <T> Concrete implementing class, used to allow fluent setters.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ParametersAreNonnullByDefault
public abstract class AbstractInlineNodeContainer<T extends AbstractInlineNodeContainer> extends AbstractNodeContainer<T, InlineNode> {

    public T text(Text node) {
        return add(node);
    }

    public T text(String text) {
        return add(Text.of(text));
    }

    public T text(String text, Consumer<Text> consumer) {
        return configureAddAndReturnThis(Text.of(text), consumer);
    }

    public T text(String text, Mark... marks) {
        return add(Text.of(text).mark(marks));
    }

    public T em(String text) {
        return add(Text.of(text).em());
    }

    public T strong(String text) {
        return add(Text.of(text).strong());
    }

    public T code(String text) {
        return add(Text.of(text).code());
    }

    public T strike(String text) {
        return add(Text.of(text).strike());
    }

    public T sub(String text) {
        return add(Text.of(text).sub());
    }

    public T sup(String text) {
        return add(Text.of(text).sup());
    }

    public T underline(String text) {
        return add(Text.of(text).underline());
    }

    public T link(String text, String href) {
        return add(Text.of(text).link(href));
    }

    public T color(String text, String hexColor) {
        return add(Text.of(text).color(hexColor));
    }

    public T hardBreak(HardBreak node) {
        return add(node);
    }

    public T hardBreak() {
        return add(HardBreak.hardBreak());
    }

    public T mention(Mention node) {
        return add(node);
    }

    public T mention(String id, String text) {
        return add(Mention.mention(id, text));
    }

    public T emoji(Emoji node) {
        return add(node);
    }

    public T emoji(String shortName) {
        return add(Emoji.emoji(shortName));
    }

    public T emoji(String shortName, String text) {
        return add(Emoji.emoji(shortName).text(text));
    }

    public T emoji(String shortName, String text, String id) {
        return add(Emoji.emoji(shortName).text(text).id(id));
    }

    public T emoji(String shortName, Consumer<Emoji> consumer) {
        return configureAddAndReturnThis(Emoji.emoji(shortName), consumer);
    }

    private <X extends InlineNode> T configureAddAndReturnThis(X node, Consumer<X> consumer) {
        consumer.accept(node);
        return add(node);
    }

    @Override
    public String text() {
        final List<InlineNode> children = children();
        if (children != null) {
            return children.stream().map(Node::text).collect(Collectors.joining());
        }
        return "";
    }

}
