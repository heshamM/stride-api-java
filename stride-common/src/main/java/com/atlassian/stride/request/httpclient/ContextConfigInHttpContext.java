package com.atlassian.stride.request.httpclient;

import com.atlassian.stride.config.ContextConfig;
import org.apache.http.protocol.HttpContext;

import java.util.Optional;

/**
 * Utility to attach and recover a {@link ContextConfig} to the current Apache HTTP Request
 * Context ({@link HttpContext})
 */
public class ContextConfigInHttpContext {

    public static Optional<ContextConfig> adapt(HttpContext httpContext) {
        return Optional.ofNullable((ContextConfig) httpContext.getAttribute("stride.context"));
    }

    public static HttpContext withContext(HttpContext httpContext, ContextConfig contextConfig) {
        if (contextConfig != null) {
            httpContext.setAttribute("stride.context", contextConfig);
        }
        return httpContext;
    }
    
}
