package com.atlassian.adf.block.media;

import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/media
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonTypeName("media")
public class Media extends AbstractNode<Media> {

    private Media() {
    }

    @JsonIgnore
    public String id() {
        return strAttribute("id").orElse("");
    }

    @JsonIgnore
    public Media id(String id) {
        return attribute("id", id);
    }

    @JsonIgnore
    public Type type() {
        return strAttribute("type")
                .map(String::toLowerCase)
                .map(Type::valueOf)
                .orElse(Type.link);
    }

    @JsonIgnore
    public Media type(Type type) {
        return attribute("type", type.name());
    }

    @JsonIgnore
    public String collection() {
        return strAttribute("collection").orElse("");
    }

    @JsonIgnore
    public Media collection(String collection) {
        return attribute("collection", collection);
    }

    @JsonIgnore
    public String occurrenceKey() {
        return strAttribute("occurrenceKey").orElse("");
    }

    @JsonIgnore
    public Media occurrenceKey(String occurrenceKey) {
        return attribute("occurrenceKey", occurrenceKey);
    }

    public static Media media(String id, Type type, String collection) {
        return new Media().id(id).type(type).collection(collection);
    }

    @Override
    public String text() {
        return "";
    }

    public enum Type {
        link, file
    }

}
