package com.atlassian.stride.api;

import com.atlassian.stride.api.model.UserDetail;
import com.atlassian.stride.model.context.Context;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

@Slf4j
public class UserApiTest {

    private StrideClient stride = Helper.client();

    @Test
    public void details() throws Exception {
        UserDetail d = stride.user().get()
                .from(Context.user(Helper.cloudId(), Helper.userId()))
                .get();
        log.info("Got user details: {}", d);

        assertThat(d.getId(), is(Helper.userId()));
        UserDetail.Email email = new UserDetail.Email();
        email.setPrimary(true);
        email.setValue(Helper.userEmail());
        assertThat(d.getEmails(), contains(email));
        assertThat(d.primaryEmail().get(), is(email.getValue()));
        assertThat(d.primaryAvatar().get(), not(isEmptyString()));
    }

}