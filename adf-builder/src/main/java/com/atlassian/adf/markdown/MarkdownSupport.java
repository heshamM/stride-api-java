package com.atlassian.adf.markdown;

import com.atlassian.adf.BlockNodeContainer;
import com.atlassian.adf.Document;
import com.atlassian.adf.InlineNodeContainer;
import com.atlassian.adf.NodeContainer;
import com.atlassian.adf.base.TextNodeContainerMixin;
import com.atlassian.adf.block.codeblock.CodeBlock;
import com.atlassian.adf.block.codeblock.Language;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.commonmark.node.BlockQuote;
import org.commonmark.node.BulletList;
import org.commonmark.node.Code;
import org.commonmark.node.CustomBlock;
import org.commonmark.node.CustomNode;
import org.commonmark.node.Emphasis;
import org.commonmark.node.FencedCodeBlock;
import org.commonmark.node.HardLineBreak;
import org.commonmark.node.Heading;
import org.commonmark.node.HtmlBlock;
import org.commonmark.node.HtmlInline;
import org.commonmark.node.Image;
import org.commonmark.node.IndentedCodeBlock;
import org.commonmark.node.Link;
import org.commonmark.node.ListItem;
import org.commonmark.node.Node;
import org.commonmark.node.OrderedList;
import org.commonmark.node.Paragraph;
import org.commonmark.node.SoftLineBreak;
import org.commonmark.node.StrongEmphasis;
import org.commonmark.node.Text;
import org.commonmark.node.ThematicBreak;
import org.commonmark.node.Visitor;
import org.commonmark.parser.Parser;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Simple implementation that tries to convert basic Markdown (CommonMark flavor) to Atlassian Document Format.
 */
@Slf4j
@ParametersAreNonnullByDefault
public class MarkdownSupport {

    /**
     * Parse a Markdown (CommonMark flavor) text into a {@link Document}. Unsupported Markdown is ignored.
     *
     * @param markdown Markdown text with basic formatting support
     * @return Document matching the markdown style
     */
    public static Document documentFromMarkdown(String markdown) throws UnsupportedMarkdownFormatException {
        return documentFromMarkdown(markdown, false);
    }

    /**
     * Parse a Markdown (CommonMark flavor) text into a {@link Document}.
     *
     * @param markdown Markdown text with basic formatting support
     * @param throwErrors When {@code false}, won't throw any exception but will omit unintelligible Markdown
     * @return Document matching the markdown style
     * @throws UnsupportedMarkdownFormatException Whenever markdown format isn't supported, but only if {@code throwErrors} is {@code true}.
     */
    public static Document documentFromMarkdown(String markdown, boolean throwErrors) throws UnsupportedMarkdownFormatException {
        Document doc = Document.create();
        Parser parser = Parser.builder().build();
        Node md = parser.parse(markdown);
        log.debug("Parsed {}, parent = {}, child = {}", md, md.getParent(), md.getFirstChild());
        md.accept(new MarkDownToDocumentVisitor(doc, throwErrors));
        return doc;
    }

    public static class MarkDownToDocumentVisitor implements Visitor {
        private final Document doc;
        private final boolean throwErrors;
        private Map<Node, com.atlassian.adf.Node> mdNodeToAdfNodeMap = new HashMap<>();
        private boolean emphasisFormattingEnabled = false;
        private boolean strongFormattingEnabled = false;
        private String linkFormattingAddress = null;

        public MarkDownToDocumentVisitor(Document doc, boolean throwErrors) {
            this.doc = doc;
            this.throwErrors = throwErrors;
        }

        void associate(Node md, com.atlassian.adf.Node adf) {
            mdNodeToAdfNodeMap.put(md, adf);
        }

        com.atlassian.adf.Node adfParent(Node md) {
            return mdNodeToAdfNodeMap.get(md.getParent());
        }

        void visitChildren(Node parent, com.atlassian.adf.Node associated) {
            Objects.requireNonNull(associated, "associated");
            associate(parent, associated);
            Node node = parent.getFirstChild();
            while (node != null) {
                Node next = node.getNext();
                node.accept(this);
                node = next;
            }
        }

        @Override
        public void visit(org.commonmark.node.Document document) {
            log.debug("MD document {}", document);
            visitChildren(document, doc);
        }

        @Override
        public void visit(Emphasis emphasis) {
            log.debug("MD emphasis {}", emphasis);
            try {
                emphasisFormattingEnabled = true;
                visitChildren(emphasis, adfParent(emphasis));
            } finally {
                emphasisFormattingEnabled = false;
            }
        }

        @Override
        public void visit(StrongEmphasis strongEmphasis) {
            log.debug("MD strongEmphasis {}", strongEmphasis);
            try {
                strongFormattingEnabled = true;
                visitChildren(strongEmphasis, adfParent(strongEmphasis));
            } finally {
                strongFormattingEnabled = false;
            }
        }

        @Override
        public void visit(Link link) {
            log.debug("MD link {}", link);
            try {
                linkFormattingAddress = link.getDestination();
                visitChildren(link, adfParent(link));
            } finally {
                linkFormattingAddress = null;
            }
        }

        @Override
        public void visit(HardLineBreak hardLineBreak) {
            log.debug("MD hardLineBreak {}", hardLineBreak);
            val adfParent = adfParent(hardLineBreak);
            if (adfParent instanceof InlineNodeContainer) {
                ((InlineNodeContainer) adfParent).hardBreak();
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected hard break");
            }
        }

        @Override
        public void visit(SoftLineBreak softLineBreak) {
            log.debug("MD softLineBreak {}", softLineBreak);
            //do nothing
        }

        @Override
        public void visit(Heading heading) {
            log.debug("MD heading {}", heading);
            val adfParent = adfParent(heading);
            if (adfParent instanceof BlockNodeContainer) {
                val adfHeading = com.atlassian.adf.block.Heading.heading(com.atlassian.adf.block.Heading.Level.of(heading.getLevel()));
                ((BlockNodeContainer) adfParent).head(adfHeading);
                visitChildren(heading, adfHeading);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for heading");
            }
        }

        @Override
        public void visit(ThematicBreak thematicBreak) {
            log.debug("MD thematicBreak {}", thematicBreak);
            val adfParent = adfParent(thematicBreak);
            if (adfParent instanceof BlockNodeContainer) {
                ((BlockNodeContainer) adfParent).rule();
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for rule");
            }
        }

        @Override
        public void visit(BlockQuote blockQuote) {
            log.debug("MD blockQuote {}", blockQuote);
            val adfParent = adfParent(blockQuote);
            if (adfParent instanceof BlockNodeContainer) {
                val bq = com.atlassian.adf.block.BlockQuote.blockQuote();
                ((BlockNodeContainer) adfParent).quote(bq);
                visitChildren(blockQuote, bq);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for block");
            }
        }

        @Override
        public void visit(BulletList bulletList) {
            log.debug("MD bulletList {}", bulletList);
            val adfParent = adfParent(bulletList);
            if (adfParent instanceof BlockNodeContainer) {
                val adfList = com.atlassian.adf.block.list.BulletList.bulletList();
                ((BlockNodeContainer) adfParent).bulletList(adfList);
                visitChildren(bulletList, adfList);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for list");
            }
        }

        @Override
        public void visit(OrderedList orderedList) {
            log.debug("MD orderedList {}", orderedList);
            val adfParent = adfParent(orderedList);
            if (adfParent instanceof BlockNodeContainer) {
                val adfList = com.atlassian.adf.block.list.OrderedList.orderedList();
                ((BlockNodeContainer) adfParent).orderedList(adfList);
                visitChildren(orderedList, adfList);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for list");
            }
        }

        @Override
        public void visit(ListItem listItem) {
            log.debug("MD listItem {}", listItem);
            val adfParent = adfParent(listItem);
            NodeContainer<com.atlassian.adf.block.list.ListItem> adfList;
            if (adfParent instanceof com.atlassian.adf.block.list.BulletList) {
                adfList = (com.atlassian.adf.block.list.BulletList) adfParent;
            } else if (adfParent instanceof com.atlassian.adf.block.list.OrderedList) {
                adfList = (com.atlassian.adf.block.list.OrderedList) adfParent;
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for list item");
            } else {
                return;
            }
            val adfItem = com.atlassian.adf.block.list.ListItem.listItem();
            adfList.add(adfItem);
            visitChildren(listItem, adfItem);
        }

        @Override
        public void visit(IndentedCodeBlock indentedCodeBlock) {
            log.debug("MD indentedCodeBlock {}", indentedCodeBlock);
            codeBlock(indentedCodeBlock, indentedCodeBlock.getLiteral(), null);
        }

        @Override
        public void visit(FencedCodeBlock fencedCodeBlock) {
            log.debug("MD fencedCodeBlock {}", fencedCodeBlock);
            codeBlock(fencedCodeBlock, fencedCodeBlock.getLiteral(), fencedCodeBlock.getInfo());
        }

        private void codeBlock(Node node, String literal, @Nullable String language) {
            log.debug("MD codeBlock '{}'", literal);
            //fix for line break that appears from nothing
            if (literal.endsWith("\n")) {
                literal = literal.substring(0, literal.length() - 1);
            }
            val adfParent = adfParent(node);
            if (adfParent instanceof BlockNodeContainer) {
                val adfCodeBlock = CodeBlock.codeBlock(literal);
                if (language != null && !language.isEmpty()) {
                    adfCodeBlock.language(Language.from(language.toLowerCase()));
                }
                ((BlockNodeContainer) adfParent).code(adfCodeBlock);
                visitChildren(node, adfCodeBlock);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for block");
            }
        }

        @Override
        public void visit(Paragraph paragraph) {
            log.debug("MD paragraph {}", paragraph);
            val adfParent = adfParent(paragraph);
            if (adfParent instanceof BlockNodeContainer) {
                val adfParagraph = com.atlassian.adf.block.Paragraph.paragraph();
                ((BlockNodeContainer) adfParent).paragraph(adfParagraph);
                visitChildren(paragraph, adfParagraph);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for paragraph");
            }
        }

        @Override
        public void visit(Text text) {
            log.debug("MD text {}", text);
            val adfParent = adfParent(text);
            if (adfParent instanceof InlineNodeContainer) {
                ((InlineNodeContainer<?>) adfParent).text(text.getLiteral(), this::formatText);
            } else if (adfParent instanceof TextNodeContainerMixin) {
                ((TextNodeContainerMixin<?>) adfParent).text(text.getLiteral(), this::formatText);
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for text");
            }
        }

        @Override
        public void visit(Code code) {
            log.debug("MD code {}", code);
            val adfParent = adfParent(code);
            if (adfParent instanceof InlineNodeContainer) {
                ((InlineNodeContainer<?>) adfParent).text(code.getLiteral(), t -> {
                    t.code();
                    formatText(t);
                });
            } else if (adfParent instanceof TextNodeContainerMixin) {
                ((TextNodeContainerMixin<?>) adfParent).text(code.getLiteral(), t -> {
                    t.code();
                    formatText(t);
                });
            } else if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Unexpected location for inline code");
            }
        }

        private void formatText(com.atlassian.adf.inline.Text t) {
            if (emphasisFormattingEnabled) {
                t.em();
            }
            if (strongFormattingEnabled) {
                t.strong();
            }
            if (linkFormattingAddress != null) {
                t.link(linkFormattingAddress);
            }
        }

        @Override
        public void visit(HtmlInline htmlInline) {
            log.debug("MD htmlInline {}", htmlInline);
            if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Inline HTML not supported");
            }
        }

        @Override
        public void visit(HtmlBlock htmlBlock) {
            log.debug("MD htmlBlock {}", htmlBlock);
            if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("HTML blocks/tags not supported");
            }
        }

        @Override
        public void visit(Image image) {
            log.debug("MD image {}", image);
            //we would have to upload the image to the media endpoint
            if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Image nodes not supported");
            }
        }

        @Override
        public void visit(CustomBlock customBlock) {
            log.debug("MD customBlock {}", customBlock);
            if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Custom nodes not supported");
            }
        }

        @Override
        public void visit(CustomNode customNode) {
            log.debug("MD customNode {}", customNode);
            if (throwErrors) {
                throw new UnsupportedMarkdownFormatException("Custom nodes not supported");
            }
        }
    }

}
