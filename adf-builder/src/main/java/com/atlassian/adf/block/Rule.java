package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/rule
 */
@Data(staticConstructor = "rule")
@EqualsAndHashCode(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("rule")
public class Rule extends AbstractNode<Rule> implements BlockNode {

    @Override
    public String text() {
        return "---";
    }
}
