package com.atlassian.adf;

import com.atlassian.adf.block.codeblock.Language;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeTextSerializationTest {

    private Document createNode() {
        return Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!"))
                .h1(p -> p.text("Title"))
                .paragraph(p -> p
                        .text("Text ")
                        .em("with ")
                        .code("format")
                        .text(" ")
                        .strike("and")
                        .text(" ")
                        .underline("other")
                        .text(" ")
                        .sup("things")
                        .sub(". "))
                .paragraph(p -> p.link("A link", "http://google.com").text(":"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .paragraph("A:")
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .paragraph(p -> p.color("asda", "#97a0af"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .paragraph(p -> p
                        .text("Multi")
                        .hardBreak()
                        .text("Line")
                        .hardBreak()
                        .text("No")
                        .hardBreak()
                        .text("Paragraph"))
                .warning(p -> p.paragraph("alert"))
                .info(p -> p.paragraph("Info"))
                .tip(p -> p.paragraph("idea"))
                .note(p -> p.paragraph("note"));
    }

    @Test
    void serializeToText() throws Exception {
        Document doc = createNode();
        String s = doc.text();
        assertEquals(loadText(), s);
    }

    private String loadText() throws Exception {
        final URI uri = NodeTextSerializationTest.class.getResource("/multiple-item-message-text.txt").toURI();
        return new String(Files.readAllBytes(Paths.get(uri)), StandardCharsets.UTF_8);
    }

}
