package com.atlassian.stride.api;

import com.atlassian.stride.api.model.MessageWithLinks;
import com.atlassian.stride.model.context.Context;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

@Slf4j
public class AsyncTest {

    private StrideClient stride = Helper.client();

    @Test
    public void testAsync() throws Exception {
        //context
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());

        //send a message and get the Id
        String messageId = stride.message().send("hi").toConversation(context).get().getId();

        //fetch the message adding an artificial delay to the future
        List<Integer> trace = new ArrayList<>();
        CompletableFuture<MessageWithLinks> f = stride.message().fetch()
                .one(messageId)
                .from(context)
                .thenApply(r -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    trace.add(2);
                    return r;
                });
        //this line should be executed before the future is completed
        trace.add(1);
        //wait to the future to complete
        f.join();
        //this line should be executed after the future is completed
        trace.add(3);
        //assert execution order
        assertThat(trace, contains(1, 2, 3));
    }

}