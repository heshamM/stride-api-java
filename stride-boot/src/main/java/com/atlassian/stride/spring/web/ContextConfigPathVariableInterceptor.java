package com.atlassian.stride.spring.web;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.context.ThreadBoundedContextConfigSupplier;
import com.atlassian.stride.spring.config.suppliers.AppConfigSupplier;
import com.atlassian.stride.spring.exception.UnresolvedClienIdException;
import com.atlassian.stride.spring.util.SpringHelper;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Resolves {@link ContextConfig} for incoming requests according to the path variable
 * {@code clientId}, if defined in your request mapping configuration.
 */
@AllArgsConstructor
public class ContextConfigPathVariableInterceptor extends HandlerInterceptorAdapter {

    private final AppConfigSupplier appConfigSupplier;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        val clientId = SpringHelper.getPathVariables(request).get("clientId");
        if (clientId != null) {
            val config = appConfigSupplier.byClientId(clientId).orElseThrow(() -> new UnresolvedClienIdException(clientId));
            ThreadBoundedContextConfigSupplier.attachConfigToThread(config.getContextConfig());
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        ThreadBoundedContextConfigSupplier.removeConfigFromThread();
    }

}