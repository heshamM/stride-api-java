package com.atlassian.adf.block.task;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.InlineNode;
import com.atlassian.adf.InlineNodeContainer;
import com.atlassian.adf.base.AbstractInlineNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/taskItem
 */
@Data(staticConstructor = "task")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("taskItem")
public class TaskItem extends AbstractInlineNodeContainer<TaskItem> implements BlockNode, InlineNodeContainer<TaskItem> {

    @JsonIgnore
    public String localId() {
        return strAttribute("localId").orElse("");
    }

    @JsonIgnore
    public TaskItem localId(String localId) {
        return attribute("localId", localId);
    }

    @JsonIgnore
    public TaskState state() {
        return strAttribute("state")
                .map(String::toUpperCase)
                .map(TaskState::valueOf)
                .orElse(TaskState.TODO);
    }

    @JsonIgnore
    public TaskItem state(TaskState state) {
        return attribute("state", state.name());
    }

    public static TaskItem task(String localId, TaskState state, InlineNode... elements) {
        return TaskItem.task()
                .localId(localId)
                .state(state)
                .add(elements);
    }

    public enum TaskState {
        TODO, DONE
    }

}
