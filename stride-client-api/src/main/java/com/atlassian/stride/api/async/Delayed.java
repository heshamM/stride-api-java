package com.atlassian.stride.api.async;

import com.atlassian.stride.context.ContextConfigSupplier;
import com.atlassian.stride.context.ContextPassingRunner;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Delays the execution while keeping the current context for the request.
 */
@RequiredArgsConstructor(staticName = "of")
@ParametersAreNonnullByDefault
public class Delayed {

    @NonNull
    private final ScheduledExecutorService scheduler;
    @NonNull
    private final Duration duration;
    @NonNull
    private final ContextConfigSupplier contextConfigSupplier;

    public CompletableFuture<Void> then(Runnable r) {
        val cf = new CompletableFuture<Void>();
        scheduler.schedule(ContextPassingRunner.withContext(() -> {
            try {
                r.run();
                cf.complete(null);
            } catch (Throwable e) {
                cf.completeExceptionally(e);
            }
        }, contextConfigSupplier.get()), duration.getNano(), TimeUnit.NANOSECONDS);
        return cf;
    }

}
