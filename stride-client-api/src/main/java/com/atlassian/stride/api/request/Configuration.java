package com.atlassian.stride.api.request;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.UpdateConfigurationRequest;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * Send configuration state updates (shows/hides the yellow line at the right of your glance at the right sidebar)
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class Configuration {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String key;
    private ConversationContext context;

    /**
     * Send the configuration update
     */
    public CompletableFuture<Void> updateWith(boolean configured) {
        Objects.requireNonNull(context, "context");

        return apiRequest.build(b -> b
                .acceptJson()
                .scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION)
                .expect(HttpStatus.SC_NO_CONTENT)
                .body(UpdateConfigurationRequest.of(context, configured)))
                .post(apiRequest.uri(StrideEndpoint.configuration.update(key)));
    }

}