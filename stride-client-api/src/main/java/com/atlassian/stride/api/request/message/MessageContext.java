package com.atlassian.stride.api.request.message;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.MessageListWithLinks;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch contextual message history (messages around one base message)
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
public class MessageContext {

    @Nonnull
    private ApiRequestHelper apiRequest;
    
    @Setter
    private String message;
    /**
     * Number of messages to fetch before the message
     */
    @Setter
    private Integer before;
    /**
     * Number of messages to fetch after the message
     */
    @Setter
    private Integer after;
    
    /**
     * Fetch message history
     */
    public CompletableFuture<MessageListWithLinks> from(ConversationContext conversationContext) {
        return apiRequest.build(MessageListWithLinks.class, b -> b.acceptJson().scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION))
                .get(apiRequest.uriBuilder()
                        .endpoint(StrideEndpoint.message.fetch.context(conversationContext, message))
                        .param("before", before)
                        .param("after", after)
                        .build());
    }

}