package com.atlassian.stride.context;

import com.atlassian.stride.config.ContextConfig;

/**
 * Provides a {@link ContextConfig} for a given client Id or according to the current context.
 */
public interface ContextConfigSupplier {

    ContextConfig get();

}