package com.atlassian.stride.spring.config.model;

import java.util.List;

/**
 * Holds your high-level of App configuration data
 */
public interface AppConfigHolder {

    /**
     * App name
     */
    String name();

    /**
     * Instance App configuration (in case multiple client ids are used - multi-tenancy)
     */
    List<AppConfig> instances();

}
