package com.atlassian.stride.api.request;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.request.RequestBuilder;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch a media file
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class FetchMedia {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String id;

    /**
     * Fetch media file
     */
    public CompletableFuture<byte[]> from(ConversationContext conversationContext) {
        return apiRequest.build(byte[].class, RequestBuilder::acceptBytes)
                .get(apiRequest.uri(StrideEndpoint.media.get(conversationContext, id)));
    }

}