package com.atlassian.stride.api.request;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.request.AsyncRequest;
import com.atlassian.stride.request.RequestBuilder;
import com.atlassian.stride.request.RequestProvider;
import com.atlassian.stride.request.common.ClientUriBuilder;
import lombok.RequiredArgsConstructor;
import lombok.val;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

/**
 * Helper class that provides common attributes and functionality when calling the API
 */
@RequiredArgsConstructor(staticName = "of")
@ParametersAreNonnullByDefault
public class ApiRequestHelper {

    @Nonnull
    protected RequestProvider requestProvider;
    @Nonnull
    protected Executor executor;
    @Nonnull
    protected ContextConfig contextConfig;

    public AsyncRequest<Void> build(Consumer<RequestBuilder> configureBuilder) {
        return build(Void.class, configureBuilder);
    }

    /**
     * Generates a request builder
     */
    public <T> AsyncRequest<T> build(Class<T> responseType, Consumer<RequestBuilder> configureBuilder) {
        Objects.requireNonNull(requestProvider, "requestProvider");
        Objects.requireNonNull(executor, "executor");
        Objects.requireNonNull(contextConfig, "contextConfig");
        Objects.requireNonNull(responseType, "responseType");
        Objects.requireNonNull(configureBuilder, "configureBuilder");
        val b = requestProvider.builder();
        b.contextConfig(contextConfig);
        configureBuilder.accept(b);
        return b.build(responseType).async(executor);
    }

    /**
     * Build an URI
     */
    public URI uri(String endpointPath) {
        Objects.requireNonNull(endpointPath, "endpointPath");
        return uriBuilder().endpoint(endpointPath).build();
    }

    /**
     * Creates an URI builder
     */
    public ClientUriBuilder uriBuilder() {
        return ClientUriBuilder.of(contextConfig.env().apiUrl());
    }

}