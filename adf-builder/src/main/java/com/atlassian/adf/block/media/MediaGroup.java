package com.atlassian.adf.block.media;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/mediaGroup
 */
@Data(staticConstructor = "mediaGroup")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("mediaGroup")
public class MediaGroup extends AbstractNodeContainer<MediaGroup, Media> implements BlockNode {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = Media.class)
    public List<Media> children() {
        return super.children();
    }

    public MediaGroup file(String id, String collection) {
        return add(Media.media(id, Media.Type.file, collection));
    }

    public MediaGroup file(String id, String collection, String occurrenceKey) {
        return add(Media.media(id, Media.Type.file, collection).occurrenceKey(occurrenceKey));
    }

    public MediaGroup link(String id, String collection) {
        return add(Media.media(id, Media.Type.link, collection));
    }

    public MediaGroup link(String id, String collection, String occurrenceKey) {
        return add(Media.media(id, Media.Type.link, collection).occurrenceKey(occurrenceKey));
    }

    public static MediaGroup mediaGroup(Media... elements) {
        return mediaGroup().add(elements);
    }

}
