package com.atlassian.stride.request;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.model.Scope;
import org.apache.http.entity.ContentType;

/**
 * Standard way for building a {@link Request}
 */
public interface RequestBuilder {

    RequestBuilder header(String name, String value);

    RequestBuilder contextConfig(ContextConfig contextConfig);

    RequestBuilder scope(Scope scope);

    RequestBuilder expect(Integer... statuses);

    RequestBuilder acceptJson();

    RequestBuilder acceptPlainText();

    RequestBuilder acceptHtml();

    RequestBuilder acceptBytes();

    RequestBuilder body(Object body);

    RequestBuilder contentType(ContentType contentType);

    <T> Request<T> build(Class<T> type);

    default UntypedRequest build() {
        return (UntypedRequest) build(Void.class);
    }

}
