package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Accessors(fluent = true, chain = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Lozenge extends UnknownPropertiesSupport {

    @Nonnull
    private String text;
    @JsonProperty("bold")
    private Boolean strong;
    private Appearance appearance = Appearance._default;

    @JsonIgnore
    public Lozenge makeStrong() {
        this.strong = Boolean.TRUE;
        return this;
    }

    public Lozenge removed() {
        return appearance(Appearance.removed);
    }

    public Lozenge success() {
        return appearance(Appearance.success);
    }

    public Lozenge inprogress() {
        return appearance(Appearance.inprogress);
    }

    public Lozenge _new() {
        return appearance(Appearance._new);
    }

    public Lozenge moved() {
        return appearance(Appearance.moved);
    }

    public static Lozenge lozenge(String text) {
        return new Lozenge(text);
    }

    public enum Appearance {
        @JsonProperty("default") _default, removed, success, inprogress, @JsonProperty("new") _new, moved
    }

}
