package com.atlassian.stride.api.model;

import com.atlassian.stride.model.context.ConversationContext;
import lombok.Data;

import javax.annotation.Nonnull;

/**
 * Maps the request object when posting a configuration update.
 */
@Data(staticConstructor = "of")
public class UpdateConfigurationRequest {

    @Nonnull
    private final ConversationContext context;
    private final boolean configured;

}
