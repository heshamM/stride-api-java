package com.atlassian.adf.inline;

import com.atlassian.adf.base.AttributeContainerMixin;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import java.util.SortedMap;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/code
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/em
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/link
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/strike
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/strong
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/subsup
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/textColor
 * https://developer.atlassian.com/cloud/stride/apis/document/marks/underline
 */
@Data(staticConstructor = "mark")
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Mark implements AttributeContainerMixin<Mark> {

    @JsonProperty("attrs")
    private SortedMap<String, Object> attributes;

    @Nonnull
    private String type;

    @JsonIgnore
    public SubSup subType() {
        return strAttribute("type")
                .map(String::toLowerCase)
                .map(SubSup::valueOf)
                .orElse(SubSup.none);
    }

    @JsonIgnore
    public String color() {
        return strAttribute("color").orElse("");
    }

    @JsonIgnore
    public String href() {
        return strAttribute("href").orElse("");
    }

    public static Mark strong() {
        return mark("strong");
    }

    public static Mark em() {
        return mark("em");
    }

    public static Mark code() {
        return mark("code");
    }

    public static Mark strike() {
        return mark("strike");
    }

    public static Mark sub() {
        return mark("subsup").attribute("type", SubSup.sub.name());
    }

    public static Mark sup() {
        return mark("subsup").attribute("type", SubSup.sup.name());
    }

    public static Mark underline() {
        return mark("underline");
    }

    public static Mark link(String href) {
        return mark("link").attribute("href", href);
    }

    public static Mark color(String hexColor) {
        return mark("textColor").attribute("color", hexColor);
    }

    public enum SubSup {
        none, sub, sup
    }

}
