package com.atlassian.adf.block.decision;

public enum DecisionState {
    DECIDED, UNDECIDED
}
