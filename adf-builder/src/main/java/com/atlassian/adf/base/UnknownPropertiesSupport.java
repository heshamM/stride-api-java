package com.atlassian.adf.base;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Allows objects mapping JSON to carry additional properties, even those not mapped in class fields.
 * <p>
 * Useful as the document format evolves, allowing you work with new properties without having to wait for
 * new releases of this library.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@EqualsAndHashCode
@ToString
@ParametersAreNonnullByDefault
public abstract class UnknownPropertiesSupport {

    private SortedMap<String, Object> other = new TreeMap<>();

    @JsonAnyGetter
    public Map<String, Object> other() {
        return other;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        other.put(name, value);
    }

    public boolean hasUnknowProperties() {
        return !other.isEmpty();
    }

}
