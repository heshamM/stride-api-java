package com.atlassian.adf.base;

import com.atlassian.adf.Node;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.SortedMap;

/**
 * Base for nodes containing attributes.
 * <p>
 * It contains a map holding the element {@code attrs} which is common for various nodes in the
 * Atlassian Document Format and some helper methods to access those attributes.
 *
 * @param <T> Concrete implementing class, used to allow fluent setters.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ParametersAreNonnullByDefault
public abstract class AbstractNode<T extends Node> extends UnknownPropertiesSupport implements Node, AttributeContainerMixin<T> {

    @JsonProperty("attrs")
    private SortedMap<String, Object> attributes;

    @SuppressWarnings("unchecked")
    @Override
    public T attributes(SortedMap<String, Object> attributes) {
        this.attributes = attributes;
        return (T) this;
    }

}
