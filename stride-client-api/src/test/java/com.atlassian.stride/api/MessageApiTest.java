package com.atlassian.stride.api;

import com.atlassian.adf.Document;
import com.atlassian.stride.api.model.EntityCreatedResponse;
import com.atlassian.stride.api.model.MessageWithLinks;
import com.atlassian.stride.model.context.Context;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

@Slf4j
public class MessageApiTest {

    private StrideClient stride = Helper.client();

    @Test
    public void sendAndFetch() throws Exception {
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        EntityCreatedResponse e = stride.message().send("hi")
                .toConversation(context)
                .get();
        log.info("Got message with id {}", e.getId());

        assertThat(e.getId(), not(isEmptyString()));

        MessageWithLinks m = stride.message().fetch()
                .one(e.getId())
                .from(context)
                .get();
        log.info("Fetched message: {}", e);

        assertThat(m.getMessage().getId(), is(e.getId()));
        Document doc = stride.document().of(m.getMessage());
        assertThat(Document.create().paragraph("hi"), is(doc));
    }

    @Test
    public void history() throws Exception {
        String messageId = "8024ab9a-e435-11e7-aaa5-02420aff0003";
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        val res = stride.message().fetch().history()
                .before(messageId)
                .limit(2)
                .from(context)
                .get();
        log.info("Got message history for id {}: ", messageId, res.getMessages());
        
        assertThat(res.getMessages(), hasSize(2));
        
        val m1 = res.getMessages().get(0);
        Document doc = stride.document().of(m1);
        assertThat(Document.create().paragraph("hi"), is(doc));
        assertThat(m1.getId(), not(messageId));
        
        val m2 = res.getMessages().get(1);
        Document doc2 = stride.document().of(m2);
        assertThat(Document.create().paragraph("I humbly wonder what you mean..."), is(doc2));
        assertThat(m2.getId(), not(messageId));
    }

    @Test
    public void historyUsingTimestampRange() throws Exception {
        String messageId = "8024ab9a-e435-11e7-aaa5-02420aff0003";
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        DateTimeFormatter dtf = DateTimeFormatter.ISO_INSTANT
                .withLocale(Locale.US)
                .withZone(ZoneId.of("UTC"));
        val res = stride.message().fetch().history()
                .after(dtf.parse("2017-12-18T20:24:29.2375962Z", Instant::from))
                .before(dtf.parse("2017-12-18T20:54:29.2375962Z", Instant::from))
                .from(context)
                .get();
        log.info("Got message history {}: ", res.getMessages());
        
        assertThat(res.getMessages(), hasSize(2));
        
        val m1 = res.getMessages().get(0);
        Document doc = stride.document().of(m1);
        assertThat(Document.create().paragraph("hi"), is(doc));
        assertThat(m1.getId(), is(messageId));
        
        val m2 = res.getMessages().get(1);
        Document doc2 = stride.document().of(m2);
        assertThat(Document.create().paragraph("hi"), is(doc2));
        assertThat(m2.getId(), not(messageId)); 
    }

    @Test
    public void context() throws Exception {
        String messageId = "8024ab9a-e435-11e7-aaa5-02420aff0003";
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        val res = stride.message().fetch().context(messageId)
                .before(2)
                .from(context)
                .get();
        log.info("Got message context for id {}: ", messageId, res.getMessages());
        
        assertThat(res.getMessages(), hasSize(3));
        
        val m1 = res.getMessages().get(0);
        Document doc = stride.document().of(m1);
        assertThat(Document.create().paragraph("hi"), is(doc));
        assertThat(m1.getId(), is(messageId));
        
        val m2 = res.getMessages().get(1);
        Document doc2 = stride.document().of(m2);
        assertThat(Document.create().paragraph("hi"), is(doc2));
        assertThat(m2.getId(), not(messageId));
        
        val m3 = res.getMessages().get(2);
        Document doc3 = stride.document().of(m3);
        assertThat(Document.create().paragraph("I humbly wonder what you mean..."), is(doc3));
        assertThat(m3.getId(), not(messageId));
    }

    @Test
    public void recent() throws Exception {
        String messageId = "8024ab9a-e435-11e7-aaa5-02420aff0003";
        ConversationContext context = Context.conversation(Helper.cloudId(), Helper.conversationId());
        val res = stride.message().fetch().recent()
                .limit(3)
                .from(context)
                .get();
        log.info("Got message context for id {}: ", messageId, res.getMessages());
        
        assertThat(res.getMessages(), hasSize(3));
    }

}