# Stride Authorization

This library authorizes requests from and to Stride:

- Incoming: validates JWT token making sure it's indeed Stride calling you, not a 3d party.
- Outgoing: generates JWT token and sign requests to Stride API.

This is an unofficial implementation of the API. You can learn more through the official 
[security documentation](https://developer.atlassian.com/cloud/stride/security/security-overview/).

## Signing outgoing requests

### Using the Stride Client API

Whenever you call Stride API you have to sign your requests. This library automates the work for you. 

This is accomplished by intercepting outgoing requests, generating a token with your 
credentials using the authorization API, then adding the token to the `Authentication` header.

### Token caching

The class responsible for token generation is - roll drums - `TokenGenerator`. However, by default 
`CachedTokenGenerator` will be used, which will use the JWT `exp` field to calculate a reasonable time
to refresh the token.

The cache invalidation can occur in two ways:

- When the current time is close to the expiration time. Stride's default expiration time is 1 hour and the 
default configuration expires the cache 1 minute earlier.
- If the server responds with the HTTP Status Unauthorized for an intercepted request: in this case the outgoing 
request interceptor will refresh the token and retry the request once more.

You can learn more about how to authenticate against the Stride API in the 
[Authentication documentation](https://developer.atlassian.com/cloud/stride/security/authentication/).

## Authorizing incoming requests

Requests from Stride to your APP will are signed with a JWT token that can be decoded and 
verified with your **oAuth Client Id** and **Secret** obtained from Stride.

In most of cases, the token is sent in the `Authentication` HTTP header. However, Stride client 
sometimes calls your App service directly, for instance to fetch a page to be displayed 
within a dialog or sidebar or also to fetch the glance status or configuration state. In these
specific cases the token can be found as a query parameter named `jwt`.

The class `TokenVerifierHelper` can be used to verify the token received from Stride. This
is done, for instance, by the class `TokenVerifierInterceptor` in the `stride-boot` project.

# The token itself

This library wraps the decoded token in an object of type `StrideJwtToken`. In the same way as the previous topic,
you can just add a parameter to your controller method of this type and it'll be automatically resolved.

An useful information you can obtain from `StrideJwtToken` is the oAuth Client Id. It's very important, for instance,
if you want your backend service to work in a multi-tenanted manner, distinguishing between different Apps
you created in DAC.