package com.atlassian.stride.auth.exception;

/**
 * Thrown when the token cannot be generated.
 */
public class FailedToObtainTokenException extends Exception {

    public FailedToObtainTokenException(String message) {
        super(message);
    }

    public FailedToObtainTokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
