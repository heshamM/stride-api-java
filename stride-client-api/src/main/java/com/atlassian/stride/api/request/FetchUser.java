package com.atlassian.stride.api.request;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.UserDetail;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.UserContext;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Fetch user details
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
public class FetchUser {

    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Fetch user details
     */
    public CompletableFuture<UserDetail> from(UserContext userContext) {
        return apiRequest.build(UserDetail.class, b -> b.acceptJson().scope(Scope.USER_VIEW_USER_PROFILE))
                .get(apiRequest.uri(StrideEndpoint.user.get(userContext)));
    }

}