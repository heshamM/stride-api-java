package com.atlassian.stride.model;

import lombok.AllArgsConstructor;
import lombok.Value;

/**
 * Model to return when you have defined a configuration page for your App in your descriptor uses and Stride calls
 * your service for information about whether your App is configured for a given context.
 * <p>
 * The endpoint must use CORS.
 */
@Value
@AllArgsConstructor(staticName = "of")
public class ConfigurationValueResponse {

    private boolean configured;

}