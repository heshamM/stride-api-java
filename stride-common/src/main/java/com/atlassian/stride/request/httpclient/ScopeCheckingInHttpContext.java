package com.atlassian.stride.request.httpclient;

import com.atlassian.stride.model.Scope;
import org.apache.http.protocol.HttpContext;

import java.util.Optional;

/**
 * Utility to attach and recover a {@link Scope} to the current Apache HTTP Request Context ({@link HttpContext}),
 * so later the token generated to access the API can be checked for the required scope defined for each
 * individual endpoint.
 */
public class ScopeCheckingInHttpContext {

    public static Optional<Scope> adapt(HttpContext httpContext) {
        return Optional.ofNullable((Scope) httpContext.getAttribute("stride.scope"));
    }

    public static HttpContext withContext(HttpContext httpContext, Scope scope) {
        if (scope != null) {
            httpContext.setAttribute("stride.scope", scope);
        }
        return httpContext;
    }
    
}
