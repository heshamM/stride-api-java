package com.atlassian.adf;

import com.atlassian.adf.inline.Emoji;
import com.atlassian.adf.inline.HardBreak;
import com.atlassian.adf.inline.Mention;
import com.atlassian.adf.inline.Text;
import com.atlassian.adf.inline.UnknownInlineNode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Interface representing inline nodes as in: https://developer.atlassian.com/cloud/stride/apis/document/structure
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = UnknownInlineNode.class)
@JsonSubTypes({
    @JsonSubTypes.Type(value = Text.class, name = "text"),
    @JsonSubTypes.Type(value = HardBreak.class, name = "hardBreak"),
    @JsonSubTypes.Type(value = Mention.class, name = "mention"),
    @JsonSubTypes.Type(value = Emoji.class, name = "emoji")
})
public interface InlineNode extends Node {

}
