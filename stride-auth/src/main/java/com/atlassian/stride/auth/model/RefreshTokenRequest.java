package com.atlassian.stride.auth.model;

import lombok.Builder;
import lombok.Data;

/**
 * Represents the request to get a fresh token.
 */
@Data
@Builder
public class RefreshTokenRequest {

    private String grant_type;
    private String client_id;
    private String client_secret;

}
