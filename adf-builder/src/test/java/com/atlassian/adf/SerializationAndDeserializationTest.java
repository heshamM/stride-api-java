package com.atlassian.adf;

import com.atlassian.adf.remoterenderer.RendererHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
public class SerializationAndDeserializationTest {

    static ObjectMapper mapper;

    @BeforeAll
    public static void init() throws Exception {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
    }

    @Test
    void paragraphWithoutFormatting() throws Exception {
        DocumentData data = load("paragraph-without-formatting.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void paragraphWithSimpleFormatting() throws Exception {
        DocumentData data = load("paragraph-with-simple-formatting.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void paragraphWithVariousFormatting() throws Exception {
        DocumentData data = load("paragraph-with-various-formats.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void paragraphWithLink() throws Exception {
        DocumentData data = load("paragraph-with-link.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void paragraphWithHardBrakes() throws Exception {
        DocumentData data = load("paragraph-with-hard-brakes.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void blockQuote() throws Exception {
        DocumentData data = load("block-quote.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void bulletList() throws Exception {
        DocumentData data = load("bullet-list.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void codeBlock() throws Exception {
        DocumentData data = load("code-block.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void headingWithFormatting() throws Exception {
        DocumentData data = load("heading-with-formatting.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void headingWithoutFormatting() throws Exception {
        DocumentData data = load("heading-without-formatting.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void orderedList() throws Exception {
        DocumentData data = load("ordered-list.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void panels() throws Exception {
        DocumentData data = load("panels.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void fullMessageTestWithVariousTypesOfNodes() throws Exception {
        DocumentData data = load("multiple-formatting-combinations.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void applicationCardSimple() throws Exception {
        DocumentData data = load("application-card-simple.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void applicationCardAdvanced() throws Exception {
        DocumentData data = load("application-card-advanced.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void costomerEmoji() throws Exception {
        DocumentData data = load("customer-emoji.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void decisionList() throws Exception {
        DocumentData data = load("decision-list.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void media() throws Exception {
        DocumentData data = load("media.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void mention() throws Exception {
        DocumentData data = load("mention.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void nonStandardEmoji() throws Exception {
        DocumentData data = load("non-standard-emoji.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void rule() throws Exception {
        DocumentData data = load("rule.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void unicodeEmoji() throws Exception {
        DocumentData data = load("unicode-emoji.json");
        data.assertThereAndBackAgain();
    }

    @Test
    void task() throws Exception {
        DocumentData data = load("task.json");
        data.assertThereAndBackAgain();
    }

    private DocumentData load(String resourceName) throws Exception {
        final String s = loadMessage(resourceName);
        return DocumentData.of(s, mapper.readValue(s, Document.class));
    }

    private String loadMessage(String resourceName) throws Exception {
        final URI uri = SerializationAndDeserializationTest.class.getResource("/messages/" + resourceName).toURI();
        return new String(Files.readAllBytes(Paths.get(uri)), StandardCharsets.UTF_8);
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    static class DocumentData {
        String original;
        Document document;

        String serializeDocument() throws Exception {
            String s = mapper.writeValueAsString(document);
            return mapper.writeValueAsString(mapper.readTree(original));
        }

        String reformatOriginal() throws Exception {
            return mapper.writeValueAsString(mapper.readTree(original));
        }

        void assertThereAndBackAgain() throws Exception {
            String reserialied = serializeDocument();
            //assert serialized is the same as the original document
            assertEquals(reformatOriginal(), reserialied);

            String renderedHtml = RendererHelper.renderHtml(reserialied);
            log.info("Document rendered to: [{}]", renderedHtml);
            //assert that the document is valid
            assertNotNull(renderedHtml);
            assertNotEquals("", renderedHtml);
        }
    }

}
