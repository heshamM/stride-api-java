package com.atlassian.stride.spring.config.model;

import lombok.Value;

import javax.annotation.Nonnull;

/**
 * Holds the external URL for your APP. It exists only to facilitate you to generate the {@code baseUrl} in your
 * descriptor automatically.
 * <p>
 * This is dynamically created by {@link com.atlassian.stride.spring.config.suppliers.DefaultServiceConfigSupplier#init}.
 */
@Value(staticConstructor = "of")
public class ServiceConfig {

    @Nonnull
    private String externalUrl;

}
