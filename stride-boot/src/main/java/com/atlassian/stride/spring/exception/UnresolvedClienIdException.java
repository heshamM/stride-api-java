package com.atlassian.stride.spring.exception;

/**
 * Thrown when the requested client id is not configured in your service
 */
public class UnresolvedClienIdException extends RuntimeException {

    private static final String message = "Configuration not available for client: ";

    public UnresolvedClienIdException(String clientId) {
        super(message + clientId);
    }

}
