package com.atlassian.adf.inline;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * BlocksMaps a possible unknown inline node.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UnknownInlineNode extends AbstractNode<UnknownInlineNode> implements InlineNode {

    @JsonProperty
    @JsonAnySetter
    private Map<String, JsonNode> properties;

    public String text() {
        return strAttribute("text").orElse("");
    }

}
