# Stride Api for Java  

A set of libraries to develop Stride Apps:

- [Atlassian Document Format Builder](adf-builder): building complex messages could not be easier, including fluent builders and a Markdown support
- [Common components and configruation](stride-common): common models and components for Stride Apps
- [Stride Authorization](stride-auth): secures requests to and from Stride 
- [Stride Client API](stride-client-api): exposes a simple and powerful asynchronous API for Stride
- [Stride + Spring Boot](stride-boot): goodies to create your Stride App without  

Examples of Apps using these libs:

- [Beloved Messages](https://bitbucket.org/atluiz/stride-app-favorite-messages): full-blown Stride App with a modern frontend using React.js and AtlasKit that allows users like, save, and pin messages. 
- [Java RefApp](https://bitbucket.org/atluiz/stride-java-refapp): Java flavor of the official [RefApp](https://bitbucket.org/atlassian/stride-api-tutorial)
- [Hello Bot](https://bitbucket.org/atluiz/stride-java-hello-bot): a very simple Stride Bot that reacts according to a customizable script

***Contributions are welcome!***

## Stack

- Spring Boot 
- Jackson 2
- JWT support with [Nimbus JOSE + JWT](https://connect2id.com/products/nimbus-jose-jwt)
- Markdown support with [commonmark-java](https://github.com/atlassian/commonmark-java)
- Apache HTTP Client
- Lombok
- Slf4j + Logback
- JUnit Jupiter

## Compiling projects

Shortcuts at the base project dir:

```bash
# shortcut to: mvn clean install -DskipTests 

./build all    # all libs
./build adf    # adf-builder 
./build client # stride-client-api
./build auth   # stride-auth
./build common # stride-common
./build boot   # stride-boot
```

## Testing

Run tests normally with Maven or your IDE. 

Some integration tsts may need you to set up some environment variables first, namely:

- `STRIDE_TEST_APP_CLIENT_ID`
- `STRIDE_TEST_APP_SECRET`
- `STRIDE_TEST_CLOUD_ID`
- `STRIDE_TEST_USER_ID`
- `STRIDE_TEST_USER_EMAIL`
- `STRIDE_TEST_CONVERSATION_ID`
- `STRIDE_TEST_CONVERSATION_NAME`

