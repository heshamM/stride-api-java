package com.atlassian.adf.markdown;

import com.atlassian.adf.Document;
import com.atlassian.adf.block.codeblock.Language;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MarkdownSupportTest {
    
    @Test
    void simpleMarkdownToDocument() throws Exception {
        Document doc = Document.fromMarkdown("hello world");
        Document ref = Document.create().paragraph("hello world");
        assertEquals(ref, doc);
    }

    @Test
    void multilineText() throws Exception {
        Document doc = Document.fromMarkdown("hello\nworld");
        Document ref = Document.create().paragraph(p -> p.text("hello").text("world"));
        assertEquals(ref, doc);
    }

    @Test
    void multilineTextWithSpaces() throws Exception {
        Document doc = Document.fromMarkdown("hello  \nworld");
        Document ref = Document.create().paragraph(p -> p.text("hello").hardBreak().text("world"));
        assertEquals(ref, doc);
    }

    @Test
    void multilineQuotes() throws Exception {
        Document doc = Document.fromMarkdown("> Roses are red\nRoses are blue");
        Document ref = Document.create().quote(b -> b.paragraph(p -> p.text("Roses are red").text("Roses are blue")));
        assertEquals(ref, doc);
    }

    @Test
    void multilineQuotesWithSpaces() throws Exception {
        Document doc = Document.fromMarkdown("> Roses are red  \n> Roses are blue");
        Document ref = Document.create().quote(b -> b.paragraph(p -> p.text("Roses are red").hardBreak().text("Roses are blue")));
        assertEquals(ref, doc);
    }

    @Test
    void multilineQuotesWithSpacesNotRepatingQuoteMark() throws Exception {
        Document doc = Document.fromMarkdown("> Roses are red  \nRoses are blue");
        Document ref = Document.create().quote(b -> b.paragraph(p -> p.text("Roses are red").hardBreak().text("Roses are blue")));
        assertEquals(ref, doc);
    }

    @Test
    void multilineQuotesWithEmptyLines() throws Exception {
        Document doc = Document.fromMarkdown("> Roses are red\n>\n> Roses are blue");
        Document ref = Document.create().quote(b -> b.paragraph("Roses are red").paragraph("Roses are blue"));
        assertEquals(ref, doc);
    }

    @Test
    void emphasis() throws Exception {
        Document doc = Document.fromMarkdown("hello *world*");
        Document ref = Document.create().paragraph(p -> p.text("hello ").em("world"));
        assertEquals(ref, doc);
        Document doc2 = Document.fromMarkdown("hello _world_");
        assertEquals(doc2, ref);
    }

    @Test
    void strong() throws Exception {
        Document ref = Document.create().paragraph(p -> p.text("hello ").strong("world"));
        Document doc = Document.fromMarkdown("hello **world**");
        assertEquals(ref, doc);
        Document doc2 = Document.fromMarkdown("hello __world__");
        assertEquals(doc2, ref);
    }

    @Test
    void heading() throws Exception {
        Document ref = Document.create().h2(h -> h.text("title_").strong("strong"));
        Document doc = Document.fromMarkdown("## title_**strong**");
        assertEquals(ref, doc);
    }

    @Test
    void headingOneWithTraces() throws Exception {
        Document ref = Document.create()
                .h1("p1")
                .paragraph("p2");
        Document doc = Document.fromMarkdown("p1\n===\np2");
        assertEquals(ref, doc);
    }

    @Test
    void headingTwoWithTraces() throws Exception {
        Document ref = Document.create()
                .h2("p1")
                .paragraph("p2");
        Document doc = Document.fromMarkdown("p1\n---\np2");
        assertEquals(ref, doc);
    }

    @Test
    void link() throws Exception {
        Document ref = Document.create().paragraph(h -> h.link("link", "atlassian.com"));
        Document doc = Document.fromMarkdown("[link](atlassian.com)");
        assertEquals(ref, doc);
    }

    @Test
    void code() throws Exception {
        Document ref = Document.create().paragraph(h -> h.text("my ").code("java").text(" code"));
        Document doc = Document.fromMarkdown("my `java` code");
        assertEquals(ref, doc);
    }

    @Test
    void blockQuote() throws Exception {
        Document ref = Document.create().quote(q -> q.paragraph("quote"));
        Document doc = Document.fromMarkdown("> quote");
        assertEquals(ref, doc);
    }

    @Test
    void codeBlock() throws Exception {
        Document ref = Document.create().code("my code");
        Document doc = Document.fromMarkdown("    my code");
        assertEquals(ref, doc);
    }

    @Test
    void codeBlockWithBacktips() throws Exception {
        Document ref = Document.create().code("my code\n\nmore code");
        Document doc = Document.fromMarkdown("```\nmy code\n\nmore code\n```");
        assertEquals(ref, doc);
    }

    @Test
    void codeBlockWithBacktipsAndLanguage() throws Exception {
        Document ref = Document.create().code(Language.java, "my code\n\nmore code");
        Document doc = Document.fromMarkdown("```java\nmy code\n\nmore code\n```");
        assertEquals(ref, doc);
    }

    @Test
    void codeBlockWithBacktipsAndLanguageSynonym() throws Exception {
        Document ref = Document.create().code(Language.javascript, "my code\n\nmore code");
        Document doc = Document.fromMarkdown("```js\nmy code\n\nmore code\n```");
        assertEquals(ref, doc);
    }

    @Test
    void bulletList() throws Exception {
        Document ref = Document.create().bulletList(l -> l
                .item(i -> i.paragraph("item1"))
                .item(i -> i.paragraph("item2")));
        Document doc = Document.fromMarkdown("* item1\n* item2");
        assertEquals(ref, doc);
    }

    @Test
    void orderedList() throws Exception {
        Document ref = Document.create().orderedList(l -> l
                .item(i -> i.paragraph("item1"))
                .item(i -> i.paragraph("item2")));
        Document doc = Document.fromMarkdown("1. item1\n2. item2");
        assertEquals(ref, doc);
    }

    @Test
    void rule() throws Exception {
        Document ref = Document.create()
                .paragraph("p1")
                .rule()
                .paragraph("p2");
        Document doc = Document.fromMarkdown("p1\n\n---\np2");
        assertEquals(ref, doc);
    }

    @Test
    void blockQuoteWithBulletList() throws Exception {
        Document ref = Document.create().quote(q -> q.bulletList(l -> l
                .item(i -> i.paragraph("item1"))
                .item(i -> i.paragraph("item2"))));
        Document doc = Document.fromMarkdown("> - item1\n> - item2");
        assertEquals(ref, doc);
    }

    @Test
    void blockQuoteWithOrderedList() throws Exception {
        Document ref = Document.create().quote(q -> q.orderedList(l -> l
                .item(i -> i.paragraph("item1"))
                .item(i -> i.paragraph("item2"))));
        Document doc = Document.fromMarkdown("> 1) item1\n> 2) item2");
        assertEquals(ref, doc);
    }

    @Test
    void exceptionOnUnsupporrtedMarkdown() throws Exception {
        UnsupportedMarkdownFormatException exception = assertThrows(UnsupportedMarkdownFormatException.class,
                () -> Document.fromMarkdown("![img](/img.jpg)", true));
        assertEquals("Image nodes not supported", exception.getMessage());
    }

    @Test
    void ignoreeExceptionOnUnsupporrtedMarkdown() throws Exception {
        Document.fromMarkdown("![img](/img.jpg)");
    }

}