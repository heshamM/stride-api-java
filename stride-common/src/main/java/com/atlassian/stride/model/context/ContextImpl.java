package com.atlassian.stride.model.context;

import lombok.Builder;
import lombok.Data;

/**
 * Context implementation with all possible attributes. Do not use it directly but use the specific interfaces instead.
 */
@Data(staticConstructor = "of")
@Builder
public class ContextImpl implements SiteContext, ConversationContext, UserContext, UserInConversationContext, Context {

    private final String cloudId;
    private final String conversationId;
    private final String userId;

}
