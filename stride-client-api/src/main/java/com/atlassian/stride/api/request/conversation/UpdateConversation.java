package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.model.ConversationUpdateRequest;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.stride.model.webhooks.Conversation.PRIVACY_PRIVATE;
import static com.atlassian.stride.model.webhooks.Conversation.PRIVACY_PUBLIC;

/**
 * Fetch a conversation
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class UpdateConversation {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private ConversationContext context;
    private String name;
    private String privacy = PRIVACY_PUBLIC;
    private String topic;

    public UpdateConversation using(Conversation conversation) {
        this.name = conversation.getName();
        this.privacy = conversation.getPrivacy();
        this.topic = conversation.getTopic();
        this.context = conversation.asContext();
        return this;
    }

    public UpdateConversation makePublic() {
        this.privacy = PRIVACY_PUBLIC;
        return this;
    }

    public UpdateConversation makePrivate() {
        this.privacy = PRIVACY_PRIVATE;
        return this;
    }

    /**
     * Update conversation
     */
    public CompletableFuture<Void> on(ConversationContext context) {
        return apiRequest.build(b -> b
                .scope(Scope.STRIDE_MANAGE_CONVERSATION)
                .expect(HttpStatus.SC_NO_CONTENT)
                .body(ConversationUpdateRequest.builder().name(name).privacy(privacy).topic(topic).build()))
                .patch(apiRequest.uri(StrideEndpoint.conversation.manage.update(context)));
    }

    /**
     * Update conversation. Requires attribute {@code context} to be defined.
     */
    public CompletableFuture<Void> send() {
        Objects.requireNonNull(context, "context");
        return on(context);
    }
    
}