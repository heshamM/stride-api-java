package com.atlassian.stride.exception;

/**
 * Thrown when the response to an endpoint unexpectedly returned an empty body
 */
public class UnexpectedEmptyResponseException extends RuntimeException {

    public UnexpectedEmptyResponseException(String message) {
        super(message);
    }

    public UnexpectedEmptyResponseException(String message, Throwable cause) {
        super(message, cause);
    }

}
