package com.atlassian.stride.spring.config.model;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.config.DefaultContextConfig;
import com.atlassian.stride.config.EnvironmentConfig;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nonnull;

/**
 * General App Configuration according to DAC (developer.atlassian.com).
 */
@NoArgsConstructor
@Data
@Validated
public class AppConfig {

    @Nonnull
    private String clientId;
    @Nonnull
    private String secret;
    @NestedConfigurationProperty
    private CustomEnvironmentConfig environment;
    private String userId;
    private String dacAppId;

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private ContextConfig contextConfig;

    /**
     * The environment to where this configuration points to
     */
    public EnvironmentConfig getEnvironmentConfig() {
        return environment == null ? EnvironmentConfig.production : environment.getEnvironmentConfig();
    }

    /**
     * Creates a {@link ContextConfig} based on the App configuration
     */
    public ContextConfig getContextConfig() {
        if (contextConfig == null) {
            contextConfig = DefaultContextConfig.of(getClientId(), getSecret(), getEnvironmentConfig());
        }
        return contextConfig;
    }
}
