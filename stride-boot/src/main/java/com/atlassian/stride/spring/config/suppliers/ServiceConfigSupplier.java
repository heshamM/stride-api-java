package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.spring.config.model.ServiceConfig;

/**
 * Supplies service configuration, namely: external URL.
 */
public interface ServiceConfigSupplier {

    /**
     * Fetch service configuration
     */
    ServiceConfig get();
    
}
