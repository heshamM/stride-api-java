package com.atlassian.adf;

import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.codeblock.Language;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DocumentCreationTest {
    static JsonSchema schema;
    static ObjectMapper mapper;

    @BeforeAll
    public static void init() throws Exception {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
        schema = JsonSchemaFactory.newBuilder().freeze().getJsonSchema("resource:/adf-schema.json");
    }

    @Test
    void paragraphWithoutFormatting() throws Exception {
        DocumentData data = load("paragraph-without-formatting.json");
        Document doc = Document.create()
                .paragraph("Hello");
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void paragraphWithSimpleFormatting() throws Exception {
        DocumentData data = load("paragraph-with-simple-formatting.json");
        Document doc = Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void paragraphWithVariousFormatting() throws Exception {
        DocumentData data = load("paragraph-with-various-formats.json");
        Document doc = Document.create().paragraph(p -> p
                .text("Text ")
                .em("with ")
                .code("format")
                .text(" ")
                .strike("and")
                .text(" ")
                .underline("other")
                .text(" ")
                .sup("things")
                .text(". "));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void paragraphWithLink() throws Exception {
        DocumentData data = load("paragraph-with-link.json");
        Document doc = Document.create().paragraph(p -> p
                .link("A link", "http://google.com")
                .text(":"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void paragraphWithHardBrakes() throws Exception {
        DocumentData data = load("paragraph-with-hard-brakes.json");
        Document doc = Document.create().paragraph(p -> p
                .text("Multi")
                .hardBreak()
                .text("Line")
                .hardBreak()
                .text("No")
                .hardBreak()
                .text("Paragraph"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void blockQuote() throws Exception {
        DocumentData data = load("block-quote.json");
        Document doc = Document.create().quote(b -> b.paragraph("Quote me"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void bulletList() throws Exception {
        DocumentData data = load("bullet-list.json");
        Document doc = Document.create().bulletList(l -> l
                .item(i -> i.paragraph("An unordered"))
                .item(i -> i.paragraph("List")));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void codeBlock() throws Exception {
        DocumentData data = load("code-block.json");
        Document doc = Document.create().code(Language.java,"public static javaCodeBlock() {\n   asd();\n}");
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void codeBlockWithUniqueId() throws Exception {
        load("code-block-with-unique-id.json");
    }

    @Test
    void headingWithFormatting() throws Exception {
        DocumentData data = load("heading-with-formatting.json");
        Document doc = Document.create().head(Heading.Level.H4, p -> p.text("Title ").link("link", "google.com"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void headingWithoutFormatting() throws Exception {
        DocumentData data = load("heading-without-formatting.json");
        Document doc = Document.create().h1(p -> p.text("Title"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void orderedList() throws Exception {
        DocumentData data = load("ordered-list.json");
        Document doc = Document.create().orderedList(l -> l
                .item(i -> i.paragraph("numbered"))
                .item(i -> i.paragraph("list")));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void panels() throws Exception {
        DocumentData data = load("panels.json");
        Document doc = Document.create()
                .warning(p -> p.paragraph("alert"))
                .info(p -> p.paragraph("Info"))
                .tip(p -> p.paragraph("idea"))
                .note(p -> p.paragraph("note"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void fullMessageTestWithVariousTypesOfNodes() throws Exception {
        DocumentData data = load("multiple-formatting-combinations.json");
        Document doc = Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!"))
                .h1(p -> p.text("Title"))
                .paragraph(p -> p
                        .text("Text ")
                        .em("with ")
                        .code("format")
                        .text(" ")
                        .strike("and")
                        .text(" ")
                        .underline("other")
                        .text(" ")
                        .sup("things")
                        .sub(". "))
                .paragraph(p -> p.link("A link", "http://google.com").text(":"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .paragraph("A:")
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .paragraph(p -> p.color("asda", "#97a0af"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .paragraph(p -> p
                                .text("Multi")
                                .hardBreak()
                                .text("Line")
                                .hardBreak()
                                .text("No")
                                .hardBreak()
                                .text("Paragraph"))
                .warning(p -> p.paragraph("alert"))
                .info(p -> p.paragraph("Info"))
                .tip(p -> p.paragraph("idea"))
                .note(p -> p.paragraph("note"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void simpleApplicationCard() throws Exception {
        DocumentData data = load("application-card-simple.json");
        Document doc = Document.create().card("Donovan Kolbly updated a file: applicationCard.md", c -> c
                        .attrs().text("Donovan Kolbly updated a file: applicationCard.md"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void advancedApplicationCard() throws Exception {
        DocumentData data = load("application-card-advanced.json");
        Document doc = Document.create().card("Donovan Kolbly updated a file: applicationCard.md", c -> c.attrs()
                .text("some text")
                .title(t -> t.user("https://www.gravatar.com/avatar/c3c9338e575a73892b0f1257eb9ee997", "Donovan Kolbly"))
                .link("https://atlassian.com")
                .collapsible(true)
                .description("\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis varius mattis massa, quis ornare orci. Integer congue\nrutrum velit, quis euismod eros condimentum quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris\nlobortis nibh id odio egestas luctus. Nunc nulla lacus, congue eu nibh non, imperdiet varius lacus. Nulla sagittis\nmagna et tincidunt volutpat. Nunc augue lorem, eleifend et tempor ut, malesuada ac lorem. Praesent quis feugiat eros,\net vehicula nibh. Maecenas vehicula commodo nisi, at rutrum ipsum posuere sit amet. Integer sit amet nisl sed ligula\nconsectetur feugiat non at ligula. Cras dignissim suscipit magna at mattis. Maecenas ante leo, feugiat vestibulum velit\na, commodo finibus velit. Maecenas interdum ullamcorper velit non suscipit. Proin tempor, magna vitae dapibus laoreet,\nquam dui convallis lectus, in vestibulum arcu eros eu velit. Quisque vel dolor enim.\n")
                .context("Stride Documentation / ... / Nodes", "https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png", "stride")
                .preview("https://image.ibb.co/kkFYW5/screenshot_13.png")
                .detail(d -> d.icon("https://image.ibb.co/fUViW5/board.png", "Issue type").text("Story"))
                .detail(d -> d.badge(101, b -> b.max(99L).important()))
                .detail(d -> d.lozenge("Nearly Complete", l -> l.inprogress()))
                .detail(d -> d.title("Watchers")
                        .user("https://www.gravatar.com/avatar/5db869c9686a1d191f99fc153c4d118c.jpg", "Kitty")
                        .user("https://www.gravatar.com/avatar/440f0328de63d671bf337779b4eece44.jpg", "Puppy"))
                .action("My action", "ap", "k"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void decisionList() throws Exception {
        DocumentData data = load("decision-list.json");
        Document doc = Document.create().decisionList("list-test-id", l -> l
                .addDecided("item-test-id", d -> d.text("Heading"))
                .addUndecided("item-test-id-2", d -> d.text("Another")));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void unicodeEmoji() throws Exception {
        DocumentData data = load("unicode-emoji.json");
        Document doc = Document.create().paragraph(p -> p
                .emoji(":grinning:", "😀"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void customerEmoji() throws Exception {
        DocumentData data = load("customer-emoji.json");
        Document doc = Document.create().paragraph(p -> p
                .emoji(":thumbsup::skin-tone-2:", "👍🏽", "1f44d"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void nonStandardEmoji() throws Exception {
        DocumentData data = load("non-standard-emoji.json");
        Document doc = Document.create().paragraph(p -> p
                .emoji(":awthanks:", ":awthanks:", "atlassian-awthanks"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void mention() throws Exception {
        DocumentData data = load("mention.json");
        Document doc = Document.create().paragraph(p -> p
                .mention("123456:abcdef00-4ccc-4bbb-dddd-111111111111", "@Bradley Ayers"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void rule() throws Exception {
        DocumentData data = load("rule.json");
        Document doc = Document.create().paragraph("Hello world").rule();
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void task() throws Exception {
        DocumentData data = load("task.json");
        Document doc = Document.create().taskList("test-list-id", l -> l
                .todo("test-id", "Do this!")
                .done("test-id", "This is completed"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void media() throws Exception {
        DocumentData data = load("media.json");
        Document doc = Document.create().mediaGroup(m -> m
                .file("6e7c7f2c-dd7a-499c-bceb-6f32bfbf30b5", "ae730abd-a389-46a7-90eb-c03e75a45bf1")
                .link("6e7c7f2c-dd7a-499c-bceb-6f32bfbf30b5", "ae730abd-a389-46a7-90eb-c03e75a45bf1"));
        data.assertDocumentEqualsAndSerializedEquals(doc);
    }

    @Test
    void randomParagraphTest() throws Exception {
        Document doc = Document.create()
                .paragraph(p -> p
                        .text("Hello ")
                        .mention("123", "joe")
                        .text(", please ")
                        .em("carefully")
                        .text(" read ")
                        .link("this contract", "https://www.example.com/contract"));
        schema.validate(mapper.valueToTree(doc));
        String s = mapper.writeValueAsString(doc);
        assertNotNull(s);
        assertNotEquals(s.length(), 0);
    }

    @Test
    void randomApplicationCardTest() throws Exception {
        Document doc = Document.create()
                .card("Title", c -> c.attrs()
                        .background("https://example.com/bg.png")
                        .link("https://example.com/something")
                        .description("Some description")
                        .detail(d -> d
                                .title("Status")
                                .text("Open")
                                .icon("https://example.com/open.png", "Not resolved yet")));

        val p = schema.validate(mapper.valueToTree(doc));
        assertTrue(p.isSuccess(), () -> p.iterator().next().toString());
    }

    @Test
    void advancedFormattingFromRefApp() throws Exception {
        Document doc = Document.create().paragraph(p -> p
                .text("Here is some ")
                .strong("bold test")
                .text(" and ")
                .em("text in italics")
                .text(" as well as ")
                .link(" a link", "https://www.atlassian.com")
                .text(" , emojis ")
                .emoji(":smile:")
                .emoji(":rofl:")
                .emoji(":nerd:")
                .text(" and some code: ")
                .code("var i = 0;")
                .text(" and a bullet list"))
                .bulletList(b -> b
                        .item("With one bullet point")
                        .item("And another"))
                .info(p -> p.paragraph("and an info panel with some text, with some more code below"))
                .code(Language.javascript, "var i = 0;\nwhile(true) {\n  i++;\n}")
                .paragraph("And a card")
                .card("With a title", c -> c.attrs()
                        .link("https://www.atlassian.com")
                        .description("With some description, and a couple of attributes")
                        .background("https://www.atlassian.com")
                        .detail(d -> d
                                .title("Type")
                                .text("Task")
                                .icon("https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype", "Task"))
                        .detail(d -> d
                                .title("User")
                                .text("Joe Blog")
                                .icon("https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype", "Task")));

        val p = schema.validate(mapper.valueToTree(doc));
        assertTrue(p.isSuccess(), () -> p.iterator().next().toString());
    }

    private DocumentData load(String resourceName) throws Exception {
        final String s = loadMessage(resourceName);
        return DocumentData.of(s, mapper.readValue(s, Document.class));
    }

    private String loadMessage(String resourceName) throws Exception {
        final URI uri = DocumentCreationTest.class.getResource("/messages/" + resourceName).toURI();
        return new String(Files.readAllBytes(Paths.get(uri)), StandardCharsets.UTF_8);
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    static class DocumentData {
        String original;
        Document document;

        String reformatOriginal() throws Exception {
            return mapper.writeValueAsString(mapper.readTree(original));
        }

        void assertDocumentEqualsAndSerializedEquals(Document documentCreated) throws Exception {
            String newDocument = mapper.writeValueAsString(documentCreated);
            assertEquals(reformatOriginal(), newDocument);
            assertEquals(document, documentCreated);

            ProcessingReport p = schema.validate(mapper.readTree(newDocument));
            assertTrue(p.isSuccess(), () -> {
                StringBuilder sb = new StringBuilder();
                p.forEach(m -> {
                    sb.append("Error:");
                    sb.append(m.getMessage());
                    sb.append(m.asException());
                    sb.append(" \n ");
                });
                sb.append("\n\n[");
                sb.append(newDocument);
                sb.append("]");
                return sb.toString();
            });
        }

        public String toString() {
            return original;
        }
    }

}
