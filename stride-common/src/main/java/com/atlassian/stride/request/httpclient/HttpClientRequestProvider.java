package com.atlassian.stride.request.httpclient;

import com.atlassian.stride.request.RequestBuilder;
import com.atlassian.stride.request.RequestProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * {@link RequestProvider} based on Apache HTTP Client
 */
@AllArgsConstructor(staticName = "of")
public class HttpClientRequestProvider implements RequestProvider {

    private final CloseableHttpClient client;
    private final ObjectMapper mapper;

    public static HttpClientRequestProvider def() {
        return of(HttpClients.createSystem(), new ObjectMapper());
    }

    @Override
    public RequestBuilder builder() {
        return new HttpClientRequestBuilder(client, mapper);
    }

}
