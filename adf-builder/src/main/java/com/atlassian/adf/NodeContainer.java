package com.atlassian.adf;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * Represents a node that contains child nodes.
 */
@ParametersAreNonnullByDefault
public interface NodeContainer<C extends Node> extends Node {

    /**
     * List of child elements.
     */
    @Nullable
    List<C> children();

    /**
     * Set child elements
     */
    NodeContainer children(List<C> children);

    /**
     * Add element
     */
    NodeContainer add(C child);

    /**
     * Add elements
     */
    NodeContainer add(C... children);

    /**
     * Add elements
     */
    NodeContainer add(Collection<C> children);

    /**
     * Will call the predicate for every node except in the main document with node and parent, respectively.
     * <p>
     * Interrupt the visiting returning {@code false}, thus return {@code true} by default.
     */
    default boolean visitAll(BiPredicate<Node, NodeContainer<? extends Node>> p) {
        final List<C> c = children();
        if (c != null) {
            for (Node n : c) {
                if (!p.test(n, this)) {
                    return false;
                }
                if (n instanceof NodeContainer) {
                    if (!((NodeContainer) n).visitAll(p)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Find all nodes matching the predicate.
     */
    @SuppressWarnings("unchecked")
    default <T extends Node> List<T> findAll(BiPredicate<Node, NodeContainer<? extends Node>> p) {
        final List<C> children = children();
        final List<T> result = new ArrayList<>();
        if (children != null) {
            for (Node n : children) {
                if (p.test(n, this)) {
                    result.add((T) n);
                }
                if (n instanceof NodeContainer) {
                    result.addAll(((NodeContainer) n).findAll(p));
                }
            }
        }
        return result;
    }

    /**
     * Find all nodes of type {@code T}
     */
    @SuppressWarnings("unchecked")
    default <T extends Node> List<T> findAll(Class<T> type) {
        return findAll((node, parent) -> type.isInstance(node));
    }

    /**
     * Filter nodes by removing those that has the predicate returning {@code false}.
     * <p>
     * if {@code removeChildlessNodes} is {@code true}, it'll iteratively remove nodes that become childless after
     * having their children removed
     * <p>
     * Returns {@code true} if, after filtering its children, the node still contains one or more elements
     * or {@code false} if it's now empty.
     */
    default boolean filter(BiPredicate<Node, NodeContainer<? extends Node>> p, boolean removeChildlessNodes) {
        final List<C> c = children();
        if (c != null) {
            for (Iterator<C> iterator = c.iterator(); iterator.hasNext(); ) {
                final C n = iterator.next();
                if (p.test(n, this)) {
                    if (n instanceof NodeContainer) {
                        if (!((NodeContainer) n).filter(p, removeChildlessNodes) && removeChildlessNodes) {
                            iterator.remove();
                        }
                    }
                } else {
                    iterator.remove();
                }
            }
            return !c.isEmpty();
        }
        return false;
    }

    /**
     * Remove all nodes in the provided collection.
     * <p>
     * if {@code removeChildlessNodes} is {@code true}, it'll iteratively remove nodes that become childless after
     * having their children removed
     * <p>
     * Returns {@code true} if, after filtering its children, the node still contains one or more elements
     * or {@code false} if it's now empty.
     */
    default void removeAll(Collection<Node> nodes, boolean removeChildlessNodes) {
        final HashSet<Node> s = new HashSet<>(nodes);
        filter((node, parent) -> !s.contains(node), removeChildlessNodes);
    }

}
