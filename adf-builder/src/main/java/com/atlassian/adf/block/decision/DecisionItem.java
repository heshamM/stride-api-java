package com.atlassian.adf.block.decision;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.InlineNodeContainer;
import com.atlassian.adf.base.AbstractInlineNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/decisionItem
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("decisionItem")
public class DecisionItem extends AbstractInlineNodeContainer<DecisionItem> implements InlineNodeContainer<DecisionItem> {

    @JsonIgnore
    public String localId() {
        return strAttribute("localId").orElse("");
    }

    @JsonIgnore
    public DecisionItem localId(String localId) {
        return attribute("localId", localId);
    }

    @JsonIgnore
    public DecisionState state() {
        return strAttribute("state").map(String::toUpperCase).map(DecisionState::valueOf).orElse(DecisionState.UNDECIDED);
    }

    @JsonIgnore
    public DecisionItem state(DecisionState state) {
        return attribute("state", state.name());
    }

    private DecisionItem() {
    }

    public static DecisionItem decided(String localId, InlineNode... elements) {
        return new DecisionItem().add(elements).localId(localId).state(DecisionState.DECIDED);
    }

    public static DecisionItem undecided(String localId, InlineNode... elements) {
        return new DecisionItem().add(elements).localId(localId).state(DecisionState.UNDECIDED);
    }

}
