package com.atlassian.stride.model.webhooks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

/**
 * Maps conversations details as received by WebHook requests.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Conversation extends UnknownPropertiesSupport {
    public static final String PRIVACY_PUBLIC = "public";
    public static final String PRIVACY_PRIVATE = "private";

    private String id;
    private String avatarUrl;
    private boolean isArchived;
    private String name;
    private String privacy;
    private String topic;
    private String type;
    private Date modified;
    private Date created;

    public boolean isPublic() {
        return "public".equals(privacy);
    }

    public boolean isPrivate() {
        return "private".equals(privacy);
    }
}
