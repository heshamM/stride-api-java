package com.atlassian.stride.request;

/**
 * Provides a {@link RequestBuilder} so users can have requests configured properly and implementations can reuse in
 * various places
 */
public interface RequestProvider {

    RequestBuilder builder();

}
