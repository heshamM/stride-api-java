package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * UserGetCommon represents identity service user
 * TODO see where it might be used
 */
@Data
public class User extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;
    private String avatarURL;
    private String displayName;
    private String id;
    private String timezone;
    private String userName;

}

