package com.atlassian.adf.markdown;

/**
 * Unchecked exception thrown when we can't convert Markdown to a {@link com.atlassian.adf.Document}
 */
public class UnsupportedMarkdownFormatException extends RuntimeException {
    public UnsupportedMarkdownFormatException(String message) {
        super(message);
    }
}
