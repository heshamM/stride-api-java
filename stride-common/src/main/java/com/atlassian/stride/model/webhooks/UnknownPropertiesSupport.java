package com.atlassian.stride.model.webhooks;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;

/**
 * Allows objects mapping JSON to carry additional properties.
 * It might be useful if at some point there are new properties you need to fetch before this library is updated.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@EqualsAndHashCode
@ToString
@ParametersAreNonnullByDefault
public abstract class UnknownPropertiesSupport {

    private Map<String, Object> other = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> other() {
        return other;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        other.put(name, value);
    }

    public boolean hasUnknowProperties() {
        return !other.isEmpty();
    }

}
