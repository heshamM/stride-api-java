package com.atlassian.adf.base;

import com.atlassian.adf.Node;
import com.atlassian.adf.NodeContainer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Base class for nodes containing children of a generic type.
 *
 * @param <T> Concrete implementing class, used to allow fluent setters.
 * @param <C> Type of the children.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ParametersAreNonnullByDefault
public abstract class AbstractNodeContainer<T extends NodeContainer, C extends Node> extends AbstractNode<T> implements NodeContainer<C> {

    @JsonProperty("content")
    private List<C> children;

    @Override
    public List<C> children() {
        return children;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T children(List<C> children) {
        this.children = children;
        return (T) this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @JsonIgnore
    public T add(C child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
        return (T) this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @JsonIgnore
    public T add(C... items) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.addAll(Arrays.asList(items));
        return (T) this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @JsonIgnore
    public T add(Collection<C> items) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.addAll(items);
        return (T) this;
    }

    @Override
    public String text() {
        final StringBuilder sb = new StringBuilder();
        if (children != null) {
            children.forEach(c -> sb.append(c.text()));
        }
        return sb.toString();
    }

}
