package com.atlassian.adf.block;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.BlockNodeContainer;
import com.atlassian.adf.base.AbstractBlockNodeContainer;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/panel
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("panel")
@ParametersAreNonnullByDefault
public class Panel extends AbstractBlockNodeContainer<Panel> implements BlockNode, BlockNodeContainer<Panel> {

    private Panel() {
    }

    public Panel type(PanelType type) {
        return attribute("panelType", type.name());
    }

    public PanelType type() {
        return strAttribute("panelType")
                .map(String::toLowerCase)
                .map(PanelType::valueOf)
                .orElse(PanelType.info);
    }

    public static Panel of(PanelType type, BlockNode... elements) {
        return new Panel().type(type).add(elements);
    }

    public enum PanelType {
        info, note, tip, warning
    }

}
