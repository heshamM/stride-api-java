package com.atlassian.adf.inline;

import com.atlassian.adf.InlineNode;
import com.atlassian.adf.base.AbstractNode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/emoji
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("emoji")
public class Emoji extends AbstractNode<Emoji> implements InlineNode {

    @JsonIgnore
    public String id() {
        return strAttribute("id").orElse("");
    }

    @JsonIgnore
    public Emoji id(String id) {
        return attribute("id", id);
    }

    @JsonIgnore
    public String shortName() {
        return strAttribute("shortName").orElse("");
    }

    @JsonIgnore
    public Emoji shortName(String shortName) {
        return attribute("shortName", shortName);
    }

    @JsonIgnore
    public String text() {
        return strAttribute("text").orElse("");
    }

    @JsonIgnore
    public Emoji text(String text) {
        return attribute("text", text);
    }

    private Emoji() {

    }

    public static Emoji emoji(String shortName) {
        return new Emoji().shortName(shortName);
    }

}
