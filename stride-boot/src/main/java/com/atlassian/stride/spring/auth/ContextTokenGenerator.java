package com.atlassian.stride.spring.auth;

import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.TokenGenerator;
import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.auth.model.RefreshTokenResponse;
import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.exception.ContextConfigNotAvailableException;
import com.atlassian.stride.spring.config.suppliers.RequestBoundedContextConfigSupplier;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * A component that wraps token generation
 */
@Component
@Slf4j
@AllArgsConstructor
public class ContextTokenGenerator {

    private final RequestBoundedContextConfigSupplier requestBoundedContextConfigSupplier;
    private final TokenGenerator tokenGenerator;
    private final CachedTokenGenerator cachedTokenGenerator;

    public RefreshTokenResponse generate() throws FailedToObtainTokenException, ContextConfigNotAvailableException {
        return tokenGenerator.generate(requestBoundedContextConfigSupplier.get());
    }

    public RefreshTokenResponse generate(ContextConfig config) throws FailedToObtainTokenException {
        return tokenGenerator.generate(config);
    }

    public RefreshTokenResponse cached() throws FailedToObtainTokenException, ContextConfigNotAvailableException {
        return cachedTokenGenerator.generate(requestBoundedContextConfigSupplier.get());
    }

    public RefreshTokenResponse cached(ContextConfig config) throws FailedToObtainTokenException, ContextConfigNotAvailableException {
        return cachedTokenGenerator.generate(config);
    }

}
