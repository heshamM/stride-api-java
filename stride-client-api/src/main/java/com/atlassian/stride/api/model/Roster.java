package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Response containing all user Ids in a conversation
 */
@Data
public class Roster extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;

    @JsonProperty("values")
    private List<String> users = new ArrayList<>();

}

