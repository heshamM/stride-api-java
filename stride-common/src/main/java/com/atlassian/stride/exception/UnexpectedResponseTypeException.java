package com.atlassian.stride.exception;

/**
 * Thrown when the response of the remote endpoint does not match what was expected
 */
public class UnexpectedResponseTypeException extends RuntimeException {

    public UnexpectedResponseTypeException(String message) {
        super(message);
    }

    public UnexpectedResponseTypeException(String message, Throwable cause) {
        super(message, cause);
    }

}
