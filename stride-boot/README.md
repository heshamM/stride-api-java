# Stride's Spring Boot integration

This library provides components and configuration for Stride Apps based on Spring Boot.

## Configuration

Import configuration class:

```java
@Import(StrideSpringConfiguration.class)
```

## `application.yaml`

If you want to integrate with ngrok seamlessly:

```yaml
# includes default configuration for Stride (like the API base URL)
spring:
  profiles:
    include:
      - ...  
      - ngrok 
      
```

And this will automatically configure authorization and the main entrypoint for the api (`StrideClient`):

```yaml
# App as created in DAC (App name is not really used directly)
app:
  name: My Amazing Stride App
  instance: 
    clientId: ${MY_STRIDE_APP_CLIENT_ID}
    secret: ${MY_STRIDE_APP_SECRET}
```

## Multi-tenancy and environment selection

If you want to use your service with multiple Apps created in DAC - and possibly in
different environments - you can have your configuration like this:

```yaml
# App Configuration
app:
  name: My Amazing Stride App
  # App as created in DAC
  instances:
    prd:
      clientId: ${MY_STRIDE_APP_CLIENT_ID}
      secret: ${MY_STRIDE_APP_SECRET}
    stg:
      clientId: ${MY_STRIDE_APP_CLIENT_ID_IN_STAGING}
      secret: ${MY_STRIDE_APP_SECRET_IN_STAGING}
      environment: # custom environment
        dacUrl: https://developer.stg.internal.atlassian.com/apps/{0}/install
        updateDescriptorUrl: https://connect.staging.atlassian.io/app/{0}/descriptor/fetch
        installationUrl: https://connect.staging.atlassian.io/addon/descriptor/{0}/latest
        apiUrl: https://api.stg.atlassian.com
```

## Using the API

Inject the Spring component `StrideClient`:

```java
private final StrideClient stride;
```

## Accessing the configuration

The properties for each environment will be mapped to an instance of `AppConfig`. 

You can use the component `AppConfigSupplier` to obtain an instance configuration by 
the `clientId`.  

You can also inject `RequestBoundedAppConfigSupplier` to obtain the configuration associated
with the current request or thread (when used within a decorated async routine).

## Request/thread context

In a Spring Boot Web Applications, each request or thread can be tied to a context using different mechanisms.

Currently, there are two ways to have the context automatically resolved:

### Authentication filter

This library provides an authenticatio filter that will take care of resolving the correct 
configuration for each incoming request, initiated by Stride, hitting your App service 
by matching the `issuer` in the JWT token with the `clientId`. 

### Path variable

If your path have a variable named `{clientId}`, the corresponding configuration will
also be automatically put in the request context. 

Example: 

```java
@PostMapping("{clientId}/{glanceKey}")
@ResponseStatus(HttpStatus.NO_CONTENT)
void updateGlance(@RequestBody UpdateGlanceRequest updateGlanceRequest,
                  @PathVariable String glanceKey) throws Exception {
    ...
}
```

This is useful when your App service has to accept context-bounded calls from other sources
than Stride or your App frontend within Stride.

## Troubleshooting

Notice the multi-tenanted style of configurations (which allows you to use one service 
instance with multiple environments at the same time) indexes each App instance by
its `clientId`, so take care when copy&paste'ing. 

Make sure the `clientId` is unique in your configuration file and that the `environment`
(`staging` or `production`) matches the one you created your app 
(`developer.atlassian.com` is production, so in most cases you can just not use the 
`environment` attribute).

## Authorizing incoming requests

Requests from Stride to your APP will are signed with a JWT token that can be decoded and 
verified with your **oAuth Client Id** and **Secret** obtained from Stride.

In most of cases, the token is sent in the `Authentication` HTTP header. However, Stride client 
sometimes calls your App service directly, for instance to fetch a page to be displayed 
within a dialog or sidebar or also to fetch the glance status or configuration state. In these
specific cases the token can be found as a query parameter named `jwt`.

### `Authentication` header

This library parses the `Authentication` HTTP header automatically for your controllers 
using the annotation `@AuthorizeJwtHeader` on your method or class:

```java
@AuthorizeJwtHeader
@PostMapping("/uninstalled")
@ResponseStatus(HttpStatus.NO_CONTENT)
void uninstalled(@RequestBody AppInstallation appUninstallation) throws Exception {
    log.info("Got uninstalled WebHook with key {}", appUninstallation.getKey());
}
```

### Query parameter token

Some `GET` requests don't have the `Authentication` header but a `jwt` query parameter
containing the token.

You can decode the token automatically in your controller using the annotation `@AuthorizeJwtParameter`:

```java
@AuthorizeJwtParameter
@CrossOrigin
@GetMapping(value = "/module/glance/state", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
GlanceValueResponse fetchGlanceLabelValue() {
    log.debug("Got request for the glance label");
    return GlanceValueResponse.of("Click me!");
}
```

## Accessing important Token information

As described in the [official documentation](https://developer.atlassian.com/cloud/stride/security/jwt/),
there are useful information within the token, like the current context and user, that you may need to process requests.

**Context and user information within the token are the only secure way to read these values when Stride calls your
service**, since you can verify the content using your secret. If your frontend and backend sends custom payloads 
containing context information, these can be forged by one user in behalf of other user or from one conversation
to another.

**Be aware the token information will be available only if you have one of the annotations mentioned above
authorizing your method.**


### Current context

For instance, in order to know which conversation originated a request, you can just add a parameter with type
`ConversationContext` to your controller method. The argument will be resolved automatically:

```java
@AuthorizeJwtParameter
@GetMapping(value = "/module/config/state", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
@ResponseBody
ConfigurationValueResponse configurationState(ConversationContext context) {
    log.info("Got configuration-state callback from context {}", context);
    return ConfigurationValueResponse.of(false);
}
```

The same works for other types of contexts, namely:

- `ConversationContext`
- `UserContext`
- `UserInConversationContext`
- `SiteContext`

# The token itself

This library wraps the decoded token in an object of type `StrideJwtToken`. In the same way as the previous topic,
you can just add a parameter to your controller method of this type and it'll be automatically resolved.

An useful information you can obtain from `StrideJwtToken` is the oAuth Client Id. It's very important, for instance,
if you want your backend service to work in a multi-tenanted manner, distinguishing between different Apps
you created in DAC.