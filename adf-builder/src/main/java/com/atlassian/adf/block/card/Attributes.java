package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.val;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@ParametersAreNonnullByDefault
public class Attributes extends UnknownPropertiesSupport {

    @Nonnull
    private Title title;
    @Nonnull
    private String text;
    private String textUrl;
    private Link background;
    private Boolean collapsible;
    private List<Action> actions;
    private Context context;
    private SimpleText description;
    private List<Detail> details;
    private Link link;
    private Link preview;

    @JsonCreator
    private Attributes(
            @JsonProperty("title") Title title,
            @JsonProperty("text") String text) {
        this.title = title;
        this.text = text;
    }

    public static Attributes of(Title title, String text) {
        return new Attributes(title, text);
    }

    @JsonIgnore
    public Attributes background(String url) {
        this.background = Link.link(url);
        return this;
    }

    @JsonIgnore
    public Attributes link(String url) {
        this.link = Link.link(url);
        return this;
    }

    @JsonIgnore
    public Attributes preview(String url) {
        this.preview = Link.link(url);
        return this;
    }

    @JsonIgnore
    public Attributes description(String description) {
        this.description = SimpleText.of(description);
        return this;
    }

    @JsonIgnore
    public Attributes detail(Detail detail) {
        if (details == null) {
            details = new ArrayList<>();
        }
        this.details.add(detail);
        return this;
    }

    @JsonIgnore
    public Attributes detail(Consumer<Detail> c) {
        val d = Detail.detail();
        c.accept(d);
        return detail(d);
    }

    @JsonIgnore
    public Attributes title(Consumer<Title> c) {
        c.accept(title);
        return this;
    }

    @JsonIgnore
    public Attributes action(Action action) {
        if (actions == null) {
            actions = new ArrayList<>();
        }
        this.actions.add(action);
        return this;
    }

    @JsonIgnore
    public Attributes action(String title, String app, String key) {
        return action(Action.action(title, app, key));
    }

    @JsonIgnore
    public Attributes context(Context context) {
        this.context = context;
        return this;
    }

    @JsonIgnore
    public Attributes context(String text) {
        return context(Context.context(text));
    }

    @JsonIgnore
    public Attributes context(String text, String iconUrl, String iconLabel) {
        return context(Context.context(text, iconUrl, iconLabel));
    }

}
