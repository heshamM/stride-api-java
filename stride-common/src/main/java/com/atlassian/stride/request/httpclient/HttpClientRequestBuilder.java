package com.atlassian.stride.request.httpclient;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.request.Request;
import com.atlassian.stride.request.RequestBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link RequestBuilder} based on Apache HTTP Client
 */
public class HttpClientRequestBuilder implements RequestBuilder {

    private final CloseableHttpClient client;
    private final ObjectMapper mapper;
    private final Map<String, String> headers;
    private final List<Integer> expectedResponseStatuses;
    private final List<ContentType> accept;
    private ContextConfig contextConfig;
    private Scope scope;
    private ContentType contentType;
    private Object body;

    public HttpClientRequestBuilder(CloseableHttpClient client, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
        headers = new HashMap<>();
        expectedResponseStatuses = new ArrayList<>();
        accept = new ArrayList<>();
    }

    @Override
    public RequestBuilder expect(Integer... statuses) {
        Collections.addAll(this.expectedResponseStatuses, statuses);
        return this;
    }

    @Override
    public RequestBuilder header(String name, String value) {
        this.headers.put(name, value);
        return this;
    }

    @Override
    public RequestBuilder contextConfig(ContextConfig contextConfig) {
        this.contextConfig = contextConfig;
        return this;
    }

    @Override
    public RequestBuilder scope(Scope scope) {
        this.scope = scope;
        return this;
    }

    @Override
    public RequestBuilder acceptJson() {
        this.accept.add(ContentType.APPLICATION_JSON);
        return this;
    }

    @Override
    public RequestBuilder acceptPlainText() {
        this.accept.add(ContentType.TEXT_PLAIN);
        return this;
    }

    @Override
    public RequestBuilder acceptHtml() {
        this.accept.add(ContentType.TEXT_HTML);
        return this;
    }

    @Override
    public RequestBuilder acceptBytes() {
        this.accept.add(ContentType.APPLICATION_OCTET_STREAM);
        return this;
    }

    @Override
    public RequestBuilder body(Object body) {
        this.body = body;
        return this;
    }

    @Override
    public RequestBuilder contentType(ContentType contentType) {
        this.contentType = contentType;
        return this;
    }

    @Override
    public <T> Request<T> build(Class<T> type) {
        return new HttpClientRequest<>(client, mapper, type, headers, expectedResponseStatuses, accept, contentType, body, contextConfig, scope);
    }

}
