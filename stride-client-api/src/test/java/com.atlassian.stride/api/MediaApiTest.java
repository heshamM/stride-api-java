package com.atlassian.stride.api;

import com.atlassian.stride.api.model.MediaUploadResponse;
import com.atlassian.stride.model.context.Context;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@Slf4j
public class MediaApiTest {

    private StrideClient stride = Helper.client();

    @Test
    public void uploadAndFetch() throws Exception {
        String name = "my media " + System.currentTimeMillis();
        byte[] original = load("/logo.png");
        MediaUploadResponse m = stride.media().upload(original)
                .name(name)
                .to(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .get();
        log.info("Media uploaded: {}", m);
        assertThat(m.getData().getId(), notNullValue());
        assertThat(m.getData().getName(), is(name));

        byte[] result = stride.media()
                .get(m.getData().getId())
                .from(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .join();
        assertThat(result, is(original));
    }

    @Test
    public void fetch() throws Exception {
        String existentMedia = "9df6c848-5a88-4593-82d8-fdcc51efa0d2";
        byte[] content = load("/logo.png");
        byte[] receivedContent = stride.media().get(existentMedia)
                .from(Context.conversation(Helper.cloudId(), Helper.conversationId()))
                .get();
        log.info("Media content received with {} bytes", receivedContent.length);
        assertThat(receivedContent, is(content));
    }

    private byte[] load(String resource) throws Exception {
        final URI uri = MediaApiTest.class.getResource(resource).toURI();
        return Files.readAllBytes(Paths.get(uri));
    }

}