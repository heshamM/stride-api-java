package com.atlassian.stride.exception;

/**
 * Thrown when a request to a remote endpoint fails for an unknown reason
 */
public class RequestErrorException extends RuntimeException {

    public RequestErrorException(Throwable cause) {
        super(cause);
    }

}
