package com.atlassian.adf;

import com.atlassian.adf.block.BlockQuote;
import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.Panel;
import com.atlassian.adf.block.Paragraph;
import com.atlassian.adf.block.Rule;
import com.atlassian.adf.block.UnknownBlockNode;
import com.atlassian.adf.block.card.ApplicationCard;
import com.atlassian.adf.block.codeblock.CodeBlock;
import com.atlassian.adf.block.decision.DecisionList;
import com.atlassian.adf.block.list.BulletList;
import com.atlassian.adf.block.list.OrderedList;
import com.atlassian.adf.block.media.MediaGroup;
import com.atlassian.adf.block.task.TaskList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Interface representing block nodes as in: https://developer.atlassian.com/cloud/stride/apis/document/structure
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = UnknownBlockNode.class)
@JsonSubTypes({
    @JsonSubTypes.Type(value = DecisionList.class, name = "decisionList"),
    @JsonSubTypes.Type(value = TaskList.class, name = "taskList"),
    @JsonSubTypes.Type(value = BlockQuote.class, name = "blockquote"),
    @JsonSubTypes.Type(value = Paragraph.class, name = "paragraph"),
    @JsonSubTypes.Type(value = Rule.class, name = "rule"),
    @JsonSubTypes.Type(value = BulletList.class, name = "bulletList"),
    @JsonSubTypes.Type(value = OrderedList.class, name = "orderedList"),
    @JsonSubTypes.Type(value = CodeBlock.class, name = "codeBlock"),
    @JsonSubTypes.Type(value = Heading.class, name = "heading"),
    @JsonSubTypes.Type(value = MediaGroup.class, name = "mediaGroup"),
    @JsonSubTypes.Type(value = Panel.class, name = "panel"),
    @JsonSubTypes.Type(value = ApplicationCard.class, name = "applicationCard")
})
public interface BlockNode extends Node {

}
