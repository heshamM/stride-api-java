package com.atlassian.stride.spring.web;

import com.atlassian.stride.spring.config.model.AppConfig;
import com.atlassian.stride.spring.config.suppliers.RequestBoundedAppConfigSupplier;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Resolves parameters with type {@link AppConfig} in your controller methods
 */
@AllArgsConstructor
@Slf4j
public class AppConfigArgumentResolver implements HandlerMethodArgumentResolver {

    private final RequestBoundedAppConfigSupplier requestBoundedAppConfigSupplier;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(AppConfig.class);
    }
 
    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        log.debug("Resolving app configuration argument for method {}", parameter.getMethod().toGenericString());
        return requestBoundedAppConfigSupplier.get();
    }

}