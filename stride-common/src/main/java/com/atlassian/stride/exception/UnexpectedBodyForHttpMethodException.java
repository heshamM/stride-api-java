package com.atlassian.stride.exception;

/**
 * Thrown when trying to make a request with a body using an HTTP method that does not support a body
 */
public class UnexpectedBodyForHttpMethodException extends RuntimeException {

    public UnexpectedBodyForHttpMethodException(String message) {
        super(message);
    }

    public UnexpectedBodyForHttpMethodException(String message, Throwable cause) {
        super(message, cause);
    }

}
