package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import lombok.Data;

/**
 * Returned data when uploading a file.
 */
@Data
public class MediaUploadResponse extends UnknownPropertiesSupport {

    private MediaUploadResponseData data;

    @Data
    public static class MediaUploadResponseData extends UnknownPropertiesSupport {

        private Object artifacts;
        private String hash;
        private String id;
        private String mediaType;
        private String mimeType;
        private String name;
        private String processingStatus;
        private Long size;
    }

}

