package com.atlassian.adf.block.codeblock;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.atlassian.adf.inline.Text;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Optional;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/codeBlock
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonTypeName("codeBlock")
public class CodeBlock extends AbstractNodeContainer<CodeBlock, Text> implements BlockNode {

    private CodeBlock() {
    }

    @Override
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = Text.class)
    public List<Text> children() {
        return super.children();
    }

    @JsonIgnore
    public CodeBlock add(String... lines) {
        for (String line : lines) {
            add(Text.of(line));
        }
        return this;
    }

    @JsonIgnore
    public Optional<Language> language() {
        return strAttribute("language").map(Language::from);
    }

    @JsonIgnore
    public CodeBlock language(Language language) {
        attribute("language", language.name());
        return this;
    }

    public static CodeBlock codeBlock(Text... elements) {
        return codeBlock().add(elements);
    }

    public static CodeBlock codeBlock(String... lines) {
        return codeBlock().add(lines);
    }

    public static CodeBlock codeBlock() {
        return new CodeBlock();
    }

}
