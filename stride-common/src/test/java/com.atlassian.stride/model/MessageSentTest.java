package com.atlassian.stride.model;

import com.atlassian.stride.model.webhooks.MessageSent;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MessageSentTest {

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testMessageSent() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/message-sent-payload.json"), MessageSent.class);
        assertThat(a.getCloudId(), is("f7ebe2c0-0309-4687-b913-41d422f2110b"));
        assertThat(a.getConversation().getName(), is("test 6"));
        assertThat(a.getMessage().getId(), is("a8f6c9e1-9459-11e7-b699-02420aff0003"));
        System.out.println(a.toString());
    }

    @Test
    public void testDirectMessage() throws Exception {
        val a = mapper.readValue(getClass().getResourceAsStream("/payload-example/direct-message-payload.json"), MessageSent.class);
        assertThat(a.getCloudId(), is("f7ebe2c0-0309-4687-b913-41d422f2110b"));
        System.out.println(a.toString());
    }

}
