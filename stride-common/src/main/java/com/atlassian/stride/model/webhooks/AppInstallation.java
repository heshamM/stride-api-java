package com.atlassian.stride.model.webhooks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Maps the App's lifecycle event payload received from WebHooks when the App is installed or uninstalled in a context.
 * See: https://developer.atlassian.com/cloud/stride/blocks/app-lifecycle/
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppInstallation extends UnknownPropertiesSupport {

    public static final String EVENT_TYPE_INSTALLED = "installed";
    public static final String EVENT_TYPE_UNINSTALLED = "uninstalled";

    public enum EventType {
        installed,
        uninstalled
    }

    private String key;
    private String productType;
    private String cloudId;
    private String resourceType;
    private String resourceId;
    private EventType eventType;
    private String userId;
    private String oauthClientId;
    private String version;

    public boolean isInstallation() {
        return EventType.installed.equals(eventType);
    }

    public boolean isUninstallation() {
        return EventType.uninstalled.equals(eventType);
    }

}
