package com.atlassian.adf.base;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.InlineNode;
import com.atlassian.adf.Node;
import com.atlassian.adf.block.BlockQuote;
import com.atlassian.adf.block.Heading;
import com.atlassian.adf.block.Panel;
import com.atlassian.adf.block.Paragraph;
import com.atlassian.adf.block.Rule;
import com.atlassian.adf.block.card.ApplicationCard;
import com.atlassian.adf.block.codeblock.CodeBlock;
import com.atlassian.adf.block.codeblock.Language;
import com.atlassian.adf.block.decision.DecisionItem;
import com.atlassian.adf.block.decision.DecisionList;
import com.atlassian.adf.block.list.BulletList;
import com.atlassian.adf.block.list.ListItem;
import com.atlassian.adf.block.list.OrderedList;
import com.atlassian.adf.block.media.Media;
import com.atlassian.adf.block.media.MediaGroup;
import com.atlassian.adf.block.task.TaskItem;
import com.atlassian.adf.block.task.TaskList;
import com.atlassian.adf.inline.Text;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Base for a node containing one or more {@link BlockNode}s.
 * <p>
 * Contain helper methods for creation of the various flavors of {@link BlockNode}s.
 *
 * @param <T> Concrete implementing class, used to allow fluent setters.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ParametersAreNonnullByDefault
public abstract class AbstractBlockNodeContainer<T extends AbstractBlockNodeContainer> extends AbstractNodeContainer<T, BlockNode> {

    public T decisionList(DecisionList node) {
        return add(node);
    }

    public T decisionList(String localId, DecisionItem... nodes) {
        return add(DecisionList.decisionList(localId));
    }

    public T decisionList(String localId, Consumer<DecisionList> consumer) {
        return configureAddAndReturnThis(DecisionList.decisionList(localId), consumer);
    }

    public T taskList(TaskList node) {
        return add(node);
    }

    public T taskList(String localId, TaskItem... nodes) {
        return add(TaskList.taskList(localId, nodes));
    }

    public T taskList(String localId, Consumer<TaskList> consumer) {
        return configureAddAndReturnThis(TaskList.taskList(localId), consumer);
    }

    public T quote(BlockQuote node) {
        return add(node);
    }

    public T quote(BlockNode... nodes) {
        return add(BlockQuote.blockQuote(nodes));
    }

    public T quote(Consumer<BlockQuote> consumer) {
        return configureAddAndReturnThis(BlockQuote.blockQuote(), consumer);
    }

    public T paragraph(Paragraph node) {
        return add(node);
    }

    public T paragraph(String text) {
        return add(Paragraph.paragraph(Text.of(text)));
    }

    public T paragraph(InlineNode... nodes) {
        return add(Paragraph.paragraph(nodes));
    }

    public T paragraph(Consumer<Paragraph> consumer) {
        return configureAddAndReturnThis(Paragraph.paragraph(), consumer);
    }

    public T rule() {
        return add(Rule.rule());
    }

    public T bulletList(BulletList node) {
        return add(node);
    }

    public T bulletList(ListItem... nodes) {
        return add(BulletList.bulletList(nodes));
    }

    public T bulletList(Consumer<BulletList> consumer) {
        return configureAddAndReturnThis(BulletList.bulletList(), consumer);
    }

    public T orderedList(OrderedList node) {
        return add(node);
    }

    public T orderedList(ListItem... nodes) {
        return add(OrderedList.orderedList(nodes));
    }

    public T orderedList(Consumer<OrderedList> consumer) {
        return configureAddAndReturnThis(OrderedList.orderedList(), consumer);
    }

    public T code(CodeBlock node) {
        return add(node);
    }

    public T code(Text... nodes) {
        return add(CodeBlock.codeBlock(nodes));
    }

    public T code(String... lines) {
        return add(CodeBlock.codeBlock(lines));
    }

    public T code(Language language, Text... nodes) {
        return add(CodeBlock.codeBlock(nodes).language(language));
    }

    public T code(Language language, String... lines) {
        return add(CodeBlock.codeBlock(lines).language(language));
    }

    public T code(Consumer<CodeBlock> consumer) {
        return configureAddAndReturnThis(CodeBlock.codeBlock(), consumer);
    }

    public T head(Heading node) {
        return add(node);
    }

    public T head(Heading.Level level, Text... nodes) {
        return add(Heading.heading(level, nodes));
    }

    public T head(Heading.Level level, String... lines) {
        return add(Heading.heading(level, lines));
    }

    public T head(Heading.Level level, Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(level), consumer);
    }

    public T h1(Text... nodes) {
        return add(Heading.heading(Heading.Level.H1, nodes));
    }

    public T h1(String... lines) {
        return add(Heading.heading(Heading.Level.H1, lines));
    }

    public T h1(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H1), consumer);
    }

    public T h2(Text... nodes) {
        return add(Heading.heading(Heading.Level.H2, nodes));
    }

    public T h2(String... lines) {
        return add(Heading.heading(Heading.Level.H2, lines));
    }

    public T h2(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H2), consumer);
    }

    public T h3(Text... nodes) {
        return add(Heading.heading(Heading.Level.H3, nodes));
    }

    public T h3(String... lines) {
        return add(Heading.heading(Heading.Level.H3, lines));
    }

    public T h3(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H3), consumer);
    }

    public T h4(Text... nodes) {
        return add(Heading.heading(Heading.Level.H4, nodes));
    }

    public T h4(String... lines) {
        return add(Heading.heading(Heading.Level.H4, lines));
    }

    public T h4(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H4), consumer);
    }

    public T h5(Text... nodes) {
        return add(Heading.heading(Heading.Level.H5, nodes));
    }

    public T h5(String... lines) {
        return add(Heading.heading(Heading.Level.H5, lines));
    }

    public T h5(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H5), consumer);
    }

    public T h6(Text... nodes) {
        return add(Heading.heading(Heading.Level.H6, nodes));
    }

    public T h6(String... lines) {
        return add(Heading.heading(Heading.Level.H6, lines));
    }

    public T h6(Consumer<Heading> consumer) {
        return configureAddAndReturnThis(Heading.heading(Heading.Level.H6), consumer);
    }


    public T mediaGroup(MediaGroup node) {
        return add(node);
    }

    public T mediaGroup(Media... nodes) {
        return add(MediaGroup.mediaGroup(nodes));
    }

    public T mediaGroup(Consumer<MediaGroup> consumer) {
        return configureAddAndReturnThis(MediaGroup.mediaGroup(), consumer);
    }

    public T panel(Panel node) {
        return add(node);
    }

    public T panel(Panel.PanelType type, BlockNode... nodes) {
        return add(Panel.of(type, nodes));
    }

    public T info(BlockNode... nodes) {
        return add(Panel.of(Panel.PanelType.info, nodes));
    }

    public T info(Consumer<Panel> consumer) {
        return configureAddAndReturnThis(Panel.of(Panel.PanelType.info), consumer);
    }

    public T note(BlockNode... nodes) {
        return add(Panel.of(Panel.PanelType.note, nodes));
    }

    public T note(Consumer<Panel> consumer) {
        return configureAddAndReturnThis(Panel.of(Panel.PanelType.note), consumer);
    }

    public T tip(BlockNode... nodes) {
        return add(Panel.of(Panel.PanelType.tip, nodes));
    }

    public T tip(Consumer<Panel> consumer) {
        return configureAddAndReturnThis(Panel.of(Panel.PanelType.tip), consumer);
    }

    public T warning(BlockNode... nodes) {
        return add(Panel.of(Panel.PanelType.warning, nodes));
    }

    public T warning(Consumer<Panel> consumer) {
        return configureAddAndReturnThis(Panel.of(Panel.PanelType.warning), consumer);
    }

    public T card(ApplicationCard node) {
        return add(node);
    }

    public T card(String title) {
        return add(ApplicationCard.card(title));
    }

    public T card(String title, Consumer<ApplicationCard> consumer) {
        return configureAddAndReturnThis(ApplicationCard.card(title), consumer);
    }

    private <X extends BlockNode> T configureAddAndReturnThis(X node, Consumer<X> consumer) {
        consumer.accept(node);
        return add(node);
    }

    @Override
    public String text() {
        final List<BlockNode> children = children();
        if (children != null) {
            return children.stream().map(Node::text).collect(Collectors.joining("\n"));
        }
        return "";
    }

}
