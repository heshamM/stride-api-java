package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import lombok.Data;

import java.util.List;

/**
 * Represents an error response
 */
@Data
public class APIErrorResponse extends UnknownPropertiesSupport {

    private List<Long> errors;
    private String message;
    private Long statusCode;

}

