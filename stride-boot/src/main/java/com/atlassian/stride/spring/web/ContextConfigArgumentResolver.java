package com.atlassian.stride.spring.web;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.spring.config.suppliers.RequestBoundedContextConfigSupplier;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Resolves parameters with type {@link ContextConfig} in your controller methods
 */
@AllArgsConstructor
@Slf4j
public class ContextConfigArgumentResolver implements HandlerMethodArgumentResolver {

    private final RequestBoundedContextConfigSupplier requestBoundedContextConfigSupplier;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(ContextConfig.class);
    }
 
    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        log.debug("Resolving context configuration argument for method {}", parameter.getMethod().toGenericString());
        return requestBoundedContextConfigSupplier.get();
    }

}