package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Action extends UnknownPropertiesSupport {

    @Nonnull
    private String title;
    @Nonnull
    private Target target;

    public static Action action(String title, Target target) {
        return new Action(title, target);
    }

    public static Action action(String title, String app, String key) {
        return new Action(title, Target.target(app, key));
    }

}
