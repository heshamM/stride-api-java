package com.atlassian.stride.model.context;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Nonnull;

/**
 * Defines the context for a site.
 *
 * @see Context
 */
@JsonDeserialize(as = ContextImpl.class)
public interface SiteContext {

    @Nonnull
    String getCloudId();

}
