package com.atlassian.stride.config;

import javax.annotation.ParametersAreNonnullByDefault;
import java.text.MessageFormat;

/**
 * Hold the environment information for the Stride API
 */
@ParametersAreNonnullByDefault
public interface EnvironmentConfig {

    /**
     * Default production configuration
     */
    EnvironmentConfig production = DefaultEnvironmentConfig.builder()
            .dacUrl("https://developer.atlassian.com/apps/{0}/install")
            .updateDescriptorUrl("https://connect.atlassian.io/app/{0}/descriptor/fetch")
            .installationUrl("https://connect.atlassian.io/addon/descriptor/{0}/latest")
            .apiUrl("https://api.atlassian.com")
            .build();

    /**
     * Internal staging configuration
     */
    EnvironmentConfig staging = DefaultEnvironmentConfig.builder()
            .dacUrl("https://developer.stg.internal.atlassian.com/apps/{0}/install")
            .updateDescriptorUrl("https://connect.staging.atlassian.io/app/{0}/descriptor/fetch")
            .installationUrl("https://connect.staging.atlassian.io/addon/descriptor/{0}/latest")
            .apiUrl("https://api.stg.atlassian.com")
            .build();

    String dacUrl();
    String updateDescriptorUrl();
    String installationUrl();
    String apiUrl();

    default String dacUrl(final String dacAppId) {
        if (dacUrl() == null) return "";
        return MessageFormat.format(dacUrl(), dacAppId);
    }

    default String updateDescriptor(final String dacAppId) {
        if (updateDescriptorUrl() == null) return "";
        return MessageFormat.format(updateDescriptorUrl(), dacAppId);
    }

    default String installationUrl(final String clientId) {
        if (installationUrl() == null) return "";
        return MessageFormat.format(installationUrl(), clientId);
    }

}
