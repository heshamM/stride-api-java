package com.atlassian.adf.block.decision;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.val;

import java.util.List;
import java.util.function.Consumer;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/decisionList
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("decisionList")
public class DecisionList extends AbstractNodeContainer<DecisionList, DecisionItem> implements BlockNode {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = DecisionItem.class)
    public List<DecisionItem> children() {
        return super.children();
    }

    @JsonIgnore
    public String localId() {
        return strAttribute("localId").orElse("");
    }

    @JsonIgnore
    public DecisionList localId(String localId) {
        return attribute("localId", localId);
    }

    private DecisionList() {
    }

    public DecisionList addDecided(String localId, Consumer<DecisionItem> c) {
        val d = DecisionItem.decided(localId);
        c.accept(d);
        return add(d);
    }

    public DecisionList addUndecided(String localId, Consumer<DecisionItem> c) {
        val d = DecisionItem.undecided(localId);
        c.accept(d);
        return add(d);
    }

    public static DecisionList decisionList(String localId, DecisionItem... elements) {
        return new DecisionList().add(elements).localId(localId);
    }

}
