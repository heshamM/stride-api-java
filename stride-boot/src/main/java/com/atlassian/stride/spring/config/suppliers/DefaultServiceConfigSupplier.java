package com.atlassian.stride.spring.config.suppliers;

import com.atlassian.stride.spring.config.model.ServiceConfig;
import com.atlassian.stride.util.NgrokClient;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

/**
 * Fetch service configuration from local environment.
 * <p>
 * If you enable {@code ngrok} profile this is try to fetch your service external/public URL from your running ngrok process.
 * <p>
 * Otherwise, set an environment variable named {@code APP_EXTERNAL_URL} with your external/public URL.
 */
@Slf4j
@RequiredArgsConstructor
public class DefaultServiceConfigSupplier implements ServiceConfigSupplier {

    @Nonnull
    private final Environment env;
    private ServiceConfig serviceConfig;

    @PostConstruct
    public void init() {
        //try general configuration
        val externalUrl = env.getProperty("APP_EXTERNAL_URL");
        if (externalUrl != null && !externalUrl.isEmpty()) {
            this.serviceConfig = ServiceConfig.of(externalUrl);
            log.info("Service starting at host {}", this.serviceConfig.getExternalUrl());
            return;
        }

        // try local ngrok if profile is enabled
        if (env.acceptsProfiles("ngrok")) {
            Integer serverPort = env.getProperty("local.server.port", Integer.class, 8080);
            val ngrokPublicUrl = NgrokClient.fetchPublicUrlForPort(serverPort);
            this.serviceConfig = ServiceConfig.of(ngrokPublicUrl.orElseThrow(() -> new RuntimeException(
                    "'public_url' not found, check ngrok configuration or run ./scripts/start-ngrok.sh")));
            log.info("Service starting in local environment at host {}", this.serviceConfig.getExternalUrl());
            return;
        }

        throw new RuntimeException("Cannot determine external URL");
    }

    @Override
    public ServiceConfig get() {
        return serviceConfig;
    }
}
