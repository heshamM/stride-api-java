package com.atlassian.stride.spring.web;

import com.atlassian.stride.auth.TokenVerifierHelper;
import com.atlassian.stride.auth.exception.InvalidTokenException;
import com.atlassian.stride.auth.model.StrideJwtToken;
import com.atlassian.stride.context.ThreadBoundedContextConfigSupplier;
import com.atlassian.stride.spring.auth.AuthorizeJwtHeader;
import com.atlassian.stride.spring.auth.AuthorizeJwtParameter;
import com.atlassian.stride.spring.config.model.AppConfig;
import com.atlassian.stride.spring.config.suppliers.AppConfigSupplier;
import com.atlassian.stride.spring.exception.UnauthorizedResponseException;
import com.atlassian.stride.spring.util.SpringHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Request interceptor for Spring controller methods annotated with {@link AuthorizeJwtHeader}. It checks the HTTP
 * header {@code Authorization} with the bearer token and verify it.
 * <p>
 * If the verification fails, it interrupts the current request and returns {@link org.springframework.http.HttpStatus#UNAUTHORIZED}.
 * <p>
 * If the verification is successful, it sets the resolved {@link com.atlassian.stride.config.ContextConfig} in the
 * current context as a {@link ThreadLocal} variable.
 */
@Slf4j
@AllArgsConstructor
public class TokenVerifierInterceptor extends HandlerInterceptorAdapter {

    private final AppConfigSupplier appConfigSupplier;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        val handlerMethod = (HandlerMethod) handler;

        val annotationHeader = SpringHelper.getMethodOrClassAnnotation(handlerMethod, AuthorizeJwtHeader.class);
        if (annotationHeader != null) {
            log.debug("Securing method {} by checking Authorization header", handlerMethod.toString());
            String authorizationHeader = request.getHeader("authorization");
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                log.debug("Authorization header: {}", authorizationHeader);
                verifyTokenAndSetupRequestContext(authorizationHeader.substring(7), request);
                return true;
            }
            throw new UnauthorizedResponseException();
        }

        val annotationParameter = SpringHelper.getMethodOrClassAnnotation(handlerMethod, AuthorizeJwtParameter.class);
        if (annotationParameter != null) {
            log.debug("Securing method {} by checking jwt query parameter", handlerMethod.toString());
            val authorizationParameter = request.getParameter("jwt");
            if (authorizationParameter != null) {
                log.debug("Authorization parameter: {}", authorizationParameter);
                verifyTokenAndSetupRequestContext(authorizationParameter, request);
                return true;
            }
            throw new UnauthorizedResponseException();
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        ThreadBoundedContextConfigSupplier.removeConfigFromThread();
    }

    private void verifyTokenAndSetupRequestContext(String authorizationToken, HttpServletRequest request) {
        StrideJwtToken token;
        try {
            token = TokenVerifierHelper.verifyAuthorizationToken(authorizationToken, iss -> appConfigSupplier.byClientId(iss).map(AppConfig::getContextConfig));
        } catch (InvalidTokenException e) {
            log.error("Token verification failed", e);
            throw new UnauthorizedResponseException();
        }
        StrideJwtTokenArgumentResolver.setInRequest(request, token);
        val requestContextConfig = appConfigSupplier.byClientId(token.getIss()).orElseThrow(UnauthorizedResponseException::new);
        ThreadBoundedContextConfigSupplier.attachConfigToThread(requestContextConfig.getContextConfig());
    }

}