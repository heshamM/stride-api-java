package com.atlassian.stride.context;

import com.atlassian.stride.config.ContextConfig;
import lombok.val;
import org.slf4j.MDC;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.Executor;

/**
 * Helper class that wraps {@link Executor}s and decorate {@link Runnable}s passing the current context to them
 */
@ParametersAreNonnullByDefault
public class ContextPassingRunner {

    private static ThreadLocal<ConfigContextExecutor> localExecutor = new ThreadLocal<>();

    public static ConfigContextExecutor wrap(Executor delegate, ContextConfig config) {
        if (localExecutor.get() != null) {
            return localExecutor.get();
        }
        return new ConfigContextExecutor(delegate, config);
    }

    public static Runnable withContext(Runnable runnable, @Nullable ContextConfig config) {
        return withContext(runnable, config, localExecutor.get());
    }

    public static Runnable withContext(Runnable runnable, @Nullable ContextConfig config, @Nullable ConfigContextExecutor executor) {
        val contextMap = MDC.getCopyOfContextMap();
        return () -> {
            try {
                localExecutor.set(executor);
                if (contextMap != null) {
                    MDC.setContextMap(contextMap);
                }
                if (config != null) {
                    ThreadBoundedContextConfigSupplier.attachConfigToThread(config);
                } else {
                    ThreadBoundedContextConfigSupplier.removeConfigFromThread();
                }
                runnable.run();
            } finally {
                localExecutor.set(null);
                ThreadBoundedContextConfigSupplier.removeConfigFromThread();
                if (contextMap != null) {
                    MDC.clear();
                }
            }
        };
    }

}
