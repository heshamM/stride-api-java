package com.atlassian.stride.api.request;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.UpdateGlanceRequest;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.Context;
import com.atlassian.stride.model.context.ConversationContext;
import com.atlassian.stride.model.context.SiteContext;
import com.atlassian.stride.model.context.UserContext;
import com.atlassian.stride.model.context.UserInConversationContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * Send glance updates: update the label and displays the blue line at the right of your glance at the right sidebar
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class Glance {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private String key;
    private String label;
    private Context context;
    private Map<String, Object> metadata;

    public Glance context(ConversationContext context) {
        this.context = (Context) context;
        return this;
    }

    public Glance context(UserInConversationContext context) {
        this.context = (Context) context;
        return this;
    }

    public Glance context(SiteContext context) {
        this.context = (Context) context;
        return this;
    }

    public Glance context(UserContext context) {
        this.context = (Context) context;
        return this;
    }

    public Glance context(Context context) {
        this.context = context;
        return this;
    }

    /**
     * Add arbitrary metadata to the request. Notice the {@code value} has to be serializable to JSON, Jackson-compatible.
     */
    public Glance metadata(String key, Object value) {
        if (metadata == null) {
            metadata = new HashMap<>();
        }
        metadata.put(key, value);
        return this;
    }

    /**
     * Send the glance update
     */
    public CompletableFuture<Void> update() {
        Objects.requireNonNull(key, "key");
        Objects.requireNonNull(label, "label");
        Objects.requireNonNull(context, "context");

        return apiRequest.build(b -> b
                .acceptJson()
                .scope(Scope.STRIDE_PARTICIPATE_IN_CONVERSATION)
                .expect(HttpStatus.SC_NO_CONTENT)
                .body(UpdateGlanceRequest.of(context, label).setMetadata(metadata)))
                .post(apiRequest.uri(StrideEndpoint.glance.update(key)));
    }

}