package com.atlassian.stride.api.request.message;

import com.atlassian.adf.Document;
import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.request.ApiRequestHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * Render a message in the Atlassian Document Format (ADF) to text or HTML
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@Setter
@ParametersAreNonnullByDefault
public class RenderMessage {

    @Setter(AccessLevel.NONE)
    @Nonnull
    private ApiRequestHelper apiRequest;
    private Document document;

    /**
     * Render a message to plain text
     */
    public CompletableFuture<String> toText() {
        return render(false);
    }

    /**
     * Render a message to HTML
     */
    public CompletableFuture<String> toHtml() {
        return render(true);
    }

    private CompletableFuture<String> render(boolean isHtml) {
        Objects.requireNonNull(document, "document");

        return apiRequest.build(String.class, b -> {
            b.body(document);
            if (isHtml) {
                b.acceptHtml();
            } else {
                b.acceptPlainText();
            }
        }).post(apiRequest.uri(StrideEndpoint.message.render()));
    }

}