package com.atlassian.stride.context;

import com.atlassian.stride.config.ContextConfig;
import com.atlassian.stride.exception.ContextConfigNotAvailableException;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

/**
 * Associate {@link ContextConfig} to the current context using a {@link ThreadLocal}
 */
@ParametersAreNonnullByDefault
public class ThreadBoundedContextConfigSupplier implements ContextConfigSupplier {

    private static final ThreadLocal<ContextConfig> contextConfigThreadLocal = new ThreadLocal<>();

    public static void attachConfigToThread(final ContextConfig config) {
        contextConfigThreadLocal.set(config);
    }

    public static Optional<ContextConfig> obtainConfigFromThread() {
        return Optional.ofNullable(contextConfigThreadLocal.get());
    }

    public static void removeConfigFromThread() {
        contextConfigThreadLocal.set(null);
    }

    @Override
    public ContextConfig get() {
        return obtainConfigFromThread().orElseThrow(ContextConfigNotAvailableException::new);
    }

}