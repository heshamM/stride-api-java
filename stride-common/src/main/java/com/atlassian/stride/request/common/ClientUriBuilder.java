package com.atlassian.stride.request.common;

import lombok.val;
import org.apache.http.client.utils.URIBuilder;

import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * URI building facilities for the Stride client API
 */
public class ClientUriBuilder {

    private final String apiBaseUrl;
    private final Map<String, String> queryParams;
    private String endpoint;

    private ClientUriBuilder(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
        queryParams = new HashMap<>();
    }

    public static ClientUriBuilder of(String apiBaseUrl) {
        return new ClientUriBuilder(apiBaseUrl);
    }

    /**
     * Path for the specific endpoint
     */
    public ClientUriBuilder endpoint(String endpointPath) {
        this.endpoint = endpointPath;
        return this;
    }

    /**
     * Add a query parameter. Ignores {@code null} values.
     */
    public ClientUriBuilder param(String name, @Nullable String value) {
        if (value != null) {
            this.queryParams.put(name, value);
        }
        return this;
    }

    /**
     * Add a query parameter. Ignores {@code null} values.
     */
    public ClientUriBuilder param(String name, @Nullable Number value) {
        if (value != null) {
            this.queryParams.put(name, value.toString());
        }
        return this;
    }

    /**
     * Add a query parameter. Ignores {@code null} values.
     */
    public ClientUriBuilder param(String name, @Nullable Boolean value) {
        if (value != null) {
            this.queryParams.put(name, value.toString());
        }
        return this;
    }

    /**
     * Add a query parameter. Ignores {@code null} values.
     * Formats {@link Instant} using ISO '2011-12-03T10:15:30+01:00'.
     */
    public ClientUriBuilder param(String name, @Nullable Instant value) {
        if (value != null) {
            this.queryParams.put(name, DateTimeFormatter.ISO_INSTANT.format(value));
        }
        return this;
    }

    public URI build() {
        try {
            val b = new URIBuilder(apiBaseUrl);
            queryParams.forEach(b::addParameter);
            if (endpoint != null) {
                b.setPath(endpoint);
            }
            return b.build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
