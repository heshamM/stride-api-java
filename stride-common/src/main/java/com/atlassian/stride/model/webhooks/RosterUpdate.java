package com.atlassian.stride.model.webhooks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Object mapping the WebHook request to the App when a conversation is created, modified, archived or deleted.
 * See: https://developer.atlassian.com/cloud/stride/apis/modules/chat/webhook/
 * <p>
 * Example:
 * <pre>
 * {@code
 * {
 *   "action": "add",
 *   "cloudId": "f7ebe2c0-0309-4687-b913-41d422f2110b",
 *   "conversation": {
 *     "avatarUrl": "https://static.stride.com/default-room-avatars/web/96pt/default_09-96.png",
 *     "created": "2017-09-06T18:51:02.235Z",
 *     "id": "83a31d5f-ef12-4ff0-9e04-2c30c506f264",
 *     "isArchived": false,
 *     "modified": "2017-09-27T01:32:18.555Z",
 *     "name": "My Favorite Room",
 *     "privacy": "public",
 *     "topic": "This is the new topic!",
 *     "type": "group"
 *   },
 *   "type": "chat_roster_updates",
 *   "user": {
 *     "id": "655362:ffccb137-2a95-4016-8e22-5bff5548eeff"
 *   }
 * }
 * }
 * </pre>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RosterUpdate extends UnknownPropertiesSupport {

    public enum Action {
        /**
         * A user joined a conversation.
         */
        add,
        /**
         * A user left a conversation.
         */
        remove
    }

    private Action action;
    private String cloudId;
    private Conversation conversation;
    private String type;
    private UserId user;

    public boolean wasAdded() {
        return Action.add.equals(action);
    }

    public boolean wasRemoved() {
        return Action.remove.equals(action);
    }

}
