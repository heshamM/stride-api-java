package com.atlassian.adf.block.card;

import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Data(staticConstructor = "detail")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Detail extends UnknownPropertiesSupport {

    private String title;
    private Badge badge;
    private Icon icon;
    private Lozenge lozenge;
    private String text;
    private List<User> users;

    @JsonIgnore
    public Detail lozenge(Lozenge node) {
        this.lozenge = node;
        return this;
    }

    @JsonIgnore
    public Detail lozenge(String text) {
        return lozenge(Lozenge.lozenge(text));
    }

    @JsonIgnore
    public Detail lozenge(String text, Consumer<Lozenge> c) {
        val l = Lozenge.lozenge(text);
        c.accept(l);
        return lozenge(l);
    }

    @JsonIgnore
    public Detail user(Icon icon) {
        this.users.add(User.user(icon));
        return this;
    }

    @JsonIgnore
    public Detail user(String url, String label) {
        return user(User.user(Icon.icon(url, label)));
    }

    @JsonIgnore
    public Detail user(String id, Icon icon) {
        return user(User.user(icon).id(id));
    }

    @JsonIgnore
    public Detail user(String id, String url, String label) {
        return user(User.user(Icon.icon(url, label)).id(id));
    }

    @JsonIgnore
    public Detail user(User user) {
        if (users == null) {
            users = new ArrayList<>();
        }
        this.users.add(user);
        return this;
    }

    @JsonIgnore
    public Detail icon(String url, String label) {
        return icon(Icon.icon(url, label));
    }

    @JsonIgnore
    public Detail badge(Badge badge) {
        this.badge = badge;
        return this;
    }

    @JsonIgnore
    public Detail badge(long value) {
        return badge(Badge.badge(value));
    }

    @JsonIgnore
    public Detail badge(long value, Consumer<Badge> c) {
        val b = Badge.badge(value);
        c.accept(b);
        return badge(b);
    }

}
