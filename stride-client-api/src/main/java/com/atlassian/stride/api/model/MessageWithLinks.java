package com.atlassian.stride.api.model;

import com.atlassian.stride.model.webhooks.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Represents a message with links
 */
@Data
public class MessageWithLinks extends UnknownPropertiesSupport {

    @JsonProperty("_links")
    private Links links;
    private Message message;

}

