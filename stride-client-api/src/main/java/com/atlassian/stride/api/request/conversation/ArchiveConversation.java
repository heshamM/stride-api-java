package com.atlassian.stride.api.request.conversation;

import com.atlassian.stride.api.StrideEndpoint;
import com.atlassian.stride.api.model.Conversation;
import com.atlassian.stride.api.request.ApiRequestHelper;
import com.atlassian.stride.model.Scope;
import com.atlassian.stride.model.context.ConversationContext;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CompletableFuture;

/**
 * Archive a conversation
 */
@RequiredArgsConstructor(staticName = "of")
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
public class ArchiveConversation {

    @Nonnull
    private ApiRequestHelper apiRequest;

    /**
     * Archive conversation
     */
    public CompletableFuture<Void> on(ConversationContext context) {
        return apiRequest.build(b -> b
                .acceptJson()
                .scope(Scope.STRIDE_MANAGE_CONVERSATION)
                .expect(HttpStatus.SC_NO_CONTENT))
                .put(apiRequest.uri(StrideEndpoint.conversation.manage.archive(context)));
    }

    /**
     * Archive conversation
     */
    public CompletableFuture<Void> on(Conversation conversation) {
        return on(conversation.asContext());
    }

}