package com.atlassian.stride.request;

import com.atlassian.stride.request.common.DefaultAsyncRequest;
import lombok.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;
import java.net.URI;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Generic request helper to support making requests to the API in a standard way.
 */
@ParametersAreNonnullByDefault
public interface Request<R> {

    /**
     * Post the request and returns a response with the
     * given {@code responseType}.
     */
    @NonNull
    R post(URI uri);


    /**
     * Post the request and returns a response with the
     * given {@code responseType}.
     */
    @NonNull
    R put(URI uri);

    /**
     * Post the request and returns a response with the
     * given {@code responseType}.
     */
    @NonNull
    R patch(URI uri);

    /**
     * Send the request and returns a response with the
     * given {@code responseType}.
     */
    @NonNull
    R get(URI uri);

    /**
     * Send the request and returns a response.
     */
    @NonNull
    R delete(URI uri);

    @NonNull
    default AsyncRequest<R> async(Executor executor) {
        return new DefaultAsyncRequest<>(executor, this);
    }
    
}
