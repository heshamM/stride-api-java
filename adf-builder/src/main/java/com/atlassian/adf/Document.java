package com.atlassian.adf;

import com.atlassian.adf.base.AbstractBlockNodeContainer;
import com.atlassian.adf.markdown.MarkdownSupport;
import com.atlassian.adf.markdown.UnsupportedMarkdownFormatException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.function.BiPredicate;

/**
 * Atlassian Document root element.
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/doc
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ParametersAreNonnullByDefault
public class Document extends AbstractBlockNodeContainer<Document> implements BlockNodeContainer<Document> {

    @JsonProperty
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private int version = 1;
    @JsonProperty
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String type = "doc";

    private Document() {
    }

    public static Document create(BlockNode... elements) {
        return new Document()
                .version(1)
                .add(elements);
    }

    /**
     * Parse a Markdown (CommonMark flavor) text into a {@link Document}. Unsupported Markdown is ignored.
     *
     * @param markdown Markdown text with basic formatting support
     * @return Document matching the markdown style
     *
     * @see MarkdownSupport#documentFromMarkdown(String)
     */
    public static Document fromMarkdown(String markdown) {
        return MarkdownSupport.documentFromMarkdown(markdown);
    }

    /**
     * Parse a Markdown (CommonMark flavor) text into a {@link Document}.
     *
     * @param markdown Markdown text with basic formatting support
     * @param throwErrors When {@code false}, won't throw any exception but will omit unintelligible Markdown
     * @return Document matching the markdown style
     * @throws UnsupportedMarkdownFormatException Whenever markdown format isn't supported, but only if {@code throwErrors} is {@code true}.
     * @see MarkdownSupport#documentFromMarkdown(String, boolean)
     */
    public static Document fromMarkdown(String markdown, boolean throwErrors) {
        return MarkdownSupport.documentFromMarkdown(markdown, throwErrors);
    }

    @Override
    public boolean visitAll(BiPredicate<Node, NodeContainer<? extends Node>> p) {
        return p.test(this, null) && super.visitAll(p);
    }

}
