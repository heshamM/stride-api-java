package com.atlassian.stride.auth.request;

import com.atlassian.stride.auth.CachedTokenGenerator;
import com.atlassian.stride.auth.exception.FailedToObtainTokenException;
import com.atlassian.stride.auth.model.RefreshTokenResponse;
import com.atlassian.stride.request.RequestProvider;
import com.atlassian.stride.request.httpclient.ContextConfigInHttpContext;
import com.atlassian.stride.request.httpclient.ScopeCheckingInHttpContext;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;

/**
 * HTTP request interceptor that adds the {@code Authorization} HTTP header with a signed JWT token.
 * <p>
 * In order to generate the token, it may make another request to the token-generation API, but it'll be cached
 * according to the TTl of the token.
 */
@AllArgsConstructor(staticName = "of")
@Slf4j
public class JwtSigningRequestInterceptor implements HttpRequestInterceptor {

    @NonNull
    private final CachedTokenGenerator cachedTokenGenerator;
    @NonNull
    private final RequestProvider requestProvider;

    @Override
    public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
        val config = ContextConfigInHttpContext.adapt(context);
        if (config.isPresent()) {
            try {
                if (request.getFirstHeader(RefreshTokenResponse.HEADER_NAME) == null) {
                    val token = cachedTokenGenerator.generate(config.get());
                    
                    //check if token contains required scope 
                    val scope = ScopeCheckingInHttpContext.adapt(context);
                    if (scope.isPresent()) {
                        if (!token.parsedScopes().contains(scope.get())) {
                            //scope not present -> cannot use this token to call the API
                            throw new HttpException(String.format("Request requires scope %s but token only has %s", scope.get().getKey(), token.parsedScopes().toString()));
                        }
                    }
                    
                    request.setHeader(RefreshTokenResponse.HEADER_NAME, token.getAuthorizationHeaderValue());
                    log.debug("Request signed with {}", token.getAuthorizationHeaderValue());
                } else {
                    log.warn("Request was already signed! Check your configuration.");
                }
            } catch (FailedToObtainTokenException e) {
                throw new IOException(e);
            }
        }
    }

}
