package com.atlassian.adf.block.card;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.UnknownPropertiesSupport;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/applicationCard
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(fluent = true, chain = true)
@ParametersAreNonnullByDefault
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonTypeName("applicationCard")
public class ApplicationCard extends UnknownPropertiesSupport implements BlockNode {

    @Nonnull
    @JsonProperty("attrs")
    private Attributes attrs;

    @JsonCreator
    private ApplicationCard(@JsonProperty("attrs") Attributes attributes) {
        this.attrs = attributes;
    }

    private ApplicationCard(String title) {
        this.attrs = Attributes.of(Title.title(title), title);
    }

    public static ApplicationCard card(String title) {
        return new ApplicationCard(title);
    }

    @Override
    public String text() {
        final StringBuilder sb = new StringBuilder();
        sb.append(attrs.title().text());
        sb.append("\n");
        sb.append(attrs.text());
        sb.append("\n");
        if (attrs.description() != null) {
            sb.append(attrs.description().text());
        }
        sb.append("\n");
        if (attrs.context() != null) {
            sb.append(attrs.context().text());
        }
        sb.append("\n");
        if (attrs.details() != null) {
            attrs.details().forEach(d -> {
                if (d.title() != null) {
                    sb.append(d.title());
                }
                if (d.text() != null) {
                    sb.append(d.text());
                }
            });
        }
        if (attrs.link() != null) {
            sb.append(attrs.link().url());
        }
        return sb.toString();
    }

}
