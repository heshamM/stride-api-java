package com.atlassian.stride.model;

import lombok.Value;

/**
 * Model to return when you have configured a Glance for you App and Stride calls your service for information.
 * <p>
 * The endpoint must use CORS.
 */
@Value
public class GlanceValueResponse {

    private final Label label;

    private GlanceValueResponse(String label) {
        this.label = new Label(label);
    }

    public static GlanceValueResponse of(String label) {
        return new GlanceValueResponse(label);
    }

    @Value
    public static class Label {
        private final String value;
    }

}