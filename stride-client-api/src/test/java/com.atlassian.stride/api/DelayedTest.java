package com.atlassian.stride.api;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

@Slf4j
public class DelayedTest {

    private StrideClient stride = Helper.client();

    @Test
    public void testWait() throws Exception {
        //fetch the message adding an artificial delay to the future
        List<Integer> trace = new ArrayList<>();
        CompletableFuture<Void> f = stride
                .delayed(Duration.ofSeconds(1))
                .then(() -> trace.add(2));
        
        //this line should be executed before the future is completed
        trace.add(1);
        //wait to the future to complete
        f.join();
        //this line should be executed after the future is completed
        trace.add(3);
        //assert execution order
        assertThat(trace, contains(1, 2, 3));
    }

    @Test
    public void doubleDelay() throws Exception {
        //fetch the message adding an artificial delay to the future
        List<Integer> trace = new ArrayList<>();
        CompletableFuture<Void> f2 = stride
                .delayed(Duration.ofSeconds(1))
                .then(() -> stride.delayed(Duration.ofSeconds(1))
                        .then(() -> trace.add(2)).join());

        //this line should be executed before the future is completed
        trace.add(1);
        //wait to the future to complete
        f2.join();
        //this line should be executed after the future is completed
        trace.add(3);
        //assert execution order
        assertThat(trace, contains(1, 2, 3));
    }

}