package com.atlassian.adf.block.list;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.BlockNodeContainer;
import com.atlassian.adf.base.AbstractBlockNodeContainer;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/listItem
 */
@Data(staticConstructor = "listItem")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("listItem")
public class ListItem extends AbstractBlockNodeContainer<ListItem> implements BlockNodeContainer<ListItem> {

    public static ListItem listItem(BlockNode... elements) {
        return listItem().add(elements);
    }

}
