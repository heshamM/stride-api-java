package com.atlassian.stride.model.context;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.annotation.Nonnull;

/**
 * Defines the context for a conversation.
 *
 * @see Context
 */
@JsonDeserialize(as = ContextImpl.class)
@JsonSerialize(as = ContextImpl.class)
public interface ConversationContext extends SiteContext {

    @Nonnull
    String getConversationId();

}
