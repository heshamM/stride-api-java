package com.atlassian.stride.config;

/**
 * Holds the basic configuration for an Stride client application
 */
public interface ContextConfig {

    String clientId();
    String secret();
    EnvironmentConfig env();

}
