package com.atlassian.adf.base;

import com.atlassian.adf.NodeContainer;
import com.atlassian.adf.inline.Mark;
import com.atlassian.adf.inline.Text;

import java.util.function.Consumer;

/**
 * A mixing of default helper methods for nodes containing text nodes as children.
 *
 * @param <T> Concrete class implementing this interface, used to allow a fluent interface access.
 */
public interface TextNodeContainerMixin<T extends NodeContainer> {
    
    T add(Text... nodes);

    default T text(Text node) {
        return add(node);
    }

    default T text(String text) {
        return add(Text.of(text));
    }

    default T text(String text, Consumer<Text> consumer) {
        final Text t = Text.of(text);
        consumer.accept(t);
        return add(t);
    }

    default T text(String text, Mark... marks) {
        return add(Text.of(text).mark(marks));
    }

    default T em(String text) {
        return add(Text.of(text).em());
    }

    default T strong(String text) {
        return add(Text.of(text).strong());
    }

    default T code(String text) {
        return add(Text.of(text).code());
    }

    default T strike(String text) {
        return add(Text.of(text).strike());
    }

    default T sub(String text) {
        return add(Text.of(text).sub());
    }

    default T sup(String text) {
        return add(Text.of(text).sup());
    }

    default T underline(String text) {
        return add(Text.of(text).underline());
    }

    default T link(String text, String href) {
        return add(Text.of(text).link(href));
    }

}
