package com.atlassian.adf.block.list;

import com.atlassian.adf.BlockNode;
import com.atlassian.adf.base.AbstractNodeContainer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.function.Consumer;

/**
 * https://developer.atlassian.com/cloud/stride/apis/document/nodes/orderedList
 */
@Data(staticConstructor = "orderedList")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(fluent = true, chain = true)
@JsonTypeName("orderedList")
@ParametersAreNonnullByDefault
public class OrderedList extends AbstractNodeContainer<OrderedList, ListItem> implements BlockNode {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = ListItem.class)
    public List<ListItem> children() {
        return super.children();
    }

    @JsonIgnore
    public OrderedList item(Consumer<ListItem> c) {
        final ListItem node = ListItem.listItem();
        c.accept(node);
        return add(node);
    }

    public static OrderedList orderedList(ListItem... elements) {
        return orderedList().add(elements);
    }

    @Override
    public String text() {
        final StringBuilder sb = new StringBuilder();
        final List<ListItem> children = children();
        if (children != null) {
            int size = children.size(), n = 0;
            for (ListItem i : children) {
                sb.append(n + 1);
                sb.append(". ");
                sb.append(i.text());
                if (n < size - 1) {
                    sb.append("\n");
                }
                n++;
            }
        }
        return sb.toString();
    }

}
