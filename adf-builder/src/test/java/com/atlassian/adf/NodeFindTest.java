package com.atlassian.adf;

import com.atlassian.adf.block.Panel;
import com.atlassian.adf.block.codeblock.Language;
import com.atlassian.adf.block.list.ListItem;
import com.atlassian.adf.inline.Text;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeFindTest {

    private Document createNode() {
        return Document.create()
                .paragraph(p -> p.text("Hello, ").strong("all").text("!"))
                .h1(p -> p.text("Title"))
                .paragraph(p -> p
                        .text("Text ")
                        .em("with ")
                        .code("format")
                        .text(" ")
                        .strike("and")
                        .text(" ")
                        .underline("other")
                        .text(" ")
                        .sup("things")
                        .sub(". "))
                .paragraph(p -> p.link("A link", "http://google.com").text(":"))
                .bulletList(l -> l
                        .item(i -> i.paragraph("An unordered"))
                        .item(i -> i.paragraph("List")))
                .paragraph("A:")
                .orderedList(l -> l
                        .item(i -> i.paragraph("numbered"))
                        .item(i -> i.paragraph("list")))
                .quote(b -> b.paragraph("Quote me"))
                .paragraph(p -> p.color("asda", "#97a0af"))
                .code(Language.java,"public static javaCodeBlock() {\n   asd();\n}")
                .paragraph(p -> p
                        .text("Multi")
                        .hardBreak()
                        .text("Line")
                        .hardBreak()
                        .text("No")
                        .hardBreak()
                        .text("Paragraph"))
                .warning(p -> p.paragraph("alert"))
                .info(p -> p.paragraph("Info"))
                .tip(p -> p.paragraph("idea"))
                .note(p -> p.paragraph("note"));
    }

    @Test
    void serializeToText() throws Exception {
        Document doc = createNode();
        String s = doc.text();
        assertEquals(loadText(), s);
    }

    @Test
    void findTextNodesStartingWithLetterA() throws Exception {
        Document doc = createNode();
        List<Text> nodes = doc.findAll((node, parent) -> node instanceof Text && node.text().toLowerCase().startsWith("a"));
        assertEquals(7, nodes.size());
    }

    @Test
    void findTextNodes() throws Exception {
        Document doc = createNode();
        List<Text> nodes = doc.findAll(Text.class);
        assertEquals(32, nodes.size());
    }

    @Test
    void findListItemTextNodes() throws Exception {
        Document doc = createNode();
        List<ListItem> nodes = doc.findAll(ListItem.class);
        assertEquals(4, nodes.size());
    }

    @Test
    void findPanelNodes() throws Exception {
        Document doc = createNode();
        List<Panel> panels = doc.findAll(Panel.class);
        assertEquals(4, panels.size());
        assertEquals(doc.children().get(11), panels.get(0));
        assertEquals(doc.children().get(12), panels.get(1));
        assertEquals(doc.children().get(13), panels.get(2));
        assertEquals(doc.children().get(14), panels.get(3));
    }

    private String loadText() throws Exception {
        final URI uri = NodeFindTest.class.getResource("/multiple-item-message-text.txt").toURI();
        return new String(Files.readAllBytes(Paths.get(uri)), StandardCharsets.UTF_8);
    }

}
