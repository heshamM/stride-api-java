# Stride Client API

Client library that abstracts the Stride API with a friendly interface.

This is an unofficial implementation of the API as you can find in the [official documentation](https://developer.atlassian.com/cloud/stride/rest/).

It includes:

- All the Stride API
- User details API

## Using the API

The entry point class is `StrideClient`.

Use `StrideClientSupplier` to build your client with a given configuration (`ConfigContext`) -
if you need to do it manually rather than using Spring Boot integration.


Call the API:

```java
strideClient.message()
    .send(text) // or an Atlassian Document
    .toConversation(Context.conversation(cloudId, conversationId)); 
```

## 100% Async certified

All the API is asynchronous and all methods return a `CompletableFuture` that, once completed, gives you the 
result object of that request, using one of the models available in this project.

The most important benefit of async is that you can implement API calls in your controllers and services inline
without having to worry about executors and passing contexts to the new threads.

Keep in mind that if your service takes too much time to respond to WebHooks, for sintance, Stride will retry 
the request. So, let's say you implement your controller in a way that you reply to a mention synchronously.
Because of the time to send the message, get the response, and do other stuff, Stride could think your're
unresponsive and call you again, probably resulting in a duplicate reply.

### Cool async stuff

You can chain async calls using `CompletableFuture#thenCompose`, like this example form the RefApp:


```java
strideClient.message().send("OK, I'm on it!").toConversation(context)
        .thenCompose(r -> messageRenderingService.renderToPlainText(doc, context))
        .thenCompose(r -> mentionExtractionService.extractAndSendMentions(doc, context))
        .thenCompose(r -> userDetailService.publishUserDetails(context, messageSent.getSender().getId()))
        .thenCompose(r -> messageExamplesService.sendMessageWithFormatting(context))
        .thenCompose(r -> messageWithImageService.sendMessageWithImage(context))
        .thenCompose(r -> glanceUpdaterService.updateGlance(context))
        .thenCompose(r -> done(context));
``` 

In this other example you can see how to start two requests in parallel (one to update the glance and other
to send a message) and then compose/join both into a third request so that the result status of the first is sent
after the second, while you handle an exceptional response: 

```java
CompletableFuture<Void> glanceFuture = strideClient.glance()
       .context(context)
       .key("refapp-glance")
       .label("Click me!!")
       .update();
strideClient.message()
       .send("Updating the glance state...")
       .toConversation(context)
       .thenCompose(r -> glanceFuture
               .whenComplete((r2, e) -> {
                   if (e == null) {
                       strideClient.message().send("It should be updated -->").toConversation(context);
                   } else {
                       strideClient.message()
                               .send("And it failed for some reason... :_(").toConversation(context);
                   }
               }));
```
 