package com.atlassian.stride.model.context;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;

/**
 * A generic way to specify a context within Stride, for instance when making a request to the API.
 * <p>
 * A context can be the whole site, a conversation, a user, or a user in a conversation.
 * <p>
 * For instance, you can send a message to a conversation, or to a specific user - which Stride calls "direct-conversation".
 * You can also push an update to the App Glance (the App icon in the right sidebar) which affects all users in all
 * conversations it's installed (site context), all users in a single conversation (conversation context),
 * a single user independently of the conversation (user scope), a single user in an specific conversation ("user in
 * conversation" context).
 */
@JsonDeserialize(as = ContextImpl.class)
@ParametersAreNonnullByDefault
public interface Context extends SiteContext, ConversationContext, UserContext, UserInConversationContext {

    static SiteContext site(String cloudId) {
        Objects.requireNonNull(cloudId,"cloudId");
        return ContextImpl.builder()
                .cloudId(cloudId)
                .build();
    }

    static ConversationContext conversation(String cloudId, String conversationId) {
        Objects.requireNonNull(cloudId,"cloudId");
        Objects.requireNonNull(conversationId,"conversationId");
        return ContextImpl.builder()
                .cloudId(cloudId)
                .conversationId(conversationId)
                .build();
    }

    static UserContext user(String cloudId, String userId) {
        Objects.requireNonNull(cloudId,"cloudId");
        Objects.requireNonNull(userId,"userId");
        return ContextImpl.builder()
                .cloudId(cloudId)
                .userId(userId)
                .build();
    }

    static UserInConversationContext userInConversation(String cloudId, String conversationId, String userId) {
        Objects.requireNonNull(cloudId,"cloudId");
        Objects.requireNonNull(conversationId,"conversationId");
        Objects.requireNonNull(userId,"userId");
        return ContextImpl.builder()
                .cloudId(cloudId)
                .conversationId(conversationId)
                .userId(userId)
                .build();
    }

    static Context of(ConversationContext conversationContext) {
        return (Context) conversationContext;
    }

    static Context of(SiteContext siteContext) {
        return (Context) siteContext;
    }

    static Context of(UserContext userContext) {
        return (Context) userContext;
    }

    static Context of(UserInConversationContext userInConversationContext) {
        return (Context) userInConversationContext;
    }

}
